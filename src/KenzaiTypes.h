/**
 * @file KenzaiTypes.h
 * @brief 建材機器の種類定義
 */
#ifndef KENZAITYPES_H
#define  KENZAITYPES_H

//#include "cytypes.h"
#include <stdint.h>
    
/**
 * @enum EKENZAI_EQUIP
 * @brief 取り扱う建材機器の種類を列挙する。
 */
typedef enum {
    EKENZAI_EQUIP_UNDEFINED = 0,
    EKENZAI_EQUIP_DEMO,
    EKENZAI_EQUIP_SOUSHOKU,    ///< 装飾窓.
    EKENZAI_EQUIP_SHUTTER1,    ///< シャッター１
    EKENZAI_EQUIP_SHUTTER2,    ///< シャッター２
    EKENZAI_EQUIP_LOOF_WINDOW, ///< 天窓
    EKENZAI_EQUIP_INTERIOR_SUNSHAED, ///<  日よけ
    EKENZAI_EQUIP_LOOVER,      ///< ルーバ
    EKENZAI_EQUIP_BLIND,       ///< ブラインド
    EKENZAI_EQUIP_TINTGLASS,   ///< 調光グラス
    EKENZAI_EQUIP_FUHRDOOR,    ///< FURH
    EKENZAI_EQUIP_NEXTDOOR,    ///< Next Door
    EKENZAI_EQUIP_NUM
} EKENZAI_EQUIP;


#endif    
/* [] END OF FILE */
