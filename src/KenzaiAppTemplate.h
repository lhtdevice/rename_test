﻿/**
 * @file KenzaiAppTemplate.h
 * @brief 建材機器のアプリケーションのひな型としてインターフェイスを定義する。
 */
#ifndef KENZAI_APP_TEMPLATE_H
#define KENZAI_APP_TEMPLATE_H
    
#include "KenzaiActDefine.h" 
#include "KenzaiTypes.h"    
    
typedef void (*KENZAI_APP_INIT_FUNC)(void);     
    
/** 
 * @struct SKenzaiAppTemplate
 * @brief 実装する建材機器の実装に必要な情報を格納する。
 */
typedef struct KenzaiAppTemplate_tag {
    KENZAI_APP_INIT_FUNC init_fn;   	///< 初期化.
    SKenzaiActDefine commonControl;		///< 操作のインターフェイス.
    EKENZAI_EQUIP equip_kind;			///< 建材種類.
} SKenzaiAppTemplate;
    
#endif    

/* [] END OF FILE */
