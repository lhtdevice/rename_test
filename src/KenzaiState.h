/**
 * @file  KenzaiState.h
 * @brief 建材機器の状態定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#ifndef KENZAI_STATE_H
#define KENZAI_STATE_H

#include <stdint.h>
#include <stdbool.h>

/**
 * @typedef enum EEQUIP_STATE
 * @brief 状態定義
 */
typedef enum {
        //! 0：状態不明
    EEQUIP_STATE_UNKNOWN = 0,
        //! 1：停止中（上限下限でない位置での停止）
    EEQUIP_STATE_STOPPED,
        //! 2：開動作中
    EEQUIP_STATE_OPENING,
        //! 3：全開停止中
    EEQUIP_STATE_OPENED,
        //! 4：閉動作中
    EEQUIP_STATE_CLOSING,
        //! 5：全閉停止中
    EEQUIP_STATE_CLOSED,
        //! 6：採風開動作中
    EEQUIP_STATE_VENTILATION_OPENING,
        //! 7：採風閉動作中
    EEQUIP_STATE_VENTILATION_CLOSING,
        //! 8：採風停止中（下限～採風全開間で停止中）
    EEQUIP_STATE_VENTILATION_STOPPED,
        //! 9：採風全開停止中
    EEQUIP_STATE_VENTILATION_OPENED,
        //! 10：ブラインド停止中（全閉～全開間で停止中）
    EEQUIP_STATE_BLIND_STOPPED,
        //! 11：ブラインド開動作中
    EEQUIP_STATE_BLIND_OPENING,
        //! 12：ブラインド全開停止中
    EEQUIP_STATE_BLIND_OPENED,
        //! 13：ブラインド閉動作中
    EEQUIP_STATE_BLIND_CLOSING,
        //! 14：ブラインド全閉停止中
    EEQUIP_STATE_BLIND_CLOSED,
        //! 15：全ラッチロック
    EEQUIP_STATE_ALLLATCH_LOCKED,
        //! 16：全ラッチアンロック
    EEQUIP_STATE_ALLLATCH_UNLOCKED,
        //! 17：ラッチ#1ロック
    EEQUIP_STATE_LATCH1_LOCKED,
        //! 18：ラッチ#1アンロック
    EEQUIP_STATE_LATCH1_UNLOCKED,
        //! 19：ラッチ#2ロック
    EEQUIP_STATE_LATCH2_LOCKED,
        //! 20：ラッチ#2アンロック
    EEQUIP_STATE_LATCH2_UNLOCKED,
        //! 21：ラッチ#3ロック
    EEQUIP_STATE_LATCH3_LOCKED,
        //! 22：ラッチ#3アンロック
    EEQUIP_STATE_LATCH3_UNLOCKED,
        //! 23：ラッチ#4ロック
    EEQUIP_STATE_LATCH4_LOCKED,
        //! 24：ラッチ#4アンロック
    EEQUIP_STATE_LATCH4_UNLOCKED,
        //! 25：ドアオープン
    EEQUIP_STATE_DOOR_OPENED,
        //! 26：ドアフルオープン
    EEQUIP_STATE_DOOR_FULLOPENED,
        //! 27：ドアクローズ
    EEQUIP_STATE_DOOR_CLOSED,
        //! 28：窓オープン
    EEQUIP_STATE_WINDOW_OPENED,
        //! 29：窓フルオープン
    EEQUIP_STATE_WINDOW_FULLOPENED,
        //! 30：窓クローズ
    EEQUIP_STATE_WINDOW_CLOSED,
    EEQUIP_STATE_NUM
} EEQUIP_STATE;

/**
 * @typedef enum EKENZAI_POSCONFIRM
 * @brief 停電状態定義
 */
typedef enum {
        //! 0：停電確認不要
    EKENZAI_POSCONFIRM_ENABLE,
        //! 1：停電確認必要
    EKENZAI_POSCONFIRM_DISABLE // 停電時に対応.
} EKENZAI_POSCONFIRM;

/**
 * @typedef enum EKENZAI_FAILURE
 * @brief エラー定義
 */
typedef enum {
        //! 0：エラーなし
    EKENZAI_FAILURE_NOTHING,
        //! 1：ハードウェアバージョンエラー
    EKENZAI_FAILURE_VERSION_HW,
        //! 2：ソフトウェアバージョンエラー
    EKENZAI_FAILURE_VERSION_SW,
        //! 3：自己診断エラー
    EKENZAI_FAILURE_SELFTEST,
        //! 4：初期化エラー
    EKENZAI_FAILURE_INIT,
        //! 5：未初期化エラー
    EKENZAI_FAILURE_NOTINIT,
        //! 6：タイムアウト
    EKENZAI_FAILURE_TIMEOUT,
        //! 7：既に開始済み
    EKENZAI_FAILURE_STARTED,
        //! 8：既に終了済み
    EKENZAI_FAILURE_STOPPED,
        //! 9：取り消し済み
    EKENZAI_FAILURE_CANCELLED,
        //! 10：データなし
    EKENZAI_FAILURE_EMPTY,
        //! 11：データフル
    EKENZAI_FAILURE_FULL,
        //! 12：ウォッチドッグタイマ カウントアップ
    EKENZAI_FAILURE_WDT_TIMEUP,
        //! 13：メモリ不足
    EKENZAI_FAILURE_LOWMEMORY,
        //! 14：リソース不足
    EKENZAI_FAILURE_RESOURCE,
        //! 15：パラメータ（引数）エラー
    EKENZAI_FAILURE_PARAMETER,
        //! 16：チェックサムエラー
    EKENZAI_FAILURE_CHECKSUM,
        //! 17：パリティエラー
    EKENZAI_FAILURE_PARITY,
        //! 18：ハッシュエラー
    EKENZAI_FAILURE_HASH,
        //! 19：比較不一致
    EKENZAI_FAILURE_COMPARE,
        //! 20：ビジー状態
    EKENZAI_FAILURE_BUSY,
        //! 21：接続エラー
    EKENZAI_FAILURE_CONNECT,
        //! 22：切断発生
    EKENZAI_FAILURE_DISCONNECT,
        //! 23：接続拒否
    EKENZAI_FAILURE_REFUSE,
        //! 24：リソースロック
    EKENZAI_FAILURE_LOCKED,
        //! 25：登録不能
    EKENZAI_FAILURE_REGISTER,
        //! 26：未登録
    EKENZAI_FAILURE_NOTREGISTERED,
        //! 27：認証エラー
    EKENZAI_FAILURE_AUTHENTICATION,
        //! ：
    EKENZAI_FAILURE_NUM,
} EKENZAI_FAILURE;

/**
 * @typedef enum EKENZAI_INIT_STATE
 * @brief 初期化状態定義
 */
typedef enum {
    //! 0：未初期化
    EKENZAI_INIT_STATE_NOT,
    //! 1：初期化済
    EKENZAI_INIT_STATE_ALREADY
} EKENZAI_INIT_STATE;

/**
 * @typedef enum EKENZAI_ACCIDENT
 * @brief 異常状態定義
 */
typedef enum {
        //! 0：異常なし
    EKENZAI_ACCIDENT_NONE,
        //! 1：過電流検出
    EKENZAI_ACCIDENT_OVERCURRENT,
        //! 2：過電流検出
    EKENZAI_WARNING_OVERCURRENT,
        //! 3：低電圧検出
    EKENZAI_ACCIDENT_LOWPOWER,
        //! 4：低負荷検出
    EKENZAI_ACCIDENT_LOWLOAD,
        //! 5：高電圧検出
    EKENZAI_ACCIDENT_HIGHVOLTAGE,
        //! 6：IOエラー
    EKENZAI_ACCIDENT_IOERROR,
        //! 7：破壊検出
    EKENZAI_ACCIDENT_DESTROYED,
} EKENZAI_ACCIDENT;

// このファイルは不適切かも。
/**
 * @typedef enum ESEND_KIND
 * @brief 通知定義
 */
typedef enum ESEND_KIND_tag {
    ESEND_KIND_ACK,     // コマンド受け取り通知（ACK）
    ESEND_KIND_NOTIFY   // 状態変化通知.
} ESEND_KIND ;


/** 
* @struct SKenzaiState
* @brief 建材機器の状態に関わる情報を格納する。
*/
typedef struct KeinzaiState_tag {
    uint8_t Initialized;
    uint8_t Pairing;
    EEQUIP_STATE equipState;
    EKENZAI_POSCONFIRM posConfirm;   ///< 停電情報に対応.
    EKENZAI_FAILURE failureInfo;
    EKENZAI_ACCIDENT accident;
  //  SKenzaiActRegist actRegist;
} SKenzaiState;


uint8_t KenzaiState_IsAccident(const SKenzaiState*);
uint8_t KenzaiState_IsPairingAdmit(const SKenzaiState* state);
uint8_t KenzaiState_IsInitiallized(const SKenzaiState* state);
uint8_t KenzaiState_IsOpened(const SKenzaiState* state);
uint8_t KenzaiState_IsTeiden(const SKenzaiState* state);
uint8_t KenzaiState_IsFailureState(const SKenzaiState* state);
uint8_t KenzaiState_IsSaifu(const SKenzaiState* state);
uint8_t KenzaiState_IsLocked(const SKenzaiState* state);
uint8_t KenzaiState_IsMoving(const SKenzaiState *state);


#endif
/* [] END OF FILE */
