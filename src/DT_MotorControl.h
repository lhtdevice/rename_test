/*
 Copyright (C) 2009-2020, Heartland.Data Inc. All rights reserved.        
                                                                          
 This software is furnished under a license and may be used and copied    
 only in accordance with the terms of such license and with the inclusion 
 of the above copyright notice. No title to and ownership of the software 
 is transferred.                                                          
 Heartland.Data Inc. makes no representation or warranties with           
 respect to the performance of this computer program, and specifically    
 disclaims any responsibility for any damages, special or consequential,  
 connected with the use of this program.                                  
*/

#ifndef	__DT_MotorControl_h__
#define	__DT_MotorControl_h__

#ifdef	__cplusplus
	#define	__DtEXTERN	extern "C"
#else
	#define	__DtEXTERN	extern
#endif

/* TestPoint MacroCode -----------------------------------------------------*/
#ifdef		__DtBaseAddress
#undef		__DtBaseAddress
#endif
#define		__DtBaseAddress		0x20
#define		__DtAllEnable		1
#if ( __DtAllEnable == 1 )
#define		__DtTestPoint(func, step)		__Dt##func##step
#define		__DtTestPointValue(func, step, value, size)		__Dt##func##step(value,size)
#define		__DtTestPointWrite(func, step, value, size)		__Dt##func##step(value,size)
#define		__DtTestPointEventTrigger(func, step, data)		__Dt##func##step(data)
#define		__DtTestPointEventTrigger32(func, step, data)		__Dt##func##step(data)
#define		__DtTestPointFuncCall(func, step, call)		__Dt##func##step(call)
#define		__DtTestPointFast(step, bit_num)		__DtFast_##step(step, bit_num)
#define		__DtTestPointValueFast(step, bit_num, value, size)		__DtFast_##step(step, bit_num, value, size)
#define		__DtTestPointEventTriggerFast(step, bit_num, data)		__DtFast_##step(step, bit_num, data)
#define		__DtTestPointEventTrigger32Fast(step, bit_num, data)		__DtFast_##step(step, bit_num, data)
#else
#define		__DtTestPoint(func, step)		
#define		__DtTestPointValue(func, step, value, size)		
#define		__DtTestPointWrite(func, step, value, size)		
#define		__DtTestPointEventTrigger(func, step, data)		
#define		__DtTestPointEventTrigger32(func, step, data)		
#define		__DtTestPointFuncCall(func, step, call)		call
#define		__DtTestPointFast(step, bit_num)		
#define		__DtTestPointValueFast(step, bit_num, value, size)		
#define		__DtTestPointEventTriggerFast(step, bit_num, data)		
#define		__DtTestPointEventTrigger32Fast(step, bit_num, data)		
#endif
__DtEXTERN		void	_TP_BusOut( unsigned int addr, unsigned int dat );
__DtEXTERN		void	_TP_MemoryOutput( unsigned int addr, unsigned int dat, void *value, unsigned int size );
__DtEXTERN		void	_TP_WritePoint( unsigned int addr, unsigned int dat, void *value, unsigned int size );
__DtEXTERN		void	_TP_EventTrigger( unsigned int addr, unsigned int dat, unsigned int event_id );
__DtEXTERN		void	_TP_EventTrigger32( unsigned int addr, unsigned int dat, unsigned int event_id );
__DtEXTERN		void	_TP_BusOutFast( unsigned int step, unsigned int bit_num );
__DtEXTERN		void	_TP_MemoryOutputFast( unsigned int step, unsigned int bit_num, void *value, unsigned int size );
__DtEXTERN		void	_TP_EventTriggerFast( unsigned int step, unsigned int bit_num, unsigned int event_id );
__DtEXTERN		void	_TP_EventTrigger32Fast( unsigned int step, unsigned int bit_num, unsigned int event_id );

/* TestPoint FuncList ------------------------------------------------------*/
#define		__DtFunc_MotorControl_getState		0
#define		__DtFunc_MotorControl_init		1
#define		__DtFunc_MotorControl_start		2
#define		__DtFunc_MotorControl_stop		3
#define		__DtFunc_setSpeed		4

/* TestPoint StepList ------------------------------------------------------*/
#define		__DtStep_0		0
#define		__DtStep_1		1
#define		__DtStep_2		2
#define		__DtStep_3		3
#define		__DtStep_4		4
#define		__DtStep_5		5

/* TestPoint DisableList ---------------------------------------------------*/
#define	__DtFast_87(step,bit_num)	/*FuncIn*/	
#define	__DtFast_88(step,bit_num)	/*if*/	
#define	__DtFast_89(step,bit_num)	/*if+FuncOut*/	
#define	__DtFast_90(step,bit_num)	/*if+FuncOut*/	
#define	__DtFast_91(step,bit_num)	/*FuncOut*/	
#define	__DtFast_92(step,bit_num)	/*FuncOut*/	
#define	__DtFast_93(step,bit_num)	/*FuncIn*/	_TP_BusOutFast( step, bit_num );
#define	__DtFast_94(step,bit_num)	/*FuncOut*/	
#define	__DtFast_95(step,bit_num)	/*FuncIn*/	_TP_BusOutFast( step, bit_num );
#define	__DtFast_96(step,bit_num)	/*if*/	
#define	__DtFast_97(step,bit_num)	/*if*/	
#define	__DtFast_98(step,bit_num)	/*if+FuncOut*/	
#define	__DtFast_99(step,bit_num)	/*FuncOut*/	
#define	__DtFast_100(step,bit_num)	/*FuncIn*/	_TP_BusOutFast( step, bit_num );
#define	__DtFast_101(step,bit_num)	/*FuncOut*/	
#define	__DtFast_102(step,bit_num,value,size)	/*Value*/	_TP_MemoryOutputFast( step, bit_num, value, size );
#define	__DtFast_103(step,bit_num)	/*FuncIn*/	_TP_BusOutFast( step, bit_num );
#define	__DtFast_104(step,bit_num)	/*FuncOut*/	

#endif	/* __DT_MotorControl_h__ */

