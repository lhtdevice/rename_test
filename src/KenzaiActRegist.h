/**
 * @file KenzaiActRegist.h
 * @brief 機器に対する操作の結果に応じた処理についての定義。
 * @copyright (c) LIXIL Corporation, 2018-2019 All Rights Reserved.
 */
#ifndef KENZAI_ACT_REGIST_H
#define KENZAI_ACT_REGIST_H
    
#include <stdint.h>
    
/** 
* @enum EACT_CHANGE
* @brief 状態の変化の識別
*/
typedef enum {
    EACT_CHANGE_TO_CLOSE,	///< 全閉状態に変化.
    EACT_CHANGE_TO_OPEN,    ///< 全開状態に変化
    EACT_CHANGE_TO_VENTILATION, ///< 採風状態に変化
    EACT_CHANGE_TO_LOCK,    ///< 施錠状態に変化
    EACT_CHANGE_TO_UNLOCK,  ///< 解錠状態に変化
    EACT_CHANGE_TO_OTHER	///< 定義以外の状態に変化.
} EACT_CHANGE;

/** 
* @typedef struct SKenzaiActRegist
* @brief 状態変化を記録する変数の集合
*/
typedef struct KenzaiActRegist_tag {
    uint32_t allCloseCount; ///< 全閉への操作のカウント.(ログからは出したほうがいいかも）
    uint32_t allOpenCount;  ///< 全開への操作カウント
    uint32_t allVentilationCount;   ///< 採風への操作カウント
    uint32_t allLockCount;  ///< 施錠への操作カウント
    uint32_t allUnlockCount;    ///< 解錠への操作カウント
    uint32_t allOtherCount; ///< 定義外の操作カウント
} SKenzaiActRegist;


uint32_t KenzaiActRegist_GetAllCloseCount(void);
uint32_t KenzaiActRegist_GetAllOpenCount(void);
uint32_t KenzaiActRegist_GetAllVentilationCount(void);
uint32_t KenzaiActRegist_GetAllLockCount(void);
uint32_t KenzaiActRegist_GetAllUnlockCount(void);
uint32_t KenzaiActRegist_GetAllFalseCount(void);
void     KenzaiActRegist_Regist(EACT_CHANGE act_change);

#endif    
/* [] END OF FILE */
