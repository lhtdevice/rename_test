/**
 * @file  debugimpl.c
 * @brief デバッグI/F実装ファイル
 * @remark デバッグ出力を使用するためにはここで実体の実装が必要。
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#include "project.h"
#include "dev_configure.h"
#include "dev_uart.h"
#include "DebugMonitor.h"

#ifdef DEBUG
#if USE_DEV_UART

#define DBQUEUE_DEPTH 8
static struct {
    uint8_t buf[MAXSIZE_UART_RECV];
    size_t size;
} db_queue[DBQUEUE_DEPTH];
static volatile uint8_t db_q_last = 0;
static volatile uint8_t db_q_next = 0;
static volatile uint8_t db_q_num  = 0;

/**
 * @brief Debug出力をキューに格納する。
 * @remark キューがフルになるタイミングでは、現在送信中のキューのデータを上書きしてしまう。そのため（DBQUEUE_DEPTH - 1）でキューをフルにしている。
 * @param[in] buf：出力文字列へのポインタ
 * @param[in] size：出力文字列長
 * @retval キューのデータ数
 */
static uint8_t write_DbBuf2Queue(const void * const buf, const size_t size)
{
    if ((DBQUEUE_DEPTH - 1) <= db_q_num) // キューがフル
    {
        return 0;
    }
    // 以下else
    memcpy(db_queue[db_q_next].buf, buf, size);
    db_queue[db_q_next].size = size;
    db_q_num += 1;
    db_q_next = (db_q_next + 1) % DBQUEUE_DEPTH;
    return db_q_num;
}

/**
 * @brief キューのデータをDebugUARTを出力する。
 * @param[in] -
 * @retval 送信データサイズ、負値：失敗
 */
static int32_t write_DbQueue2Uart(void)
{
    int32_t result = dev_uart_Write(UARTIDX_DEBUG, db_queue[db_q_last].buf, db_queue[db_q_last].size, DEVIO_ASYNC);
    switch (result)
    {
        case DEVRESULT_SUCCESS:
            db_q_num -= 1;
            db_q_last = (db_q_last + 1) % DBQUEUE_DEPTH;
            break;
        case DEVRESULT_BUSY:
            break;
        default: // error
            break;
    }
    return result;
}

#if 0
/**
 * @brief Debugシリアル受信コールバックサンプル。
 * @details プロトタイプはPrjLibDevice.hに定義する。
            ターミネータ（\a）を受信する。もしくはバッファフルでコールされる。
 * @param[in] sParam：受信バッファポインタの整数値
 * @param[in] uParam：受信データサイズ
 * @retval -
 */
void serdb_recv_callback(const intptr_t sParam, const uintptr_t uParam)
{
    (void)sParam; (void)uParam;
    // デバッグモニタからの入力処理をここに記述する（モード変更等）
}

#endif

static void serdb_sent_callback(const intptr_t sParam, const uintptr_t uParam)
{
    (void)sParam; (void)uParam;
    // デバッグモニタからの送信完了処理をここに記述する
    if (0 < db_q_num) // 送信キューにデータがある場合
    {
        write_DbQueue2Uart();
    }
}

void DebugOutput(const char* str)
{
    switch (write_DbBuf2Queue(str, strlen(str)))
    {
    case 0: // キューがフル。データは捨てる。
        break; // nothing to do
    case 1: // キューにデータが1個だけ（UART not busy）。送信実行。
        write_DbQueue2Uart();
        break;
    default: // キューにデータが2個以上（UART busy）。送信完了割り込みで送信。
        break; // nothing to do
    }
    //while (DEVRESULT_BUSY == dev_uart_Write(UARTIDX_DEBUG, str, strlen(str), DEVIO_ASYNC))
    //{
    //    CyDelay(1);
    //}
}

void DebugOutputWithInt(const char* str, uint32_t number)
{
    char num[12];
    if (NULL == str)
    {
		snprintf(num, 12, "%08X"STR_CRLF, (unsigned int)number);
    }
    else
    {
        DebugOutput(str);
        snprintf(num, 12, ":%08X"STR_CRLF, (unsigned int)number);
    }
    DebugOutput(num);
}

void DebugOutput_init(void)
{
    dev_uart_ClearLibrary();
    dev_uart_Initialize(UARTIDX_DEBUG, NULL, serdb_sent_callback);
}

#endif//USE_DEV_UART
#endif//DEBUG
/* [] END OF FILE */
