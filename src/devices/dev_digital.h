/**
 * @file  dev_digital.h
 * @brief デジタルIO定義ファイル
 * @remark High/Low2値を扱うデバイスを定義する。スイッチ入力、2値センサ、LED出力等。
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#ifndef DEV_DIGITAL_H
#define DEV_DIGITAL_H

#include "LibDevice.h"

enum tag_digital_index {
    GPIOIDX_MODE1 = 0, // Input
    GPIOIDX_MODE2,
    GPIOIDX_MODE3,
    GPIOIDX_MODE4,
    GPIOIDX_MODE5,
    GPIOIDX_MODE6,
    GPIOIDX_RAIN,
    //GPIOIDX_THERMO,
    GPIOIDX_LSW1,
    GPIOIDX_LSW2,
    GPIOIDX_OPEN,	   // Output
    GPIOIDX_CLOSE,
    GPIOIDX_RS485IRQ,
    GPIOIDX_RS485CS,
    //GPIOIDX_RELAY1,
    //GPIOIDX_RELAY2,
    GPIOIDX_ACL1,
    GPIOIDX_ACL2,
    GPIOIDX_MAX
};
#define NUM_GPIO_DEVICES  GPIOIDX_MAX
#define GPIOIDX_ACMOPEN   GPIOIDX_ACL1
#define GPIOIDX_ACMCLOSE  GPIOIDX_ACL2
#define GPIOIDX_LSWOPEN   GPIOIDX_LSW1
#define GPIOIDX_LSWCLOSE  GPIOIDX_LSW2

typedef enum tag_io_int_type {
    IOINT_NONE = 0,
    IOINT_RISING,
    IOINT_FALLING,
    IOINT_BOTH,
} IOINT_TYPE;

void    dev_digital_ClearLibrary(void);
bool    dev_digital_Initialize(const uint8_t channel, const FPDEV_CALLBACK fpIntr);
bool    dev_digital_Finalize(const uint8_t channel);
int32_t dev_digital_Read(const uint8_t channel);
int32_t dev_digital_Write(const uint8_t channel, const uint32_t data);
int32_t dev_digital_Wait(const uint8_t channel, const uint32_t data, const DEVIO_TYPE iotype, const IOINT_TYPE inttype);
int32_t dev_digital_Cancel(const uint8_t channel);

#if 0
bool dev_digital_Init(
    S_DEVICE * const            dev,
    const char * const          id,
    const FPDEV_CLEAR_INTERRPUT fpClearInt,
    const FPDEV_CALLBACK        fpCallback,
    const FPDEV_READ_U32        fpRead,
    const FPDEV_WRITE_U32       fpWrite
);
bool dev_digital_Finalize(const S_DEVICE * const dev);

bool dev_digital_read(const S_DEVICE * const dev, uint32_t * const value);
bool dev_digital_write(const S_DEVICE * const dev, const uint32_t value);
void dev_digital_int_handler(S_DEVICE * const dev, const uint32_t opt);
#endif

#endif//DEV_DIGITAL_H
/* [] END OF FILE */
