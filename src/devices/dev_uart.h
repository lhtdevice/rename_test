/**
 * @file  dev_uart.h
 * @brief UARTデバイス定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2019 All Rights Reserved.
 */

#ifndef DEV_UART_H
#define DEV_UART_H

#include "LibDevice.h"

enum tag_uart_index {
    UARTIDX_DEBUG = 0,
    UARTIDX_RS485,
    UARTIDX_COMMMODULE,
    UARTIDX_MAX
};
#define NUM_UART_DEVICES  UARTIDX_MAX
#define MAXSIZE_UART_RECV 80

void dev_uart_ClearLibrary(void);
bool dev_uart_Initialize(const uint8_t channel, const FPDEV_CALLBACK fpRecv, const FPDEV_CALLBACK fpSent);
bool dev_uart_Finalize(const uint8_t channel);
int32_t dev_uart_Cancel(const uint8_t channel);
int32_t dev_uart_Read(const uint8_t channel, void * const buf, const size_t size, const DEVIO_TYPE type);
int32_t dev_uart_Write(const uint8_t channel, const void * const buf, const size_t size, const DEVIO_TYPE type);

#endif//DEV_UART_H
/* [] END OF FILE */
