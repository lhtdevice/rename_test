﻿/**
 * @file  dev_pwm.h
 * @brief PWMデバイス定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#ifndef DEV_PWM_H
#define DEV_PWM_H

#include "LibDevice.h"

enum tag_pwm_index {
    PWMIDX_DCMOTOR = 0,
    PWMIDX_RESERVE,
    PWMIDX_MAX
};
#define NUM_PWM_DEVICES PWMIDX_MAX
#define PWM_LEVEL_LOW   0
#define PWM_LEVEL_HIGH  1

void dev_pwm_ClearLibrary(void);
bool dev_pwm_Initialize(const uint8_t channel, const FPDEV_CALLBACK fpCallback);
bool dev_pwm_Finalize(const uint8_t channel);

bool dev_pwm_start(const uint8_t channel);
bool dev_pwm_stop(const uint8_t channel);
bool dev_pwm_update(const uint8_t channel, const uint32_t rate, const uint32_t period);
bool dev_pwm_update2(const uint8_t channel, const uint32_t rate, const uint32_t rate2, const uint32_t period);
bool dev_pwm_getParameter(const uint8_t ch, uint32_t * const rate, uint32_t * const period);
bool dev_pwm_getParameter2(const uint8_t ch, uint32_t * const rate, uint32_t * const rate2, uint32_t * const period);
//bool dev_pwm_output(const uint8_t channel, const uint8_t level);

#endif//DEV_PWM_H
/* [] END OF FILE */
