/**
 * @file  dev_timer.c
 * @brief タイマーデバイス実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#include "project.h"
#include "dev_timer.h"

#if USE_DEV_TIMER

typedef bool (* FPTIMER)(void);
typedef bool (* FPRTCGET)(void * const);
typedef bool (* FPRTCSET)(const void * const);

typedef struct s_timer_struct {
    FPDEV_CALLBACK fpCallback;
    uint32_t ticks;
    struct {
        uint32_t       count;
        TIMERCB_TYPE   type;
        FPDEV_CALLBACK fpCallback;
    } entry[NUM_REGIST_ENTRY];
    bool assigned;
} DEV_TIMER;

/********************* ↓デバイス依存関数プロトタイプ↓ *************************/
// 10ms タイマー
static bool TIMER1MS_Initialize(void);
static bool TIMER1MS_Finalize(void);
static bool TIMER1MS_Start(void);
static bool TIMER1MS_Stop(void);
// 1s タイマー
static bool RTC_Initialize(void);
static bool RTC_Finalize(void);
static bool RTC_Start(void);
static bool RTC_Stop(void);
static bool RTC_Get(void *);
static bool RTC_Set(const void *);
/********************* ↑デバイス依存関数プロトタイプ↑ *************************/

static DEV_TIMER dev[NUM_TIMER_DEVICES];
static FPTIMER  fpInit[NUM_TIMER_DEVICES]   = {TIMER1MS_Initialize, RTC_Initialize};
static FPTIMER  fpFine[NUM_TIMER_DEVICES]   = {TIMER1MS_Finalize,   RTC_Finalize};
static FPTIMER  fpStart[NUM_TIMER_DEVICES]  = {TIMER1MS_Start,      RTC_Start};
static FPTIMER  fpStop[NUM_TIMER_DEVICES]   = {TIMER1MS_Stop,       RTC_Stop};
static FPRTCGET fpGet[NUM_TIMER_DEVICES]    = {NULL,                RTC_Get};
static FPRTCSET fpSet[NUM_TIMER_DEVICES]    = {NULL,                RTC_Set};

/**
 * @brief タイマーデバイスライブラリを初期化します。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void dev_timer_ClearLibrary(void)
{
    static bool bClear = false;
    if (!bClear)
    {
        memset(dev, 0, sizeof(dev));
        bClear = true;
    }
}

/**
 * @brief タイマーデバイスを初期化します。
 * @param[in] ch         使用デバイスチャネル
 * @param[in] fpCallback コールバック関数へのポインタ、NULL可
 * @param[out] -
 * @retval uint8_t （bool値） true：異常あり、false：異常なし
 */
bool dev_timer_Initialize(const uint8_t ch, const FPDEV_CALLBACK fpCallback)
{
    if ((ch >= NUM_TIMER_DEVICES) || dev[ch].assigned) return false;
    dev[ch].fpCallback = fpCallback;
    dev[ch].assigned = true;
    if (fpInit[ch]) fpInit[ch]();
    if (fpStart[ch]) fpStart[ch]();
    return true;
}

/**
 * @brief タイマーデバイスを終了します。
 * @param[in] ch 使用デバイスチャネル
 * @retval uint8_t （bool値） true：異常あり、false：異常なし
 */
bool dev_timer_Finalize(const uint8_t ch)
{
    if ((ch >= NUM_TIMER_DEVICES) || !dev[ch].assigned) return false;
    if (fpFine[ch]) fpFine[ch]();
    memset(&dev[ch], 0, sizeof(DEV_TIMER));
    return true;
}

/**
 * @brief タイマーを開始します。
 * @param[in] ch 使用デバイスチャネル
 * @retval uint8_t （bool値） true：異常あり、false：異常なし
 */
bool dev_timer_start(const uint8_t ch)
{
    if ((ch >= NUM_TIMER_DEVICES) || !dev[ch].assigned) return false;
    if (fpStart[ch]) fpStart[ch]();
    return true;
}

/**
 * @brief タイマーを停止します。
 * @param[in] ch 使用デバイスチャネル
 * @retval uint8_t （bool値） true：異常あり、false：異常なし
 */
bool dev_timer_stop(const uint8_t ch)
{
    if ((ch >= NUM_TIMER_DEVICES) || !dev[ch].assigned) return false;
    if (fpStop[ch]) fpStop[ch]();
    return true;
}

/**
 * @brief 現在のカウント値を取得します。
 * @param[in] ch 使用デバイスチャネル
 * @retval uint32_t タイマーカウント値
 */
uint32_t dev_timer_getTicks(const uint8_t ch)
{
    if ((ch >= NUM_TIMER_DEVICES) || !dev[ch].assigned) return false;
    return dev[ch].ticks;
}

static inline uint8_t BCD2Dec(const uint8_t bcd)
{
	return (bcd & 0x0f) + (((bcd >> 4) & 0x0f) * 10);
}

static inline uint8_t Dec2BCD(const uint8_t dec)
{
	return (dec % 10) + ((dec / 10) << 4);
}

/**
 * @brief 現在のRTCカウント値を取得します。
 * @param[in] ch 使用デバイスチャネル
 * @param[in] p tm構造体へのポインタ
 * @retval bool 成功／失敗
 */
bool dev_timer_getRTC(const uint8_t ch, struct tm * const p)
{
	rtc_counter_value_t cv;
    if ((ch >= NUM_TIMER_DEVICES) || !dev[ch].assigned) return false;
    if (fpGet[ch] && fpGet[ch](&cv))
	{
		p->tm_year  = 100 + BCD2Dec(cv.year);
		p->tm_mon   = BCD2Dec(cv.month) - 1;
		p->tm_mday  = BCD2Dec(cv.day);
		p->tm_wday  = cv.week;
		p->tm_hour  = BCD2Dec(cv.hour);
		p->tm_min   = BCD2Dec(cv.min);
		p->tm_sec   = BCD2Dec(cv.sec);
		p->tm_yday  = 0; // currently, not supported.
		p->tm_isdst = 0;
		return true;
	}
	return false;
}

/**
 * @brief RTCカウント値を設定します。
 * @param[in] ch 使用デバイスチャネル
 * @param[in] p tm構造体へのポインタ
 * @retval bool 成功／失敗
 */
bool dev_timer_setRTC(const uint8_t ch, const struct tm * const p)
{
	rtc_counter_value_t cv;
    if ((ch >= NUM_TIMER_DEVICES) || !dev[ch].assigned) return false;
    if (fpSet[ch])
	{
		cv.year  = Dec2BCD(p->tm_year - 100);
		cv.month = Dec2BCD(p->tm_mon + 1);
		cv.day   = Dec2BCD(p->tm_mday);
		cv.week  = Dec2BCD(p->tm_wday);
		cv.hour  = Dec2BCD(p->tm_hour);
		cv.min   = Dec2BCD(p->tm_min);
		cv.sec   = Dec2BCD(p->tm_sec);
		return fpSet[ch](&cv);
	}
	return false;
}

/**
 * @brief タイマーを登録します。
 * @param[in] ch 使用デバイスチャネル
 * @param[in] count 設定するタイマーのカウント値（時間ではないことに注意）
 * @param[in] type タイマのタイプ（単発／連続）
 * @param[in] fpCallback コールバック関数へのポインタ
 * @retval int32_t 成功：タイマーID（0 <= < NUM_REGIST_ENTRY）、失敗：エラーコード（負）
 */
int32_t dev_timer_setCallback(const uint8_t ch, const uint32_t count, const TIMERCB_TYPE type, FPDEV_CALLBACK fpCallback)
{
    if ((ch >= NUM_TIMER_DEVICES) || !dev[ch].assigned || !count) return TIMER_ID_ERROR;
    if ((TIMERCB_NONE >= type) || (TIMERCB_CONTINUOUS < type)) return TIMER_ID_ERROR;
    if (!dev[ch].fpCallback && !fpCallback) return TIMER_ID_ERROR;
    for (int ii=0; ii<NUM_REGIST_ENTRY; ii++)
    {
        if (TIMERCB_NONE == dev[ch].entry[ii].type)
        {
            dev[ch].entry[ii].count = count;
            if (TIMERCB_ONETIME == type)
            {
                dev[ch].entry[ii].count += dev[ch].ticks;
            }
            dev[ch].entry[ii].type = type;
            if (fpCallback) dev[ch].entry[ii].fpCallback = fpCallback;
            else            dev[ch].entry[ii].fpCallback = dev[ch].fpCallback;
            return ii;
        }
    }
    return TIMER_ID_ERROR;
}

/**
 * @brief タイマーをキャンセルします。
 * @param[in] ch 使用デバイスチャネル
 * @param[in] id キャンセルするタイマーID
 * @retval int32_t 成功：0、失敗：エラーコード（負）
 */
int32_t dev_timer_cancelCallback(const uint8_t ch, const int32_t id)
{
    if ((ch >= NUM_TIMER_DEVICES) || !dev[ch].assigned || (0 > id) || (NUM_REGIST_ENTRY <= id)) return TIMER_ID_ERROR;
    dev[ch].entry[id].type = TIMERCB_NONE;
    return 0;
}

/**
 * @brief タイマー割り込みハンドラ。プロジェクトのハードウェア割り込みから呼ばれる。
 * @remark 登録タイマー値をチェックしてコールバック関数を呼び出す。
 * @param[in] ch 使用デバイスチャネル
 * @retval -
 */
static void dev_timer_int_handler(const uint8_t ch)
{
    dev[ch].ticks += 1;
    if (0 == dev[ch].ticks) dev[ch].ticks = 1;
    if (dev[ch].fpCallback)
    {
        for (int ii=0; ii<NUM_REGIST_ENTRY; ii++)
        {
            if (TIMERCB_CONTINUOUS == dev[ch].entry[ii].type)
            {
                if (0 == (dev[ch].ticks % dev[ch].entry[ii].count))
                {
                    dev[ch].entry[ii].fpCallback(ii, dev[ch].ticks);
                }
            }
            else if (TIMERCB_ONETIME == dev[ch].entry[ii].type)
            {
                if (dev[ch].ticks == dev[ch].entry[ii].count)
                {
                    dev[ch].entry[ii].type = TIMERCB_NONE;
                    dev[ch].entry[ii].fpCallback(ii, dev[ch].ticks);
                }
            }
        }
    }
}

/********************* ↓ハードウェア依存部↓ ***************************/
/**
 * @brief タイマ割り込みルーチン。
 */
void ISR_Timer1ms(void)
{
    dev_timer_int_handler(TIMERIDX_1MS);
}

static bool TIMER1MS_Initialize(void)
{
    TIMER1MS_Start();
    return true;
}

static bool TIMER1MS_Finalize(void)
{
    TIMER1MS_Stop();
    return true;
}

static bool TIMER1MS_Start(void)
{
    R_TAU0_Channel3_Start();
    return true;
}

static bool TIMER1MS_Stop(void)
{
    R_TAU0_Channel3_Stop();
    return true;
}

/**
 * @brief リアルタイムクロック割り込みルーチン。
 */
void ISR_RTC(void)
{
    dev_timer_int_handler(TIMERIDX_1S);
}

static bool RTC_Initialize(void)
{
    RTC_Start();
    return true;
}

static bool RTC_Finalize(void)
{
    RTC_Stop();
    return true;
}

static bool RTC_Start(void)
{
    R_RTC_Start();
    return true;
}

static bool RTC_Stop(void)
{
    R_RTC_Stop();
    return true;
}

static bool RTC_Get(void * const p)
{
	rtc_counter_value_t *pcv = (rtc_counter_value_t *)p;
	MD_STATUS st = R_RTC_Get_CounterValue(pcv);
	return (MD_OK == st);
}

static bool RTC_Set(const void * const p)
{
	rtc_counter_value_t *pcv = (rtc_counter_value_t *)p;
	MD_STATUS st = R_RTC_Set_CounterValue(*pcv);
	return (MD_OK == st);
}
/********************* ↑ハードウェア依存部↑ ***************************/
#endif//USE_DEV_TIMER
/* [] END OF FILE */
