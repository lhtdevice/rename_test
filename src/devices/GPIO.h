/**
 * @file GPIO.h
 */

#ifndef _GPIO_H_
#define _GPIO_H_

/*******************************************************************************
 * Include Files
 *******************************************************************************/
#include "iodefine.h"

/*******************************************************************************
 * Macros
 *******************************************************************************/
#define GPIO_LED1       P6_bit.no2
#define GPIO_LED2       P6_bit.no3
#define GPIO_FGC        P5_bit.no1
#define GPIO_SS         P5_bit.no2
#define GPIO_CW_CCW     P5_bit.no3

#define GPIO_DATA_LOW   0
#define GPIO_DATA_HIGH  1

#endif // _GPIO_H_