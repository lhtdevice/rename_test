/**
 * @file  LibDevice.c
 * @brief デバイス共通部実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#include "LibDevice.h"
#if USE_DEV_DIGITAL
    #include "dev_digital.h"
#endif
#if USE_DEV_TIMER
    #include "dev_timer.h"
#endif
#if USE_DEV_UART
    #include "dev_uart.h"
#endif
#if USE_DEV_EEPROM
    #include "dev_eeprom.h"
#endif
#if USE_DEV_PWM
    #include "dev_pwm.h"
#endif

/**
 * @brief デバイスライブラリを初期化します。
 * @param[in] -
 * @param[out] -
 * @retval bool true：成功、false：失敗
 */
bool initDeviceLibrary(void)
{
#if USE_DEV_DIGITAL
    dev_digital_ClearLibrary();
#endif

#if USE_DEV_TIMER
    dev_timer_ClearLibrary();
#endif

#if USE_DEV_UART
    dev_uart_ClearLibrary();
#endif

#if USE_DEV_EEPROM
    dev_eeprom_ClearLibrary();
#endif

#if USE_DEV_PWM
    dev_pwm_ClearLibrary();
#endif

    return true;
}

/* [] END OF FILE */
