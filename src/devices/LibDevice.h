/**
 * @file  LibDevice.h
 * @brief デバイス共通部定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#ifndef LIB_DEVICE_H
#define LIB_DEVICE_H

#define LIB_DEVICE_PROJECT_STR "Device Library"
#define LIB_DEVICE_VERSION_STR "0.34.201012"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "dev_configure.h"

#ifdef __RL78__
 #include "r_cg_macrodriver.h"
 typedef int32_t             intptr_t;
 typedef uint32_t            uintptr_t;
#else
 #include <stdint.h>
#endif

#define DEVRESULT_SUCCESS    0
#define DEVRESULT_FAILURE   -1
#define DEVRESULT_PARAMETER -2
#define DEVRESULT_BUSY      -3
#define DEVRESULT_NULL      -4

#define DEVIO_FAILURE   -1

#define FALLING_EDGE 0
#define RISING_EDGE  1
#define LEVEL_LOW    0
#define LEVEL_HIGH   1
#define DIR_CLOSE    0
#define DIR_OPEN     1
#define DIR_STOP     0xff

typedef enum enum_device_io_type {
    DEVIO_SYNC = 0,   // 同期リードライト。指定バイト数読み込みまでブロック
    DEVIO_ASYNC,      // 非同期リードライト。指定バイト数読み込みでコールバック関数をコール
    DEVIO_ASYNC_CONT  // 連続非同期リード。
} DEVIO_TYPE;

/**
 * @typedef enum DEVICE_TYPE
 * @brief デバイス識別コード定義
 */
typedef enum tag_enum_device {
    DEVTYPE_NONE = 0,
    DEVTYPE_DIGITAL,
    DEVTYPE_ADC,
    DEVTYPE_MOTOR,
    DEVTYPE_PWM,
    DEVTYPE_TIMER,
    DEVTYPE_UART,
    DEVTYPE_EEPROM,
    DEVTYPE_ENCODER,    // モータエンコーダ
    DEVTYPE_USERDEFINE  //! ユーザ定義デバイス 新しいデバイスはこの前に追加する。
} DEVICE_TYPE;

typedef void (* FPDEV_CALLBACK)(const intptr_t sParam, const uintptr_t uParam);
typedef void (* FPDEV_CALLBACKF)(const intptr_t sParam, const float fParam);
typedef void (* FPDEV_CALLBACKD)(const intptr_t sParam, const double dfParam);
bool initDeviceLibrary(void);
#endif//LIB_DEVICE_H
/* [] END OF FILE */
