/**
 * @file  UE878.h
 * @brief 受信器モジュール定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2019 All Rights Reserved.
 */
#ifndef INC_UE878_H
#define INC_UE878_H

//#include <stdint.h>
#include <string.h>    
#include "project.h"
#include "dev_uart.h"
#include "KenzaiActRegist.h"
#include "KenzaiState.h"
#include "KenzaiOutputInfo.h"    
#include "ActLogManager.h"

#define UE878RCV_BIN_TERMINATOR 0xAA
#define UE878RCV_STR_TERMINATOR 0x0A

/** 
 * @typedef enum UE878_RCV_COMMAND
 * @brief UARTで受信するコマンド。受信器→機器
 * @remark myCommandCheckTimer.c から移動
 */
typedef enum tag_enum_ue878rcv_command {
    UE878RCVCMD_WAIT = 0,
    UE878RCVCMD_OPEN = 1,
    UE878RCVCMD_CLOSE = 2,
    UE878RCVCMD_STOP = 3,
    UE878RCVCMD_MEMORY2,
    UE878RCVCMD_MEMORY1,
    UE878RCVCMD_SAIFU_CLOSE,
    UE878RCVCMD_MOMENTARY_OPEN,
    UE878RCVCMD_MOMENTARY_CLOSE,  // 8
    UE878RCVCMD_INITIALIZE,           // 未使用
    UE878RCVCMD_INITIALIZE_CANCEL,    // 未使用
    UE878RCVCMD_FAVORITE_SET,         // 未使用
    UE878RCVCMD_FAVORITE_CANCEL,      // 未使用
    UE878RCVCMD_CHANNEL_REGIST,       // 未使用
    UE878RCVCMD_REMOCON_REGIST,       // 未使用
    UE878RCVCMD_ALL_CLEAR,            // 未使用
    UE878RCVCMD_MEMORY1_SET,      // 16
    UE878RCVCMD_INITIALIZE_START, // 17
    UE878RCVCMD_CORRECT_POS,          // 未使用
    UE878RCVCMD_CORRECT_OBSTACLE,     // 未使用
    UE878RCVCMD_SOFT_RESET1,          // 未使用
    UE878RCVCMD_SOFT_RESET2,      // 21
    UE878RCVCMD_CLEAR_LEARN,      // 22 soft reset 3
    UE878RCVCMD_CORRECT_SAIFU_CLOSE,
    UE878RCVCMD_ANSWER_BACK,      // 24
    UE878RCVCMD_ACQUIRE_BASE_POWER,
    UE878RCVCMD_ARBITRARY_SET,
    UE878RCVCMD_INSPECT,
    UE878RCVCMD_28,
    UE878RCVCMD_29,
    UE878RCVCMD_30,
    UE878RCVCMD_31,
    UE878RCVCMD_MAX   // 32
} UE878_RCV_COMMAND;

typedef struct tag_ue878rcv_packet {
    uint8_t cmd;
    uint8_t term;
} UE878RCVPKT, *PUE878RCVPKT;


#define UE878SND_BIN_TERMINATOR 0xA8
typedef enum tag_ue878_info_bit {
    UE878SNDINFO_ABNORMAL = 0,
    UE878SNDINFO_POWEROFF,
    UE878SNDINFO_AIR,
    UE878SNDINFO_CLOSE_OPEN,
    UE878SNDINFO_INIT,
    UE878SNDINFO_PAIRING,
    UE878SNDINFO_TROUBLE,
    UE878SNDINFO_ACK
} UE878INFO_BIT;
#define UE878SNDINFOBIT_ACK (0x80)
#define UART_SEND_ACCIDENT_SHIFT (6)
#define UART_SEND_PAIRING_ADMIT_SHIFT (5)
#define UART_SEND_INITIALIZED_INFO_SHIFT (4)
#define UART_SEND_OPENPOS_SHIFT (3)
#define UART_SEND_POSINFO_SHIFT (2)
#define UART_SEND_TEIDEN_SHIFT (1)
#define UART_SEND_FAILURE_SHIFT (0)

typedef struct tag_ue878snd_packet {
    uint8_t info;
    uint8_t times_h;
    uint8_t times_l;
    uint8_t term;
} UE878SNDPKT, *PUE878SNDPKT;


typedef enum {
    UE878TYPE_DECOWINDOW = 0,
    UE878TYPE_ITALIA,
    UE878TYPE_AIRIS,
    UE878TYPE_LOOFWINDOW,
    UE878TYPE_SUNSHADE,
    UE878TYPE_UNDEF5,
    UE878TYPE_UNDEF6,
    UE878TYPE_UNDEF7,
    UE878TYPE_NUM
} UE878_TYPES;

//=====================================
//        Function Prototypes 
//=====================================
int32_t handlerUE878CommandRecv(void);
int32_t ue878_getCommand(void);
bool    ue878_init(uint8_t channel);
bool    ue878_SendData(ESEND_KIND const kind, const SKenzaiState* const state, const SKenzaiOutputInfo* const info);

#endif//INC_UE878_H
/* [] END OF FILE */
