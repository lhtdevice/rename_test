﻿/**
 * @file  dev_pwm.c
 * @brief PWMデバイス実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#include "project.h"
#include "dev_pwm.h"

#if USE_DEV_PWM

typedef bool (* FPPWM_INIT)(void);
typedef bool (* FPPWM_SET)(const uint32_t rate, const uint32_t period);
typedef bool (* FPPWM_SET2)(const uint32_t rate, const uint32_t rate2, const uint32_t period);
typedef bool (* FPPWM_GET)(uint32_t * const rate, uint32_t * const period);
typedef bool (* FPPWM_GET2)(uint32_t * const rate, uint32_t * const rate2, uint32_t * const period);
typedef bool (* FPPWM_OUT)(const uint8_t level);

typedef struct s_pwm_struct {
    FPDEV_CALLBACK fpCallback;
    uint32_t rate;
    uint32_t rate2;
    uint32_t period;
    bool assigned;
} DEV_PWM;

/********************* ↓デバイス依存関数プロトタイプ↓ *************************/
// モーター制御
static bool PWMMOTOR_Initialize(void);
static bool PWMMOTOR_Finalize(void);
static bool PWMMOTOR_Start(void);
static bool PWMMOTOR_Stop(void);
static bool PWMMOTOR_Get(uint32_t * const rate, uint32_t * const rate2, uint32_t * const period);
static bool PWMMOTOR_Set(const uint32_t rate, const uint32_t rate2, const uint32_t period);
//static bool PWMMOTOR_Output(const uint8_t level);
/********************* ↑デバイス依存関数プロトタイプ↑ *************************/

static DEV_PWM dev[NUM_PWM_DEVICES];
static FPPWM_INIT fpInit[NUM_PWM_DEVICES]  = {PWMMOTOR_Initialize, NULL};
static FPPWM_INIT fpFine[NUM_PWM_DEVICES]  = {PWMMOTOR_Finalize,   NULL};
static FPPWM_INIT fpStart[NUM_PWM_DEVICES] = {PWMMOTOR_Start,      NULL};
static FPPWM_INIT fpStop[NUM_PWM_DEVICES]  = {PWMMOTOR_Stop,       NULL};
static FPPWM_GET2 fpGet[NUM_PWM_DEVICES]   = {PWMMOTOR_Get,        NULL};
static FPPWM_SET2 fpSet[NUM_PWM_DEVICES]   = {PWMMOTOR_Set,        NULL};
//static FPPWM_OUT  fpOut[NUM_PWM_DEVICES]   = {PWMMOTOR_Output,     NULL};

/**
 * @brief PWMデバイスライブラリを初期化します。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void dev_pwm_ClearLibrary(void)
{
    static bool bClear = false;
    if (!bClear)
    {
        memset(dev, 0, sizeof(dev));
        bClear = true;
    }
}

bool dev_pwm_Initialize(const uint8_t ch, const FPDEV_CALLBACK fpCallback)
{
    if ((ch >= NUM_PWM_DEVICES) || dev[ch].assigned) return false;
    dev[ch].fpCallback = fpCallback;
    dev[ch].assigned = fpInit[ch]() ? fpInit[ch]() : false;
    return dev[ch].assigned;
}

bool dev_pwm_Finalize(const uint8_t ch)
{
    if ((ch >= NUM_PWM_DEVICES) || !dev[ch].assigned) return false;
    if (fpFine[ch]) fpFine[ch]();
    memset(&dev[ch], 0, sizeof(DEV_PWM));
    return true;
}

bool dev_pwm_start(const uint8_t ch)
{
    if ((ch >= NUM_PWM_DEVICES) || !dev[ch].assigned) return false;
    return fpStart[ch]();
}

bool dev_pwm_stop(const uint8_t ch)
{
    if ((ch >= NUM_PWM_DEVICES) || !dev[ch].assigned) return false;
    return fpStop[ch]();
}

bool dev_pwm_update2(const uint8_t ch, const uint32_t rate, const uint32_t rate2, const uint32_t period)
{
    if ((ch >= NUM_PWM_DEVICES) || !dev[ch].assigned) return false;
    dev[ch].rate = rate;
    dev[ch].rate2 = rate2;
    dev[ch].period = period;
    return fpSet[ch](rate, rate2, period);
}

bool dev_pwm_getParameter2(const uint8_t ch, uint32_t * const rate, uint32_t * const rate2, uint32_t * const period)
{
    if ((ch >= NUM_PWM_DEVICES) || !dev[ch].assigned) return false;
    fpGet[ch](rate, rate2, period);
    dev[ch].rate = *rate;
    dev[ch].rate2 = *rate2;
    dev[ch].period = *period;
    return true;
}

//bool dev_pwm_output(const uint8_t ch, const uint8_t level)
//{
//    if ((ch >= NUM_PWM_DEVICES) || !dev[ch].assigned) return false;
//    return fpOut[ch](level);
//}

/********************* ↓ハードウェア依存部↓ ***************************/
static bool PWMMOTOR_Initialize(void)
{
    // nothing to do
    return true;
}

static bool PWMMOTOR_Finalize(void)
{
    PWM_Stop();
    return true;
}

static bool PWMMOTOR_Start(void)
{
    PWM_Start();
    return true;
}

static bool PWMMOTOR_Stop(void)
{
    PWM_Stop();
    return true;
}

static bool PWMMOTOR_Set(const uint32_t rate, const uint32_t rate2, const uint32_t period)
{
    (void)rate; (void)period;
    if ((UINT_MAX < period) || (rate > period)) return false;
    PWM_WritePeriod(period);
    PWM_WriteCompare(rate, rate2);
    return true;
}

static bool PWMMOTOR_Get(uint32_t * const rate, uint32_t * const rate2, uint32_t * const period)
{
	uint16_t r1, r2;
    *period = PWM_ReadPeriod();
    if (PWM_ReadCompare(&r1, &r2))
	{
		*rate = r1;
		*rate2 = r2;
	}
    return true;
}

#if 0
static bool PWMMOTOR_Output(const uint8_t level)
{
    PWM_Stop();
    if (PWM_LEVEL_LOW == level)
        P1 = P1 & ~_40_Pn6_OUTPUT_1;
    else
        P1 = P1 | _40_Pn6_OUTPUT_1;
    return true;
}
#endif

/********************* ↑ハードウェア依存部↑ ***************************/
#endif//USE_DEV_PWM
