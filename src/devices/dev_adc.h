/**
 * @file  dev_adc.h
 * @brief ADC定義ファイル
 * @remark アナログ値を扱うデバイスを定義する。
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#ifndef DEV_ADC_H
#define DEV_ADC_H

#include "LibDevice.h"
#include "float.h"

enum tag_adc_index {
    ADCIDX_DC = 0,
	ADCIDX_THERMO,
    ADCIDX_AC,
    ADCIDX_MAX
};
#define NUM_ADC_DEVICES  ADCIDX_MAX

#define ADC_ERROR   (-1.0)
#define ADC_SUCCESS (100.0)
#define ADC_VREF    (5.0)
#define ADC_QUANTIZATION_BITS (10)

void    dev_adc_ClearLibrary(void);
bool    dev_adc_Initialize(const uint8_t channel, const FPDEV_CALLBACKF fpIntr);
bool    dev_adc_Finalize(const uint8_t channel);
float   dev_adc_Read(const uint8_t channel);
float   dev_adc_Start(const uint8_t channel, const DEVIO_TYPE iotype);
int32_t dev_adc_Cancel(const uint8_t channel);

#endif//DEV_ADC_H