﻿/**
 * @file  Motor.h
 * @brief モーター実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */
#include "project.h"
#include "LibDevice.h"
#include "main.h"
#include "dev_pwm.h"
#include "dev_digital.h"
#include "Motor.h"

typedef struct motor_param {
    uint32_t rate_close;
    uint32_t rate_open;
    uint32_t period;
} MOTOR_PARAM;

MOTOR_PARAM param[PWMIDX_MAX] = {{0, 0, 0}, {0, 0, 0}};

void motor_Init(void)
{
	if (!g_ac_or_dc)
	{
	    dev_pwm_getParameter2(PWMIDX_DCMOTOR, &param[PWMIDX_DCMOTOR].rate_close, &param[PWMIDX_DCMOTOR].rate_open, &param[PWMIDX_DCMOTOR].period);
		param[PWMIDX_DCMOTOR].rate_close = (param[PWMIDX_DCMOTOR].period * g_dcm_rate) / 100;
		param[PWMIDX_DCMOTOR].rate_open  = (param[PWMIDX_DCMOTOR].period * g_dcm_rate) / 100;
	}
	motor_Free();
}

void motor_Brake(void)
{
	if (!g_ac_or_dc)
	{
	    dev_pwm_update2(PWMIDX_DCMOTOR, 0, 0, param[PWMIDX_DCMOTOR].period);
		dev_digital_Write(GPIOIDX_CLOSE, 1);
		dev_digital_Write(GPIOIDX_OPEN, 1);
	}
}

void motor_Free(void)
{
	if (g_ac_or_dc)
	{
		dev_digital_Write(GPIOIDX_ACMOPEN,  0);
		dev_digital_Write(GPIOIDX_ACMCLOSE, 0);
	}
	else
	{
	    dev_pwm_update2(PWMIDX_DCMOTOR, 0, 0, param[PWMIDX_DCMOTOR].period);
		dev_digital_Write(GPIOIDX_CLOSE, 0);
		dev_digital_Write(GPIOIDX_OPEN, 0);
//	    dev_pwm_stop(PWMIDX_DCMOTOR);
	}
}

void motor_Off(void)
{
	motor_Free();
}

void motor_On(const bool dir_normal)
{
	if (g_ac_or_dc)
	{
	    if (dir_normal)
		{
			dev_digital_Write(GPIOIDX_ACMOPEN,  0);
			dev_digital_Write(GPIOIDX_ACMCLOSE, 1);
		}
		else
		{
			dev_digital_Write(GPIOIDX_ACMOPEN,  1);
			dev_digital_Write(GPIOIDX_ACMCLOSE, 0);
		}
	}
	else
	{
	    dev_pwm_stop(PWMIDX_DCMOTOR);
	    if (dir_normal)
	    {
			dev_digital_Write(GPIOIDX_OPEN, 0);
			dev_digital_Write(GPIOIDX_CLOSE, 1);
	        dev_pwm_update2(PWMIDX_DCMOTOR, param[PWMIDX_DCMOTOR].rate_close, 0, param[PWMIDX_DCMOTOR].period);
	    }
	    else
	    {
			dev_digital_Write(GPIOIDX_CLOSE, 0);
			dev_digital_Write(GPIOIDX_OPEN, 1);
	        dev_pwm_update2(PWMIDX_DCMOTOR, 0, param[PWMIDX_DCMOTOR].rate_open, param[PWMIDX_DCMOTOR].period);
	    }
	    dev_pwm_start(PWMIDX_DCMOTOR);
	}
}

void motor_Set(const uint16_t rate_per_mille)
{
	if (g_ac_or_dc) return;
    if (1000 < rate_per_mille) return;
    param[PWMIDX_DCMOTOR].rate_close = (param[PWMIDX_DCMOTOR].period * rate_per_mille) / 1000;
    param[PWMIDX_DCMOTOR].rate_open  = (param[PWMIDX_DCMOTOR].period * rate_per_mille) / 1000;
}

/* [] END OF FILE */
