/**
 * @file  dev_digital.c
 * @brief デジタルIO実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#include "project.h"
#include "dev_digital.h"

#if USE_DEV_DIGITAL

typedef bool    (* FPDEVIO_INIT)(void);
typedef uint8_t (* FPDEVIO_READ)(void);
typedef void    (* FPDEVIO_WRITE)(const uint8_t);

typedef struct s_gpio_struct {
    FPDEV_CALLBACK fpCallback;
    DEVIO_TYPE io_type;
    IOINT_TYPE int_type;
    bool assigned;
} DEV_GPIO;

/********************* ↓デバイス依存関数プロトタイプ↓ *************************/
// モードswitch
static uint8_t Mode_Read(void);
// 降雨センサ
static bool Rain_Initialize(void);
static uint8_t Rain_Read(void);
// 温度センサ
//static bool Thermo_Initialize(void);
//static uint8_t Thermo_Read(void);
// Limit SWs
static uint8_t LSW_Open_Read(void);
static uint8_t LSW_Close_Read(void);
// DCモータ
static bool Open_Initialize(void);
static void Open_Write(const uint8_t);
static bool Close_Initialize(void);
static void Close_Write(const uint8_t);
// RS485 IRQ
static bool RS485IRQ_Initialize(void);
static void RS485IRQ_Write(const uint8_t);
// RS485 CS
static bool RS485CS_Initialize(void);
static void RS485CS_Write(const uint8_t);
// Relay1
//static bool Relay1_Initialize(void);
//static void Relay1_Write(const uint8_t);
// Relay2
//static bool Relay2_Initialize(void);
//static void Relay2_Write(const uint8_t);
// AC L1
static bool ACL1_Initialize(void);
static void ACL1_Write(const uint8_t);
// AC L2
static bool ACL2_Initialize(void);
static void ACL2_Write(const uint8_t);
/********************* ↑デバイス依存関数プロトタイプ↑ *************************/

static DEV_GPIO dev[NUM_GPIO_DEVICES];
static FPDEVIO_INIT  fpInit[NUM_GPIO_DEVICES]   = {
	NULL,                NULL,               NULL,              NULL,              NULL,             NULL,
	Rain_Initialize,     /*Thermo_Initialize,*/  NULL,              NULL,   Open_Initialize,  Close_Initialize,
	RS485IRQ_Initialize, RS485CS_Initialize, /*Relay1_Initialize, Relay2_Initialize,*/ ACL1_Initialize,  ACL2_Initialize};
static FPDEVIO_INIT  fpFine[NUM_GPIO_DEVICES]   = {
	NULL,                NULL,               NULL,              NULL,              NULL,             NULL,
	NULL,                /*NULL,*/               NULL,              NULL,              NULL,             NULL,
	NULL,                NULL,               /*NULL,              NULL,*/              NULL,             NULL,};
static FPDEVIO_READ  fpRead[NUM_GPIO_DEVICES]   = {
	Mode_Read,           Mode_Read,          Mode_Read,         Mode_Read,         Mode_Read,        Mode_Read,
	Rain_Read,           /*Thermo_Read,*/        LSW_Open_Read,     LSW_Close_Read,    NULL,             NULL,
	NULL,                NULL,               /*NULL,              NULL,*/              NULL,             NULL,};
static FPDEVIO_WRITE fpWrite[NUM_GPIO_DEVICES]  = {
	NULL,                NULL,               NULL,              NULL,              NULL,             NULL,
	NULL,                /*NULL,*/               NULL,              NULL,              Open_Write,       Close_Write,
	RS485IRQ_Write,      RS485CS_Write,      /*Relay1_Write,      Relay2_Write,*/      ACL1_Write,       ACL2_Write,};
static FPDEVIO_READ  fpWait[NUM_GPIO_DEVICES]   = {
	NULL,                NULL,               NULL,              NULL,              NULL,             NULL,
	NULL,                /*NULL,*/               NULL,              NULL,              NULL,             NULL,
	NULL,                NULL,               /*NULL,              NULL,*/              NULL,             NULL,};
static FPDEVIO_INIT  fpCancel[NUM_GPIO_DEVICES] = {
	NULL,                NULL,               NULL,              NULL,              NULL,             NULL,
	NULL,                /*NULL,*/               NULL,              NULL,              NULL,             NULL,
	NULL,                NULL,               /*NULL,              NULL,*/              NULL,             NULL,};

void dev_digital_ClearLibrary(void)
{
    static bool bClear = false;
    if (!bClear)
    {
        memset(dev, 0, sizeof(dev));
        bClear = true;
    }
}

bool dev_digital_Initialize(const uint8_t channel, const FPDEV_CALLBACK fpIntr)
{
    if ((channel >= NUM_GPIO_DEVICES) || dev[channel].assigned) return false;
    dev[channel].fpCallback = fpIntr;
    dev[channel].assigned = fpInit[channel] ? fpInit[channel]() : true;
    return dev[channel].assigned;
}

bool dev_digital_Finalize(const uint8_t channel)
{
    if ((channel >= NUM_GPIO_DEVICES) || !dev[channel].assigned) return false;
    if (fpFine[channel]) fpFine[channel]();
    memset(&dev[channel], 0, sizeof(DEV_GPIO));
    return true;
}

int32_t dev_digital_Read(const uint8_t channel)
{
    if ((channel >= NUM_GPIO_DEVICES) || !dev[channel].assigned) return DEVRESULT_FAILURE;
    return fpRead[channel] ? fpRead[channel]() : DEVRESULT_FAILURE;
}

int32_t dev_digital_Write(const uint8_t channel, const uint32_t data)
{
    if ((channel >= NUM_GPIO_DEVICES) || !dev[channel].assigned) return DEVRESULT_FAILURE;
    if (!fpWrite[channel]) return DEVRESULT_NULL;
	fpWrite[channel]((uint8_t)data);
	return DEVRESULT_SUCCESS;
}

int32_t dev_digital_Wait(const uint8_t channel, const uint32_t data, const DEVIO_TYPE iotype, const IOINT_TYPE inttype)
{
    if ((channel >= NUM_GPIO_DEVICES) || !dev[channel].assigned || !fpRead[channel]) return DEVRESULT_FAILURE;
    if ((DEVIO_SYNC > iotype) || (DEVIO_ASYNC_CONT < iotype)) return DEVRESULT_PARAMETER;
    if (DEVIO_SYNC == iotype)
    {
        while ((int32_t)data != fpRead[channel]()) NOP();
    }
    else
    {
        if (!fpWait[channel]) return DEVRESULT_FAILURE;
        if ((IOINT_NONE >= inttype) || (IOINT_BOTH < inttype)) return DEVRESULT_PARAMETER;
        dev[channel].io_type = iotype;
        dev[channel].int_type = inttype;
        fpWait[channel]();
    }
    return DEVRESULT_SUCCESS;
}

int32_t dev_digital_Cancel(const uint8_t channel)
{
    if ((channel >= NUM_GPIO_DEVICES) || !dev[channel].assigned) return DEVRESULT_FAILURE;
    fpCancel[channel]();
    return DEVRESULT_SUCCESS;
}
/********************* ↑public IO関数↑ *************************/

/********************* ↓ハードウェア依存部↓ ***************************/
static uint8_t Mode_Read(void)
{
    return P5 & 0x3F;
}

static bool Rain_Initialize(void)
{
    return true;
}

static uint8_t Rain_Read(void)
{
    return ((P7 & _80_Pn7_OUTPUT_1) != 0);
}

/*
static bool Thermo_Initialize(void)
{
    return true;
}

static uint8_t Thermo_Read(void)
{
    return ((P2 & _80_Pn7_OUTPUT_1) != 0);
}
*/

static uint8_t LSW_Open_Read(void)
{
	// Limit Switch OpenはノーマルON
	uint8_t v = (P7 & _10_Pn4_OUTPUT_1);
    return (v == 0x10);
}

static uint8_t LSW_Close_Read(void)
{
//    return (~P6 & (_04_Pn2_OUTPUT_1 | _08_Pn3_OUTPUT_1));
	// Limit Switch CloseはノーマルOFF
	uint8_t v = (~P7 & _20_Pn5_OUTPUT_1);
    return (v == 0x20);
}

static bool Open_Initialize(void)
{
	P7 &= ~_02_Pn1_OUTPUT_1;
    return true;
}

static void Open_Write(uint8_t v)
{
	if (v)
		P7 |= _02_Pn1_OUTPUT_1;
	else
		P7 &= ~_02_Pn1_OUTPUT_1;
}

static bool Close_Initialize(void)
{
	P7 &= ~_01_Pn0_OUTPUT_1;
    return true;
}

static void Close_Write(uint8_t v)
{
	if (v)
		P7 |= _01_Pn0_OUTPUT_1;
	else
		P7 &= ~_01_Pn0_OUTPUT_1;
}

static bool RS485IRQ_Initialize(void)
{
    return true;
}

static void RS485IRQ_Write(uint8_t v)
{
	if (v)
		P13 |= _01_Pn0_OUTPUT_1;
	else
		P13 &= ~_01_Pn0_OUTPUT_1;
}

static bool RS485CS_Initialize(void)
{
    return true;
}

static void RS485CS_Write(uint8_t v)
{
	if (v)
		P0 |= _02_Pn1_OUTPUT_1;
	else
		P0 &= ~_02_Pn1_OUTPUT_1;
}

/***********
static bool Relay1_Initialize(void)
{
    return true;
}

static void Relay1_Write(uint8_t v)
{
	if (v)
		P7 |= _10_Pn4_OUTPUT_1;
	else
		P7 &= ~_10_Pn4_OUTPUT_1;
}

static bool Relay2_Initialize(void)
{
    return true;
}

static void Relay2_Write(uint8_t v)
{
	if (v)
		P7 |= _20_Pn5_OUTPUT_1;
	else
		P7 &= ~_20_Pn5_OUTPUT_1;
}
***********/

static bool ACL1_Initialize(void)
{
	P1 &= ~_20_Pn5_OUTPUT_1;
    return true;
}

static void ACL1_Write(uint8_t v)
{
	uint8_t vv = P1;
	if (v)
		vv |= _20_Pn5_OUTPUT_1;
	else
		vv &= ~_20_Pn5_OUTPUT_1;
	P1 = vv;
}

static bool ACL2_Initialize(void)
{
	P1 &= ~_40_Pn6_OUTPUT_1;
    return true;
}

static void ACL2_Write(uint8_t v)
{
	if (v)
		P1 |= _40_Pn6_OUTPUT_1;
	else
		P1 &= ~_40_Pn6_OUTPUT_1;
}
/********************* ↑ハードウェア依存部↑ ***************************/

#endif//USE_DEV_DIGITAL
/* [] END OF FILE */
