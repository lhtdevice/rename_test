﻿/**
 * @file  dev_encoder.c
 * @brief エンコーダデバイス実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2019 All Rights Reserved.
 */

#include "project.h"
#include "dev_encoder.h"
#include "main.h"

#if USE_DEV_ENC

#define DEV_ENC_SMA_NUM 8

typedef bool (* FPENC_INIT)(void);

typedef struct s_enc_struct {
    FPDEV_CALLBACKF fpCallback;
    float    speed[DEV_ENC_SMA_NUM]; // 移動平均算出用バッファ。単位は(1/4エンコーダ周期)/ms
    float    mm_per_encoder; // 単位はmm/(1/4エンコーダ周期)
    uint32_t tm_last;        // 前回の割り込み発生時間[ms]
    uint8_t  num_speed_data; // 移動平均データ数
    uint8_t  ptr_speed_next; // 次の移動平均データ格納インデックス
    uint8_t  dir;            // 動作方向
    bool     assigned;
} DEV_ENC;

/********************* ↓デバイス依存関数プロトタイプ↓ *************************/
// エンコーダ制御
static bool ENCODER_Initialize(void);
static bool ENCODER_Finalize(void);
static bool ENCODER_Start(void);
static bool ENCODER_Stop(void);
/********************* ↑デバイス依存関数プロトタイプ↑ *************************/

static DEV_ENC dev[NUM_ENC_DEVICES];
static FPENC_INIT fpInit[NUM_ENC_DEVICES]  = {ENCODER_Initialize};
static FPENC_INIT fpFine[NUM_ENC_DEVICES]  = {ENCODER_Finalize};
static FPENC_INIT fpStart[NUM_ENC_DEVICES] = {ENCODER_Start};
static FPENC_INIT fpStop[NUM_ENC_DEVICES]  = {ENCODER_Stop};

/**
 * @brief エンコーダデバイスライブラリを初期化します。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void dev_enc_ClearLibrary(void)
{
    static bool bClear = false;
    if (!bClear)
    {
        memset(dev, 0, sizeof(dev));
        bClear = true;
    }
}

/**
 * @brief エンコーダデバイスを初期化します。
 * @param[in] ch エンコーダデバイス番号
 * @param[in] fpCallback エンコーダ割り込みコールバック関数
 * @param[out] -
 * @retval bool 初期化結果
 */
bool dev_enc_Initialize(const uint8_t ch, const FPDEV_CALLBACKF fpCallback)
{
    if ((ch >= NUM_ENC_DEVICES) || dev[ch].assigned) return false;
    dev[ch].fpCallback = fpCallback;
    dev[ch].assigned = fpInit[ch]() ? fpInit[ch]() : false;
    return dev[ch].assigned;
}

/**
 * @brief エンコーダデバイスを終了します。
 * @param[in] ch エンコーダデバイス番号
 * @param[out] -
 * @retval bool 終了処理結果
 */
bool dev_enc_Finalize(const uint8_t ch)
{
    if ((ch >= NUM_ENC_DEVICES) || !dev[ch].assigned) return false;
    if (fpFine[ch]) fpFine[ch]();
    memset(&dev[ch], 0, sizeof(DEV_ENC));
    return true;
}

/**
 * @brief エンコーダデバイスを開始します。
 * @param[in] ch エンコーダデバイス番号
 * @param[out] -
 * @retval bool 開始処理結果
 */
bool dev_enc_start(const uint8_t ch)
{
    if ((ch >= NUM_ENC_DEVICES) || !dev[ch].assigned) return false;
    dev[ch].tm_last = getTime();
    return fpStart[ch]();
}

/**
 * @brief エンコーダデバイスを停止します。
 * @param[in] ch エンコーダデバイス番号
 * @param[out] -
 * @retval bool 停止処理結果
 */
bool dev_enc_stop(const uint8_t ch)
{
    if ((ch >= NUM_ENC_DEVICES) || !dev[ch].assigned) return false;
    return fpStop[ch]();
}

/**
 * @brief 速度の移動平均を求める。
 * @param[in] ch エンコーダデバイス番号
 * @param[out] -
 * @retval float 移動平均[(1/4エンコーダ周期) / ms]
 */
static float getSpeed(const uint8_t ch)
{
    if ((ch >= NUM_ENC_DEVICES) || !dev[ch].assigned) return 0.0;
    float sum_speed = 0.0;
    for (uint8_t ii=0; ii<DEV_ENC_SMA_NUM; ii++)
    {
        sum_speed += dev[ch].speed[ii];
    }
    return sum_speed / dev[ch].num_speed_data;
}

/**
 * @brief エンコーダ割り込み。
 * @param[in] ch エンコーダデバイス番号
 * @param[out] -
 * @retval float 移動平均[(1/4エンコーダ周期) / ms]
 */
static void addCount(const uint8_t ch)
{
    static int8_t div_encoder = 1;
    uint32_t tm_now = getTime();
    uint32_t tm_elapse;
    if (tm_now >= dev[ch].tm_last)
        tm_elapse = tm_now - dev[ch].tm_last;
    else
        tm_elapse = tm_now + (ULONG_MAX - dev[ch].tm_last);
    if (0 == tm_elapse)
    {
        div_encoder += 1;
        if (DEV_ENC_SMA_NUM < div_encoder) // タイマ周期が長すぎる
        {
            assert(DEV_ENC_SMA_NUM >= div_encoder);
            haltByError();
        }
        return;
    }
    dev[ch].tm_last = tm_now;
    //float speed = (dev[ch].mm_per_encoder / tm_elapse) / div_encoder; // mm/ms = m/s
    float speed = (float)div_encoder / tm_elapse; // (1/4 encoder count) / mm
    for (int8_t ii=0; ii<div_encoder; ii++)
    {
        dev[ch].speed[dev[ch].ptr_speed_next] = speed;
        dev[ch].ptr_speed_next = (dev[ch].ptr_speed_next + 1) % DEV_ENC_SMA_NUM;
        if (DEV_ENC_SMA_NUM > dev[ch].num_speed_data) dev[ch].num_speed_data += 1;
    }
    if (DIR_CLOSE == dev[ch].dir) div_encoder = div_encoder * -1;
    speed = getSpeed(ch);
    dev[ch].fpCallback(div_encoder, speed);
    div_encoder = 1;
}

uint32_t dev_enc_getStopTime(const uint8_t ch)
{
    if ((ch >= NUM_ENC_DEVICES) || !dev[ch].assigned) return 0;
    uint32_t tm_now = getTime();
    uint32_t tm_elapse;
    if (tm_now >= dev[ch].tm_last)
        tm_elapse = tm_now - dev[ch].tm_last;
    else
        tm_elapse = tm_now + (ULONG_MAX - dev[ch].tm_last);
    return tm_elapse;
}

void dev_enc_setEncoderLength(const uint8_t ch, const float mm_per_enc)
{
    if ((ch >= NUM_ENC_DEVICES) || !dev[ch].assigned) return;
    dev[ch].mm_per_encoder = mm_per_enc;
}

/********************* ↓ハードウェア依存部↓ ***************************/
static uint8_t  state_A = 0; // A相の状態
static uint8_t  state_B = 0; // B相の状態
static uint32_t full_count = 0; // 全閉⇔全開のエンコーダカウント数（1/4周期）

static bool ENCODER_Initialize(void)
{
    // nothing to do
    return true;
}

static bool ENCODER_Finalize(void)
{
    // to do:imprementation
    return true;
}

static bool ENCODER_Start(void)
{
    // to do:imprementation
    return true;
}

static bool ENCODER_Stop(void)
{
    // to do:imprementation
    return true;
}

/**
 * @brief エンコーダA相割り込みルーチン。
 */
void ISR_AncoderA(const uint8_t edge)
{
    state_A = (RISING_EDGE == edge) ? LEVEL_HIGH : LEVEL_LOW;
    addCount(ENCIDX_1);
    return;
}

/**
 * @brief エンコーダB相割り込みルーチン。
 */
void ISR_AncoderB(const uint8_t edge)
{
    uint8_t new_dir = DIR_OPEN;
    if (RISING_EDGE == edge)
    {
        if (LEVEL_LOW == state_A)
        {
            new_dir = DIR_CLOSE;
        }
    }
    dev[ENCIDX_1].dir = new_dir;
    addCount(ENCIDX_1);
    return;
}

/********************* ↑ハードウェア依存部↑ ***************************/
#endif//USE_DEV_UART
