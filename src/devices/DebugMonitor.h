/**
 * @file  DebugMonitor.h
 * @brief デバッグ定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2019 All Rights Reserved.
 */

#ifndef DEBUG_MONITOR_H
#define DEBUG_MONITOR_H

#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>

/**
 * @typedef enum DEBUGLEVEL
 * @brief デバッグレベル定義
 */
typedef enum {
    DEBUGLEVEL_DUMMY = -1, // enum 型をunsignedにしないため（for CS+）
    DEBUGLEVEL_VERBOSE = 0,
    DEBUGLEVEL_DEBUG,
    DEBUGLEVEL_INFORMATION,
    DEBUGLEVEL_WARNING,
    DEBUGLEVEL_ERROR,
    DEBUGLEVEL_MAX
} DEBUGLEVEL;

#ifdef DEBUG
    char *dbprintf(char level, char *str, ...);
    void dbdump(char level, void *buf, size_t size, char *id);
#else
    #define dbprintf(...) ((void)0)
    #define dbdump(a,b,c,d) ((void)0)
#endif
#define DEBUG_OUTPUT_STRING dbprintf

#ifdef DEBUG
    void DebugMonitor_init(void);
    DEBUGLEVEL setDebugLevel(DEBUGLEVEL level);
    DEBUGLEVEL getDebugLevel(void);
    void DEBUG_OUTPUT_STR(const char* str);
    void DEBUG_OUTPUT_WITHUINT(const char* str, uint32_t number);
#else
    #define DebugMonitor_init()     ((void)0)
    #define setDebugLevel(a)         ((void)0)
    #define getDebugLevel()         ((void)0)
    #define DEBUG_OUTPUT_STR(a)      ((void)0)
    #define DEBUG_OUTPUT_WITHUINT(a,b) ((void)0)
#endif

#endif//DEBUG_MONITOR_H
/* [] END OF FILE */
