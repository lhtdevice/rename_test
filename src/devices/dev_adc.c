/**
 * @file  dev_adc.c
 * @brief ADC実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#include "project.h"
#include "dev_adc.h"

typedef bool  (* FPDEVIO_INIT)(void);
typedef float (* FPDEVIO_READ)(void);

typedef struct s_gpio_struct {
    FPDEV_CALLBACKF fpCallback;
    DEVIO_TYPE io_type;
    bool assigned;
} DEV_ADC;

uint32_t cnt_adc = 0;

/********************* ↓デバイス依存関数プロトタイプ↓ *************************/
static bool DC_Initialize(void);
static float DC_Read(void);
static float DC_Start(void);
static bool DC_Cancel(void);
static bool Thermo_Initialize(void);
static float Thermo_Read(void);
static float Thermo_Start(void);
static bool Thermo_Cancel(void);
static bool AC_Initialize(void);
static float AC_Read(void);
static float AC_Start(void);
static bool AC_Cancel(void);

static bool ADC_Start(void);
static bool ADC_Stop(void);
/********************* ↑デバイス依存関数プロトタイプ↑ *************************/

static DEV_ADC dev[NUM_ADC_DEVICES];
static FPDEVIO_INIT  fpInit[NUM_ADC_DEVICES]   = {DC_Initialize, Thermo_Initialize, AC_Initialize};
static FPDEVIO_INIT  fpFine[NUM_ADC_DEVICES]   = {NULL,          NULL,              NULL};
static FPDEVIO_READ  fpRead[NUM_ADC_DEVICES]   = {DC_Read,       Thermo_Read,       AC_Read};
static FPDEVIO_READ  fpStart[NUM_ADC_DEVICES]  = {DC_Start,      Thermo_Start,      AC_Start};
static FPDEVIO_INIT  fpCancel[NUM_ADC_DEVICES] = {DC_Cancel,     Thermo_Cancel,     AC_Cancel};

/********************* ↓public IO関数↓ *************************/
void dev_adc_ClearLibrary(void)
{
    static bool bClear = false;
    if (!bClear)
    {
        memset(dev, 0, sizeof(dev));
        bClear = true;
    }
}

bool dev_adc_Initialize(const uint8_t ch, const FPDEV_CALLBACKF fpIntr)
{
    if ((ch >= NUM_ADC_DEVICES) || dev[ch].assigned) return false;
    dev[ch].fpCallback = fpIntr;
    dev[ch].assigned = fpInit[ch] ? fpInit[ch]() : true;
    return dev[ch].assigned;
}

bool dev_adc_Finalize(const uint8_t ch)
{
    if ((ch >= NUM_ADC_DEVICES) || !dev[ch].assigned) return false;
    if (fpFine[ch]) fpFine[ch]();
    memset(&dev[ch], 0, sizeof(DEV_ADC));
    return true;
}

float dev_adc_Read(const uint8_t ch)
{
    static float    adcval[NUM_ADC_DEVICES];
    static uint32_t cnt[NUM_ADC_DEVICES] = {0, 0};
    if ((ch >= NUM_ADC_DEVICES) || !dev[ch].assigned) return ADC_ERROR;
    if (cnt_adc != cnt[ch])
    {
        cnt[ch] = cnt_adc;
        adcval[ch] = fpRead[ch]();
    }
    return adcval[ch];
}

float dev_adc_Start(const uint8_t ch, const DEVIO_TYPE iotype)
{
    if ((ch >= NUM_ADC_DEVICES) || !dev[ch].assigned || !fpRead[ch]) return ADC_ERROR;
    if ((DEVIO_SYNC > iotype) || (DEVIO_ASYNC_CONT < iotype)) return ADC_ERROR;
    if (DEVIO_SYNC == iotype)
    {
        ADC_Stop();
        uint32_t cnt = cnt_adc;
        fpStart[ch]();
        while (cnt_adc == cnt)
        {
            NOP();
        }
        return dev_adc_Read(ch);
    }
    else
    {
        dev[ch].io_type = iotype;
        return fpStart[ch]();
    }
}

int32_t dev_adc_Cancel(const uint8_t ch)
{
    ADC_Stop();
    return DEVRESULT_SUCCESS;
}
/********************* ↑public IO関数↑ *************************/

/********************* ↓ハードウェア依存部↓ ***************************/
static uint8_t current_channel = _00_AD_INPUT_CHANNEL_0;

static float getAdcResult(void)
{
    uint16_t val;
    //R_ADC_Get_Result(&val);
    return (ADC_VREF * val) / (1 << ADC_QUANTIZATION_BITS);
}

void ISR_ADC(void)
{
    uint8_t ch = ADCIDX_DC;
    cnt_adc += 1;
    if (_00_AD_INPUT_CHANNEL_0 != current_channel)
    {
        ch = ADCIDX_AC;
    }
    if (dev[ch].assigned && dev[ch].fpCallback && (DEVIO_SYNC != dev[ch].io_type))
    {
        if (DEVIO_ASYNC == dev[ch].io_type)
        {
            ADC_Stop();
        }
        dev[ch].fpCallback(ch, getAdcResult());
    }
}

static bool ADC_Start(void)
{
    //R_ADC_Stop();
    //R_ADC_Start();
    return true;
}

static bool ADC_Stop(void)
{
    //R_ADC_Stop();
    return true;
}

static bool DC_Initialize(void)
{
	current_channel = _00_AD_INPUT_CHANNEL_0;
    return true;
}

static float DC_Read(void)
{
    return getAdcResult();
}

static float DC_Start(void)
{
    //R_ADC_Stop();
    //R_ADC_Set_Channel(_00_AD_INPUT_CHANNEL_0);
    //R_ADC_Start();
    return ADC_SUCCESS;
}

static bool DC_Cancel(void)
{
    //R_ADC_Stop();
    return true;
}

static bool AC_Initialize(void)
{
	current_channel = _07_AD_INPUT_CHANNEL_7;
    return true;
}

static float AC_Read(void)
{
    return getAdcResult();
}

static float AC_Start(void)
{
    //R_ADC_Stop();
    //R_ADC_Set_Channel(_07_AD_INPUT_CHANNEL_7);
    //R_ADC_Start();
    return ADC_SUCCESS;
}

static bool AC_Cancel(void)
{
    //R_ADC_Stop();
    return true;
}

static bool Thermo_Initialize(void)
{
    // nothing to do
    return true;
}

static float Thermo_Read(void)
{
    return getAdcResult();
}

static float Thermo_Start(void)
{
    //R_ADC_Stop();
    //R_ADC_Set_Channel(_03_AD_INPUT_CHANNEL_3);
    //R_ADC_Start();
    return ADC_SUCCESS;
}

static bool Thermo_Cancel(void)
{
   // R_ADC_Stop();
    return true;
}

/********************* ↑ハードウェア依存部↑ ***************************/
