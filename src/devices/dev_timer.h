/**
 * @file  dev_timer.h
 * @brief タイマーデバイス定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#ifndef DEV_TIMER_H
#define DEV_TIMER_H

#include "LibDevice.h"
#include "r_cg_rtc.h"

enum tag_timer_index {
    TIMERIDX_1MS = 0,
    TIMERIDX_1S,
    TIMERIDX_MAX
};
#define NUM_TIMER_DEVICES TIMERIDX_MAX

#define NUM_REGIST_ENTRY     8
#define TIMER_ID_ERROR       -1

typedef enum tag_timer_callback_type {
    TIMERCB_NONE = 0,
    TIMERCB_ONETIME,
    TIMERCB_CONTINUOUS,
} TIMERCB_TYPE;

struct tm {
	int tm_sec;
	int tm_min;
	int tm_hour;
	int tm_mday;
	int tm_mon;
	int tm_year;
	int tm_wday;
	int tm_yday;
	int tm_isdst;
};

void     dev_timer_ClearLibrary(void);
bool     dev_timer_Initialize(const uint8_t channel, const FPDEV_CALLBACK fpCallback);
bool     dev_timer_Finalize(const uint8_t channel);
bool     dev_timer_start(const uint8_t channel);
bool     dev_timer_stop(const uint8_t channel);
uint32_t dev_timer_getTicks(const uint8_t channel);
bool     dev_timer_getRTC(const uint8_t channel, struct tm * const p);
bool     dev_timer_setRTC(const uint8_t channel, const struct tm * const p);
int32_t  dev_timer_setCallback(const uint8_t channel, const uint32_t count, const TIMERCB_TYPE type, FPDEV_CALLBACK fpCallback);
int32_t  dev_timer_cancelCallback(const uint8_t ch, const int32_t id);

#endif//DEV_TIMER_H
/* [] END OF FILE */
