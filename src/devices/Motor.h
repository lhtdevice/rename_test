﻿/**
 * @file  Motor.h
 * @brief モーター定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */
#ifndef INC_MOTOR_H
#define INC_MOTOR_H

#define MOTORDIR_NORMAL  1
#define MOTORDIR_REVERSE 0

void motor_Init(void);
void motor_Brake(void);
void motor_Free(void);
void motor_On(const bool dir_normal);
void motor_Off(void);
void motor_Set(const uint16_t rate_per_mille);

#endif//INC_MOTOR_H
/* [] END OF FILE */
