/**
 * @file DebugMonitor.c
 * @brief デバッグモニタの実装ファイル。宣言のみを記載している関数があるので、使用する場合はこのファイルをインクルードして定義する必要がある。
 * @remarks 実装は見直し
*/
#include "project.h"
#include "DebugMonitor.h"
#include <string.h>

/* 使用するアプリ側で定義必要。 */
extern void DebugOutputWithInt(const char* str, const uint32_t number);
extern void DebugOutput(const char* str);
extern void DebugOutput_init(void);

#ifdef DEBUG

static DEBUGLEVEL db_level = DEBUGLEVEL_DEBUG;
#define DEBUG_MAX_STRLEN 80
static char dbstr[DEBUG_MAX_STRLEN];

/**
 * @brief デバッグ環境を初期化する。
 * @param[in] -
 * @param[out] -
 * @pre -
 * @post -
 * @retval -
*/
void DebugMonitor_init(void)
{
    DebugOutput_init();
}

/**
 * @brief デバッグレベルを設定する
 * @param[in]  デバッグレベル 0≦<DEBUGLEVEL_MAX（値が小さいほど出力が多くなる）
 * @param[out] -
 * @retval 設定されたデバッグレベル
 */
DEBUGLEVEL setDebugLevel(DEBUGLEVEL level)
{
    if ((DEBUGLEVEL_VERBOSE <= level) && (DEBUGLEVEL_MAX > level))
    {
        db_level = level;
    }
    return db_level;
}

/**
 * @brief デバッグレベルを取得する
 * @param[in]  -
 * @param[out] -
 * @retval 設定されているデバッグレベル
 */
DEBUGLEVEL getDebugLevel(void)
{
    return db_level;
}

/**
 * @brief 指定された文字列を、指定された数字を合わせて出力する。
 * @param[in] str 出力する文字列
 * @param[in] number 文字列として出力する数字
 * @param[out] -
 * @pre -
 * @post -
 * @retval -
*/
void DEBUG_OUTPUT_WITHUINT(const char* str, uint32_t number)
{
    DebugOutputWithInt(str, number);
}

/**
 * @brief 指定された文字列を出力する。
 * @param[in] str 出力する文字列
 * @param[out] -
 * @pre -
 * @post -
 * @retval -
*/
void DEBUG_OUTPUT_STR(const char* str)
{
    DebugOutput(str);
}

/**
 * @brief 指定されたフォーマットでデバッグ出力する
 * @param[in]  level：デバッグレベル
 * @param[in]  str：出力フォーマット
 * @param[in]  ...：可変長引数、出力フォーマットに対応する変数
 * @retval char *：出力された文字列
 * @remarks 出力される文字列の最大長はDEBUG_MAX_STRLEN（80）
 */
char *dbprintf(char level, char *str, ...)
{
#ifdef __RL78__ // CS+ では初期化しないとwarningを出すため
    va_list vargs = NULL;
#else
    va_list vargs;
#endif
    char *strret = NULL;

    if (level < db_level) return NULL;
#if 0
    uint32 time = getTime();
    snprintf(dbstr, DEBUG_MAX_STRLEN, "%6lu.%02lu:", time / 100, time % 100);

    dbPutString(dbstr);
#endif
    va_start(vargs, str);
    if (0 < vsnprintf(dbstr, DEBUG_MAX_STRLEN, str, vargs))
    {
        DebugOutput(dbstr);
        strret = dbstr;
    }
    va_end(vargs);

    return strret;
}

/**
 * @brief 指定されたデータをバイナリダンプする
 * @param[in]  level：デバッグレベル
 * @param[in]  buf：出力データ
 * @param[in]  size：出力データのサイズ
 * @param[in]  id：出力データの最初に付加する識別用文字列（最大16文字）
 * @retval -
 */
void dbdump(char level, void *buf, size_t size, char *id)
{
    if (level < db_level) return;

    char *psrc = (char *)buf;
    char *pout = dbstr;
    if (id)
    {
        strncpy(pout, id, 16);
        pout += strlen(id);
    }
	size_t ii;
    for ( ii=0; ii<size; ii++)
    {
        if (0 == (ii % 16))
        {
			sprintf(pout, STR_CRLF"  %02X", psrc[ii]);
			pout += 5;
        }
        else if (0 == (ii % 8))
        {
            sprintf(pout, "  %02X", psrc[ii]);
            pout += 4;
        }
        else {
            sprintf(pout, " %02X", psrc[ii]);
            pout += 3;
        }
        if (15 == (ii % 16))
        {
            DebugOutput(dbstr);
            pout = dbstr;
        }
    }
    sprintf(pout, STR_CRLF);
    DebugOutput(dbstr);
}

#endif//DEBUG
/* [] END OF FILE */
