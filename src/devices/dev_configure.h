﻿/**
 * @file  dev_configure.h
 * @brief デバイス構成設定ファイル
 * @copyright (c) LIXIL Corporation, 2018-2019 All Rights Reserved.
 */

#ifndef DEVB_CONFIGURE_H
#define DEVB_CONFIGURE_H

#define USE_DEV_DIGITAL 1
#define USE_DEV_TIMER   1
#define USE_DEV_UART    1
#define USE_DEV_EEPROM  0
#define USE_DEV_PWM     1
#define USE_DEV_ADC     1
#define USE_DEV_ENC     1

#endif//DEVB_CONFIGURE_H