/**
 * @file  dev_uart.c
 * @brief UARTデバイス実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#include "project.h"
#include "dev_uart.h"

#if USE_DEV_UART

typedef bool    (* FPDEVUART_INIT)(void);
typedef int32_t (* FPDEVUART_READ)(void);
typedef int32_t (* FPDEVUART_WRITE)(const void * const, const size_t);

typedef struct s_uart_struct {
    FPDEV_CALLBACK fpCallbackReceived;
    FPDEV_CALLBACK fpCallbackSent;
    uint8_t bufRecv[MAXSIZE_UART_RECV];
    uint8_t *bufCallback;
    DEVIO_TYPE recv_type;
    DEVIO_TYPE send_type;
    size_t  size_recv;
    size_t  size_recved;
    size_t  size_send;
    bool busy_recv;
    bool busy_send;
    bool assigned;
} DEV_UART;

/********************* ↓デバイス依存関数プロトタイプ↓ *************************/
// デバッグシリアル
static bool DBG_Initialize(void);
static bool DBG_Finalize(void);
static int32_t DBG_Write(const void * const buf, const size_t size);
static bool DBG_Cancel(void);

// RS485（風速計等）
static bool RS485_Initialize(void);
static bool RS485_Finalize(void);
static int32_t RS485_Read(void);
static int32_t RS485_Write(const void * const buf, const size_t size);
static bool RS485_Cancel(void);

// LIXIL受信器（bluetooth/zigbee）
static bool UE878_Initialize(void);
static bool UE878_Finalize(void);
static int32_t UE878_Read(void);
static int32_t UE878_Write(const void * const buf, const size_t size);
static bool UE878_Cancel(void);
/********************* ↑デバイス依存関数プロトタイプ↑ *************************/

static DEV_UART dev[NUM_UART_DEVICES];
static FPDEVUART_INIT  fpInit[NUM_UART_DEVICES]   = {DBG_Initialize, RS485_Initialize, UE878_Initialize};
static FPDEVUART_INIT  fpFine[NUM_UART_DEVICES]   = {DBG_Finalize,   RS485_Finalize,   UE878_Finalize};
static FPDEVUART_READ  fpRead[NUM_UART_DEVICES]   = {NULL,           RS485_Read,       UE878_Read};
static FPDEVUART_WRITE fpWrite[NUM_UART_DEVICES]  = {DBG_Write,      RS485_Write,      UE878_Write};
static FPDEVUART_INIT  fpCancel[NUM_UART_DEVICES] = {DBG_Cancel,     RS485_Cancel,     UE878_Cancel};


/********************* ↓public UART関数↓ *************************/
/**
 * @brief UARTライブラリを初期化する。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void dev_uart_ClearLibrary(void)
{
    static bool bClear = false;
    if (!bClear)
    {
        memset(dev, 0, sizeof(dev));
        bClear = true;
    }
}

/**
 * @brief UARTデバイスの初期化。
 * @param[in] channel UARTチャネル
 * @param[in] fpRecv 受信コールバック関数
 * @param[in] fpSent 送信コールバック関数
 * @param[out] -
 * @retval bool 成功／失敗
 */
bool dev_uart_Initialize(const uint8_t channel, const FPDEV_CALLBACK fpRecv, const FPDEV_CALLBACK fpSent)
{
    if ((channel >= NUM_UART_DEVICES) || dev[channel].assigned) return false;
    dev[channel].fpCallbackReceived = fpRecv;
    dev[channel].fpCallbackSent     = fpSent;
    dev[channel].assigned = fpInit[channel]();
    return dev[channel].assigned;
}

/**
 * @brief UARTデバイスの使用終了処理。
 * @param[in] channel UARTチャネル
 * @param[out] -
 * @retval bool 成功／失敗
 */
bool dev_uart_Finalize(const uint8_t channel)
{
    if ((channel >= NUM_UART_DEVICES) || !dev[channel].assigned) return false;
    fpFine[channel]();
    memset(&dev[channel], 0, sizeof(DEV_UART));
    return true;
}

/**
 * @brief UARTデバイスからデータを受信する。
 * @param[in] channel UARTチャネル
 * @param[in] buf 受信データバッファへのポインタ（呼び出し側でバッファを用意する）
 * @param[in] size 受信データサイズ
 * @param[in] type 受信方式（同期、非同期、非同期連続）
 * @param[out] -
 * @retval int32_t 負値：失敗、0以上：受信データサイズ（非同期の場合は0）
 */
int32_t dev_uart_Read(const uint8_t channel, void * const buf, const size_t size, const DEVIO_TYPE type)
{
    if ((channel >= NUM_UART_DEVICES) || !dev[channel].assigned) return DEVRESULT_FAILURE;
    if ((NULL == buf) || (DEVIO_ASYNC_CONT < type)
     || ((DEVIO_ASYNC <= type) && (NULL == dev[channel].fpCallbackReceived))) return DEVRESULT_PARAMETER;
    if (dev[channel].busy_recv) return DEVRESULT_BUSY;
    dev[channel].size_recv = size;
    dev[channel].recv_type = type;
    dev[channel].busy_recv = true;
    dev[channel].bufCallback = (uint8_t *)buf;
    return (int32_t)fpRead[channel]();
 }

/**
 * @brief UARTデバイスにデータを送信する。
 * @param[in] channel UARTチャネル
 * @param[in] buf 送信データへのポインタ
 * @param[in] size 送信データサイズ
 * @param[in] type 送信方式（同期、非同期）
 * @param[out] -
 * @retval int32_t 負値：失敗、０：成功
 */
int32_t dev_uart_Write(const uint8_t channel, const void * const buf, const size_t size, const DEVIO_TYPE type)
{
    if ((channel >= NUM_UART_DEVICES) || !dev[channel].assigned) return DEVRESULT_FAILURE;
    if ((NULL == buf) || (DEVIO_ASYNC < type)) return DEVRESULT_PARAMETER;
    if (dev[channel].busy_send) return DEVRESULT_BUSY;
    dev[channel].size_send = size;
    dev[channel].send_type = type;
    dev[channel].busy_send = true;
    return fpWrite[channel](buf, size);
}

/**
 * @brief UARTデバイスの送受信を中止する。
 * @remark 同期送受信はマルチスレッドならキャンセルできる。
 * @param[in] channel UARTチャネル
 * @param[out] -
 * @retval int32_t 負値：失敗、０：成功
 */
int32_t dev_uart_Cancel(const uint8_t channel)
{
    if ((channel >= NUM_UART_DEVICES) || !dev[channel].assigned) return DEVRESULT_FAILURE;
    fpCancel[channel]();
    return DEVRESULT_SUCCESS;
}
/********************* ↑public UART関数↑ *************************/

/********************* ↓ハードウェア依存部↓ ***************************/
// 受信器I/F UART

/**
 * @brief 通信モジュールを初期化（起動）する。
 * @param[in] -
 * @param[out] -
 * @retval bool 成功／失敗
 */
static bool UE878_Initialize(void)
{
    R_UART2_Start();
    R_UART2_Receive(dev[UARTIDX_COMMMODULE].bufRecv, 1);
    return true;
}

/**
 * @brief 通信モジュールの使用を終了する。
 * @param[in] -
 * @param[out] -
 * @retval bool 成功／失敗
 */
static bool UE878_Finalize(void)
{
    R_UART2_Stop();
    return true;
}

/**
 * @brief 通信モジュール受信を行う。
 * @param[in] -
 * @param[out] -
 * @retval int32_t 受信データサイズ。
 */
static int32_t UE878_Read(void)
{
    if (DEVIO_SYNC == dev[UARTIDX_COMMMODULE].recv_type)
    {
        while (dev[UARTIDX_COMMMODULE].busy_recv) NOP();
        dev[UARTIDX_COMMMODULE].size_recv = 0;
    }
    return dev[UARTIDX_COMMMODULE].size_recved;
}

/**
 * @brief 通信モジュール非同期送信を起動する。
 * @param[in] -
 * @param[out] -
 * @retval int32_t 結果
 */
static int32_t UE878_Write(const void * const buf, const size_t size)
{
    uint8_t *data = (uint8_t *)buf;
    if (MD_OK == R_UART2_Send(data, size)) return DEVRESULT_SUCCESS;
    else                                   return DEVRESULT_FAILURE;
}

/**
 * @brief 通信モジュール非同期送受信をキャンセルする。
 * @param[in] -
 * @param[out] -
 * @retval bool 成功／失敗
 */
static bool UE878_Cancel(void)
{
    dev[UARTIDX_COMMMODULE].busy_recv = false;
    dev[UARTIDX_COMMMODULE].busy_send = false;
    return true;
}

/**
 * @brief 通信モジュール受信割り込みルーチン。データを受信してデバイスライブラリに渡す。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void ISR_UE878_Recv(void)
{
    // 割り込み時点でデータは格納されている
    if (dev[UARTIDX_COMMMODULE].size_recv > 0) // 受信要求あり
    {
        memcpy(dev[UARTIDX_COMMMODULE].bufCallback, dev[UARTIDX_COMMMODULE].bufRecv, dev[UARTIDX_COMMMODULE].size_recv);
        dev[UARTIDX_COMMMODULE].size_recved = dev[UARTIDX_COMMMODULE].size_recv;
        switch (dev[UARTIDX_COMMMODULE].recv_type)
        {
        case DEVIO_SYNC:
            dev[UARTIDX_COMMMODULE].size_recv = 0;
            dev[UARTIDX_COMMMODULE].busy_recv = false;
            break;
        case DEVIO_ASYNC:
            dev[UARTIDX_COMMMODULE].size_recv = 0;
            dev[UARTIDX_COMMMODULE].busy_recv = false;
            if (NULL != dev[UARTIDX_COMMMODULE].fpCallbackReceived)
            {
                dev[UARTIDX_COMMMODULE].fpCallbackReceived(UARTIDX_COMMMODULE, dev[UARTIDX_COMMMODULE].size_recved);
                // コールバック関数内での再dev_uart_Read()コール可
            }
            break;
        case DEVIO_ASYNC_CONT:
            if (!dev[UARTIDX_COMMMODULE].busy_recv) // 受信キャンセル
            {
                dev[UARTIDX_COMMMODULE].size_recv = 0;
            }
            if (NULL != dev[UARTIDX_COMMMODULE].fpCallbackReceived)
            {
                dev[UARTIDX_COMMMODULE].fpCallbackReceived(UARTIDX_COMMMODULE, dev[UARTIDX_COMMMODULE].size_recved);
                // コールバック関数内での再dev_uart_Cancel()コール可
                R_UART2_Receive(dev[UARTIDX_COMMMODULE].bufRecv, 1);
            }
        default:
            ; //nothing to do
        }
    }
}

/**
 * @brief 通信モジュール送信割り込みルーチン。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void ISR_UE878_Sent(void)
{
    dev[UARTIDX_COMMMODULE].busy_send = false;
    if (NULL != dev[UARTIDX_COMMMODULE].fpCallbackSent)
    {
        dev[UARTIDX_COMMMODULE].fpCallbackSent(UARTIDX_COMMMODULE, dev[UARTIDX_COMMMODULE].size_send);
    }
}


// RS485 I/F UART

/**
 * @brief RS485を初期化（起動）する。
 * @param[in] -
 * @param[out] -
 * @retval bool 成功／失敗
 */
static bool RS485_Initialize(void)
{
    //R_UART1_Start();
    //R_UART1_Receive(dev[UARTIDX_RS485].bufRecv, 1);
    return true;
}

/**
 * @brief RS485の使用を終了する。
 * @param[in] -
 * @param[out] -
 * @retval bool 成功／失敗
 */
static bool RS485_Finalize(void)
{
   // R_UART1_Stop();
    return true;
}

/**
 * @brief RS485受信を行う。
 * @param[in] -
 * @param[out] -
 * @retval int32_t 受信データサイズ。
 */
static int32_t RS485_Read(void)
{
    if (DEVIO_SYNC == dev[UARTIDX_RS485].recv_type)
    {
        while (dev[UARTIDX_RS485].busy_recv) NOP();
        dev[UARTIDX_RS485].size_recv = 0;
    }
    return dev[UARTIDX_RS485].size_recved;
}

/**
 * @brief RS485非同期送信を起動する。
 * @param[in] -
 * @param[out] -
 * @retval int32_t 結果
 */
static int32_t RS485_Write(const void * const buf, const size_t size)
{
    uint8_t *data = (uint8_t *)buf;
    //if (MD_OK == R_UART1_Send(data, size)) return DEVRESULT_SUCCESS;
    //else return DEVRESULT_FAILURE;
    return DEVRESULT_SUCCESS;
}

/**
 * @brief RS485非同期送受信をキャンセルする。
 * @param[in] -
 * @param[out] -
 * @retval bool 成功／失敗
 */
static bool RS485_Cancel(void)
{
    dev[UARTIDX_RS485].busy_recv = false;
    dev[UARTIDX_RS485].busy_send = false;
    return true;
}

/**
 * @brief RS485受信割り込みルーチン。データを受信してデバイスライブラリに渡す。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void ISR_RS485_Recv(void)
{
    // 割り込み時点でデータは格納されている
    if (dev[UARTIDX_RS485].size_recv > 0) // 受信要求あり
    {
        memcpy(dev[UARTIDX_RS485].bufCallback, dev[UARTIDX_RS485].bufRecv, dev[UARTIDX_RS485].size_recv);
        dev[UARTIDX_RS485].size_recved = dev[UARTIDX_RS485].size_recv;
        switch (dev[UARTIDX_RS485].recv_type)
        {
        case DEVIO_SYNC:
            dev[UARTIDX_RS485].size_recv = 0;
            dev[UARTIDX_RS485].busy_recv = false;
            break;
        case DEVIO_ASYNC:
            dev[UARTIDX_RS485].size_recv = 0;
            dev[UARTIDX_RS485].busy_recv = false;
            if (NULL != dev[UARTIDX_RS485].fpCallbackReceived)
            {
                dev[UARTIDX_RS485].fpCallbackReceived(UARTIDX_RS485, dev[UARTIDX_RS485].size_recved);
                // コールバック関数内での再dev_uart_Read()コール可
            }
            break;
        case DEVIO_ASYNC_CONT:
            if (!dev[UARTIDX_RS485].busy_recv) // 受信キャンセル
            {
                dev[UARTIDX_RS485].size_recv = 0;
            }
            if (NULL != dev[UARTIDX_RS485].fpCallbackReceived)
            {
                dev[UARTIDX_RS485].fpCallbackReceived(UARTIDX_RS485, dev[UARTIDX_RS485].size_recved);
                // コールバック関数内での再dev_uart_Cancel()コール可
                R_UART2_Receive(dev[UARTIDX_RS485].bufRecv, 1);
            }
        default:
            ; //nothing to do
        }
    }
}

/**
 * @brief RS485送信割り込みルーチン。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void ISR_RS485_Sent(void)
{
    dev[UARTIDX_RS485].busy_send = false;
    if (NULL != dev[UARTIDX_RS485].fpCallbackSent)
    {
        dev[UARTIDX_RS485].fpCallbackSent(UARTIDX_RS485, dev[UARTIDX_RS485].size_send);
    }
}


/**
 * @brief デバッグ用UARTを初期化（起動）する。
 * @param[in] -
 * @param[out] -
 * @retval bool 成功／失敗
 */
static bool DBG_Initialize(void)
{
    R_UART0_Start();
    //デバッグUARTは受信なし（送信のみ）
    //R_UART1_Receive(dev[UARTIDX_DEBUG].bufRecv, 1);
    return true;
}

/**
 * @brief デバッグ用UARTを停止する。
 * @param[in] -
 * @param[out] -
 * @retval bool 成功／失敗
 */
static bool DBG_Finalize(void)
{
    R_UART0_Stop();
    return true;
}

/**
 * @brief デバッグ用UARTで非同期送信を起動する。
 * @param[in] -
 * @param[out] -
 * @retval int32_t 結果
 */
static int32_t DBG_Write(const void * const buf, const size_t size)
{
    uint8_t *data = (uint8_t *)buf;
    if (MD_OK == R_UART0_Send(data, size)) return DEVRESULT_SUCCESS;
    else                                   return DEVRESULT_FAILURE;
}

/**
 * @brief デバッグ用UARTで非同期送受信をキャンセルする。
 * @param[in] -
 * @param[out] -
 * @retval bool 成功／失敗
 */
static bool DBG_Cancel(void)
{
    dev[UARTIDX_DEBUG].busy_recv = false;
    dev[UARTIDX_DEBUG].busy_send = false;
    return true;
}

/**
 * @brief DEBUG用UART送信割り込みルーチン。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void ISR_Debug_Sent(void)
{
    dev[UARTIDX_DEBUG].busy_send = false;
    if (NULL != dev[UARTIDX_DEBUG].fpCallbackSent)
    {
        dev[UARTIDX_DEBUG].fpCallbackSent(UARTIDX_DEBUG, dev[UARTIDX_DEBUG].size_send);
    }
}

/********************* ↑ハードウェア依存部↑ ***************************/

#endif//USE_DEV_UART
/* [] END OF FILE */
