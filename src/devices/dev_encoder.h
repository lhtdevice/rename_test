﻿/**
 * @file  dev_encoder.h
 * @brief エンコーダデバイス定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2019 All Rights Reserved.
 */

#ifndef DEV_ENC_H
#define DEV_ENC_H

#include "LibDevice.h"

enum tag_enc_index {
    ENCIDX_1 = 0,
    ENCIDX_MAX
};
#define NUM_ENC_DEVICES ENCIDX_MAX

void dev_enc_ClearLibrary(void);
bool dev_enc_Initialize(const uint8_t channel, const FPDEV_CALLBACKF fpCallback);
bool dev_enc_Finalize(const uint8_t channel);

bool     dev_enc_start(const uint8_t channel);
bool     dev_enc_stop(const uint8_t channel);
float    dev_enc_getSpeed(const uint8_t channel);
//int32_t  dev_enc_getPosition(const uint8_t channel);
uint32_t dev_enc_getStopTime(const uint8_t ch);
void     dev_enc_setEncoderLength(const uint8_t channel, const float mm_per_enc);

#endif//DEV_ENC_H
/* [] END OF FILE */
