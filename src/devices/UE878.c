/**
 * @file  UE878.c
 * @brief 受信器モジュール実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */
#include "UE878.h"
#include "dev_uart.h"
#include "KenzaiApplication.h"
#include "DebugMonitor.h"

// #define DEBUG_UE878

static UE878SNDPKT send_pkt;

static void uartPackSendData(ESEND_KIND const kind, const SKenzaiState* const state, const SKenzaiOutputInfo* const info);
static uint8_t getKenzaiType(EKENZAI_EQUIP const equip);

static uint8_t bufRecv[8];
static uint8_t uart_channel = 0;
static uint8_t rcv_data[2];
volatile static bool ready_Cmd = false;

static EKENZAI_ACT_OPERATION actCommand = EKENZAI_ACT_OPERATION_NONE;

/**
 * @brief UARTから受信したコマンドを取得する。取得後はコマンドを無効にする。
 * @param[in] -
 * @param[out] -
 * @retval int32_t 有効な受信コマンド
 */
int32_t ue878_getCommand(void)
{
    if (!ready_Cmd) return EKENZAI_ACT_OPERATION_NONE;
    ready_Cmd = false;
    return actCommand;
}

/**
 * @brief UART受信データ処理関数。UART割り込みからコールされる。
 * @details  ここからアプリケーションのデータ受信処理をコールする。
            受信データは0xaaをターミネーターとしている。もしくはバッファフル。
 * @retval -
 * @remark myCommandCheckTimer.c から移動
 */
int32_t handlerUE878CommandRecv(void)
{
    if (!ready_Cmd) return EKENZAI_ACT_OPERATION_NONE;
    //ready_Cmd = false;
    const uint8_t *pCmd = (const uint8_t *)rcv_data;
#ifdef DEBUG_UE878
    dbprintf(DEBUGLEVEL_DEBUG, "RRecv:%02X %02X"STR_CRLF, pCmd[0], pCmd[1]);
#endif
    //for (int ii=0; ii<2000; ii++) NOP();

    // コマンド変換.
    switch (pCmd[0])
    {
    case UE878RCVCMD_WAIT:
    case UE878RCVCMD_ANSWER_BACK:
        // これはありえる。ACKは返す。
        actCommand = EKENZAI_ACT_OPERATION_NOACT;
        break;
    case UE878RCVCMD_OPEN:
    case 7:
        actCommand = EKENZAI_ACT_OPERATION_OPEN;
        break;
    case UE878RCVCMD_CLOSE:
    case 8:
        actCommand = EKENZAI_ACT_OPERATION_CLOSE;
        break;
    case UE878RCVCMD_STOP:
        actCommand = EKENZAI_ACT_OPERATION_STOP;
        break;
    default:
        actCommand = EKENZAI_ACT_OPERATION_NONE;
    }
    return actCommand;
}

/**
 * @brief UART受信完了ハンドラ。
 * @param[in] sParam UARTチャネル
 * @param[in] uParam 受信データサイズ
 * @param[out] -
 * @retval -
 */
void handlerRecv(const intptr_t sParam, const uintptr_t uParam)
{
    static uint8_t ptr = 0;
    if ((sParam != uart_channel) || !uParam) return;

    rcv_data[ptr] = bufRecv[0];
    if (0 == ptr)
    {
        ptr += 1;
    }
    else
    {
        if (rcv_data[ptr] == UE878RCV_BIN_TERMINATOR)
        {
            ready_Cmd = true;
            handlerUE878CommandRecv();
            ptr = 0;
        }
        else
        {
            rcv_data[0] = rcv_data[ptr];
        }
    }
}

/**
 * @brief lixil受信モジュール（bluetooth/zigbee）の初期化。受信も起動する。
 * @param[in] channel UARTチャネル
 * @param[out] -
 * @retval bool 成功／失敗
 */
bool ue878_init(uint8_t channel)
{
    int32_t ret = 0;
    uart_channel = channel;
    dev_uart_ClearLibrary();
    ret = dev_uart_Initialize(uart_channel, handlerRecv, NULL);
    if (ret)
    {
        dev_uart_Read(uart_channel, bufRecv, 1, DEVIO_ASYNC_CONT);
    }
    return ret;
}

/**
 * @brief lixil受信モジュール（bluetooth/zigbee）にデータを送信する。
 * @param[in] kind 通信種別（ACK／通知）
 * @param[in] state 建材の状態
 * @param[in] info 建材機器制御結果の出力情報
 * @param[out] -
 * @retval bool 成功／失敗
 */
bool ue878_SendData(ESEND_KIND const kind, const SKenzaiState* const state, const SKenzaiOutputInfo* const info)
{
    uartPackSendData(kind, state, info);
    dev_uart_Write(UARTIDX_COMMMODULE, &send_pkt, sizeof(send_pkt), DEVIO_SYNC);
    SendHistory_append(&send_pkt);
#ifdef DEBUG_UE878
    dbprintf(DEBUGLEVEL_DEBUG, "RSent:%02X %02X %02X %02X"STR_CRLF,
        send_pkt.info, send_pkt.times_h, send_pkt.times_l, send_pkt.term);
#endif
    return true;
}

/**
 * @brief lixil受信モジュール（bluetooth/zigbee）に送信するパケットを作成する。
 * @param[in] kind 通信種別（ACK／通知）
 * @param[in] state 建材の状態
 * @param[in] info 建材機器制御結果の出力情報
 * @param[out] -
 * @retval -
 */
static void uartPackSendData(ESEND_KIND const kind, const SKenzaiState* const state, const SKenzaiOutputInfo* const info)
{
    memset(&send_pkt, 0, sizeof(send_pkt));

    /* データ格納. */
    if (ESEND_KIND_ACK == kind)
    {
        send_pkt.info = UE878SNDINFOBIT_ACK;
    }

    send_pkt.info |= (uint8_t)
        ((KenzaiState_IsAccident(state)      << UE878SNDINFO_TROUBLE)
        | (KenzaiState_IsPairingAdmit(state) << UE878SNDINFO_PAIRING)
        | (KenzaiState_IsInitiallized(state) << UE878SNDINFO_INIT)
        | (!KenzaiState_IsOpened(state)      << UE878SNDINFO_CLOSE_OPEN)
        | (KenzaiState_IsSaifu(state)        << UE878SNDINFO_AIR)
        | (KenzaiState_IsTeiden(state)       << UE878SNDINFO_POWEROFF)
        | (KenzaiState_IsFailureState(state) << UE878SNDINFO_ABNORMAL)
        );

    /* 動作回数格納 */
    uint16_t act_count = (uint16_t)KenzaiActRegist_GetAllCloseCount();
    send_pkt.times_l = (uint8_t)(act_count & 0x00FF);
    send_pkt.times_h = (uint8_t)((act_count & 0xFF00) >> 8);

    /* 品種情報格納 */
    // 未実装。
    //info->equip;
    send_pkt.term = getKenzaiType(info->equip) | UE878SND_BIN_TERMINATOR;
}

/**
 * @brief 建材機器情報からlixil受信モジュールに送信する品種情報を生成する。
 * @param[in] equip 建材機器情報
 * @param[out] -
 * @retval uint8_t 品種情報
 */
static uint8_t getKenzaiType(EKENZAI_EQUIP const equip)
{
    switch (equip)
    {
    case EKENZAI_EQUIP_UNDEFINED:
        return 0;    // 装飾窓と一緒になる。
    case EKENZAI_EQUIP_SOUSHOKU:
        return UE878TYPE_DECOWINDOW;
    case EKENZAI_EQUIP_DEMO:
        return UE878TYPE_ITALIA; //シャッター１と同じとする。
    case EKENZAI_EQUIP_SHUTTER1:
        return UE878TYPE_ITALIA;
    case EKENZAI_EQUIP_SHUTTER2:
        return UE878TYPE_AIRIS;
    case EKENZAI_EQUIP_LOOF_WINDOW:
        return UE878TYPE_LOOFWINDOW;
    case EKENZAI_EQUIP_INTERIOR_SUNSHAED:
        return UE878TYPE_SUNSHADE;
    case EKENZAI_EQUIP_LOOVER: //! not implemented yet
    case EKENZAI_EQUIP_FUHRDOOR: //! not implemented yet
    case EKENZAI_EQUIP_NEXTDOOR: //! not implemented yet
    case EKENZAI_EQUIP_NUM: // これはエラー（default）
    default:
        return 0;
    }
}

/* [] END OF FILE */
