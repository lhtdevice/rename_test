/**
 * @file  KenzaiState.c
 * @brief 建材機器の状態実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#include <stddef.h>
#include "KenzaiState.h"

/**
 * @brief 異常の有無を問い合わせる
 * @param[in]  state 建材機器の状態へのポインタ
 * @param[out] -
 * @retval uint8_t （bool値） true：異常あり、false：異常なし
 */
uint8_t KenzaiState_IsAccident(const SKenzaiState* state)
{
    if (NULL == state) return 0;
    return (state->accident != EKENZAI_ACCIDENT_NONE);
}

/**
 * @brief ペアリングの状態を問い合わせる
 * @param[in]  state 建材機器の状態へのポインタ
 * @param[out] -
 * @retval uint8_t （bool値） true：ペアリング済、false：未ペアリング
 */
uint8_t KenzaiState_IsPairingAdmit(const SKenzaiState* state)
{
    if (NULL == state) return 0;
    return (state->Pairing == 1);
}

/**
 * @brief 初期化の状態を問い合わせる
 * @param[in]  state 建材機器の状態へのポインタ
 * @param[out] -
 * @retval uint8_t （bool値） true：初期化済、false：未初期化
 */
uint8_t KenzaiState_IsInitiallized(const SKenzaiState* state)
{
    if (NULL == state) return 0;
    return (state->Initialized == EKENZAI_INIT_STATE_ALREADY);
}

/**
 * @brief 建材機器が開いているかを問い合わせる
 * @param[in]  state 建材機器の状態へのポインタ
 * @param[out] -
 * @retval uint8_t （bool値） true：開いている、false：閉じている
 */
uint8_t KenzaiState_IsOpened(const SKenzaiState* state)
{
    if (NULL == state) return 0;
    switch (state->equipState)
    {
    case EEQUIP_STATE_STOPPED:  //! 1：停止中（上限下限でない位置での停止）
        // fall through
    case EEQUIP_STATE_OPENING:  //! 2：開動作中
        // fall through
    case EEQUIP_STATE_OPENED:   //! 3：全開停止中
        // fall through
    case EEQUIP_STATE_CLOSING:  //! 4：閉動作中
        // fall through
    case EEQUIP_STATE_DOOR_OPENED:  //! 25：ドアオープン
        // fall through
    case EEQUIP_STATE_DOOR_FULLOPENED:  //! 26：ドアフルオープン
        // fall through
    case EEQUIP_STATE_WINDOW_OPENED:    //! 28：窓オープン
        // fall through
    case EEQUIP_STATE_WINDOW_FULLOPENED:    //! 29：窓フルオープン
        return 1;
    // 以下else
    case EEQUIP_STATE_CLOSED:
    case EEQUIP_STATE_VENTILATION_OPENING:
    case EEQUIP_STATE_VENTILATION_CLOSING:
    case EEQUIP_STATE_VENTILATION_STOPPED:
    case EEQUIP_STATE_VENTILATION_OPENED:
    case EEQUIP_STATE_BLIND_STOPPED:
    case EEQUIP_STATE_BLIND_OPENING:
    case EEQUIP_STATE_BLIND_OPENED:
    case EEQUIP_STATE_BLIND_CLOSING:
    case EEQUIP_STATE_BLIND_CLOSED:
    case EEQUIP_STATE_ALLLATCH_LOCKED:
    case EEQUIP_STATE_ALLLATCH_UNLOCKED:
    case EEQUIP_STATE_LATCH1_LOCKED:
    case EEQUIP_STATE_LATCH1_UNLOCKED:
    case EEQUIP_STATE_LATCH2_LOCKED:
    case EEQUIP_STATE_LATCH2_UNLOCKED:
    case EEQUIP_STATE_LATCH3_LOCKED:
    case EEQUIP_STATE_LATCH3_UNLOCKED:
    case EEQUIP_STATE_LATCH4_LOCKED:
    case EEQUIP_STATE_LATCH4_UNLOCKED:
    case EEQUIP_STATE_DOOR_CLOSED:
    case EEQUIP_STATE_WINDOW_CLOSED:
    case EEQUIP_STATE_UNKNOWN:
    case EEQUIP_STATE_NUM: // この状態はない（default）
    default:
        ; // nothing to do (return 0)
    }
    return 0;
}

/**
 * @brief 建材機器（シャッター）が採風中かどうかを問い合わせる
 * @param[in]  state 建材機器の状態へのポインタ
 * @param[out] -
 * @retval uint8_t （bool値） true：採風中、false：採風していない
 */
uint8_t KenzaiState_IsSaifu(const SKenzaiState* state)
{
    if (NULL == state) return 0;
    switch (state->equipState)
    {
    case EEQUIP_STATE_VENTILATION_OPENING:  //! 6：採風開動作中
        // fall through
    case EEQUIP_STATE_VENTILATION_CLOSING:  //! 7：採風閉動作中
        // fall through
    case EEQUIP_STATE_VENTILATION_STOPPED:  //! 8：採風停止中（下限～採風全開間で停止中）
        // fall through
    case EEQUIP_STATE_VENTILATION_OPENED:   //! 9：採風全開停止中
        return 1;
    // 以下 else
    case EEQUIP_STATE_OPENING:
    case EEQUIP_STATE_CLOSING:
    case EEQUIP_STATE_OPENED:
    case EEQUIP_STATE_CLOSED:
    case EEQUIP_STATE_STOPPED:
    case EEQUIP_STATE_BLIND_STOPPED:
    case EEQUIP_STATE_BLIND_OPENING:
    case EEQUIP_STATE_BLIND_OPENED:
    case EEQUIP_STATE_BLIND_CLOSING:
    case EEQUIP_STATE_BLIND_CLOSED:
    case EEQUIP_STATE_ALLLATCH_LOCKED:
    case EEQUIP_STATE_ALLLATCH_UNLOCKED:
    case EEQUIP_STATE_LATCH1_LOCKED:
    case EEQUIP_STATE_LATCH1_UNLOCKED:
    case EEQUIP_STATE_LATCH2_LOCKED:
    case EEQUIP_STATE_LATCH2_UNLOCKED:
    case EEQUIP_STATE_LATCH3_LOCKED:
    case EEQUIP_STATE_LATCH3_UNLOCKED:
    case EEQUIP_STATE_LATCH4_LOCKED:
    case EEQUIP_STATE_LATCH4_UNLOCKED:
    case EEQUIP_STATE_DOOR_OPENED:
    case EEQUIP_STATE_DOOR_FULLOPENED:
    case EEQUIP_STATE_DOOR_CLOSED:
    case EEQUIP_STATE_WINDOW_OPENED:
    case EEQUIP_STATE_WINDOW_FULLOPENED:
    case EEQUIP_STATE_WINDOW_CLOSED:
    case EEQUIP_STATE_UNKNOWN:
    case EEQUIP_STATE_NUM: // この状態はない（default）
    default:
        ; // nothing to do (return 0)
    }
    return 0;
}

/**
 * @brief 停電復帰であるかを問い合わせる
 * @param[in]  state 建材機器の状態へのポインタ
 * @param[out] -
 * @retval uint8_t （bool値） true：停電復帰、false：停電ではない
 */
uint8_t KenzaiState_IsTeiden(const SKenzaiState* state)
{
    if (NULL == state) return 0;
    return (state->posConfirm == EKENZAI_POSCONFIRM_DISABLE);
}

/**
 * @brief 建材機器でエラーが発生しているかを問い合わせる
 * @param[in]  state 建材機器の状態へのポインタ
 * @param[out] -
 * @retval uint8_t （bool値） true：エラー発生、false：エラーなし
 */
uint8_t KenzaiState_IsFailureState(const SKenzaiState* state)
{
    if (NULL == state) return 0;
    return (state->failureInfo != EKENZAI_FAILURE_NOTHING);
}

/**
 * @brief 建材機器のロック状態を問い合わせる
 * @param[in]  state 建材機器の状態へのポインタ
 * @param[out] -
 * @retval uint8_t （bool値） true：施錠、false：未施錠
 */
uint8_t KenzaiState_IsLocked(const SKenzaiState* state)
{
    if (NULL == state) return 0;
    return (state->equipState == EEQUIP_STATE_ALLLATCH_LOCKED);
}

/**
 * @brief 建材機器（モーター）が動いているかを問い合わせる
 * @param[in]  state 建材機器の状態へのポインタ
 * @param[out] -
 * @retval uint8_t （bool値） true：動作中、false：停止中
 */
uint8_t KenzaiState_IsMoving(const SKenzaiState *state)
{
    if (NULL == state) return 0;
    switch (state->equipState)
    {
    case EEQUIP_STATE_OPENING:  //! 2：開動作中
        // fall through
    case EEQUIP_STATE_CLOSING:  //! 4：閉動作中
        return 1;
    case EEQUIP_STATE_VENTILATION_OPENING:  //! 6：採風開動作中
        // fall through
    case EEQUIP_STATE_VENTILATION_CLOSING:  //! 7：採風閉動作中
        // fall through
    case EEQUIP_STATE_BLIND_OPENING:    //! 11：ブラインド開動作中
        // fall through
    case EEQUIP_STATE_BLIND_CLOSING:    //! 13：ブラインド閉動作中
        return 0;
    // 以下 else
    case EEQUIP_STATE_OPENED:
    case EEQUIP_STATE_CLOSED:
    case EEQUIP_STATE_STOPPED:
    case EEQUIP_STATE_VENTILATION_STOPPED:
    case EEQUIP_STATE_VENTILATION_OPENED:
    case EEQUIP_STATE_BLIND_STOPPED:
    case EEQUIP_STATE_BLIND_OPENED:
    case EEQUIP_STATE_BLIND_CLOSED:
    case EEQUIP_STATE_ALLLATCH_LOCKED:
    case EEQUIP_STATE_ALLLATCH_UNLOCKED:
    case EEQUIP_STATE_LATCH1_LOCKED:
    case EEQUIP_STATE_LATCH1_UNLOCKED:
    case EEQUIP_STATE_LATCH2_LOCKED:
    case EEQUIP_STATE_LATCH2_UNLOCKED:
    case EEQUIP_STATE_LATCH3_LOCKED:
    case EEQUIP_STATE_LATCH3_UNLOCKED:
    case EEQUIP_STATE_LATCH4_LOCKED:
    case EEQUIP_STATE_LATCH4_UNLOCKED:
    case EEQUIP_STATE_DOOR_OPENED:
    case EEQUIP_STATE_DOOR_FULLOPENED:
    case EEQUIP_STATE_DOOR_CLOSED:
    case EEQUIP_STATE_WINDOW_OPENED:
    case EEQUIP_STATE_WINDOW_FULLOPENED:
    case EEQUIP_STATE_WINDOW_CLOSED:
    case EEQUIP_STATE_UNKNOWN:
    case EEQUIP_STATE_NUM: // この状態はない（default）
    default:
        ; // nothing to do (return 0)
    }
    return 0;
}

/* [] END OF FILE */
