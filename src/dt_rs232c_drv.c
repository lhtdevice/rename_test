// =============================================================================
//  Copyright (C) 2009-2016, Heartland Data inc.  All Rights Reserved.
//
//  Title  :   UART Driver
//  EventID:   Insert EventID Test Point
//  AppVer :   DT10 Ver10.XX�`
//  FileID :   15a
//  Version:   1.1
//  Author :   HLDC
//
// =============================================================================

// =============================================================================
// 	Please customize the code for your environment.
// =============================================================================
#include "r_cg_macrodriver.h"
#include "r_cg_serial.h"

//------------------------------------------------------------------------------
//	Desc:	Header for Ethernet Controll
//------------------------------------------------------------------------------
//#include "Common.h"

//------------------------------------------------------------------------------
//  Macro:	DT_UINT
//	Desc:	Please chage Test Point argument type for DT10 Project setting.
//------------------------------------------------------------------------------
#define	DT_UINT unsigned int

//------------------------------------------------------------------------------
//	Macro:	DT_INLINE
//	Desc: 	Please use "static" instead of "inline" if "inline" cannot be used.
//------------------------------------------------------------------------------
#define	DT_INLINE	static
//#define	DT_INLINE	_inline			// for Windows
//#define	DT_INLINE	static		// when "inline" cannot be used

//------------------------------------------------------------------------------
//	Func:	_TP_BusUartInit
//	Desc: 	Please describe the code to initialize Uart Port.
//------------------------------------------------------------------------------
static void _TP_BusUartInit(void)
{
    R_UART1_Start();
}

//------------------------------------------------------------------------------
//	Func: _TP_BusOutDrv
//	Desc: Test Point Output Function
//------------------------------------------------------------------------------
static int init = 0;
DT_INLINE void _TP_BusOutDrv( unsigned char *buff, DT_UINT count )
{
	int	n;
	if( init == 0 ){
		_TP_BusUartInit();
		init = 1;
	}
	// Uart_Send( hCom, buff, count, &n, NULL );
	R_UART1_Send(buff, count);
}

//------------------------------------------------------------------------------
//	Macro:	DT_MAX_VAL_SIZE
//	Desc:	Please set the valule of Variables lenght.
//			But, never set the value over 256.
//------------------------------------------------------------------------------
#define DT_MAX_VAL_SIZE		256

// =============================================================================
// 	Don't change the code from here as possible.
// =============================================================================

//------------------------------------------------------------------------------
//	Macro:	Command Macros
//	Desc:	Please do not change the value.
//------------------------------------------------------------------------------
#define STX				0x02
#define ETX				0x03
#define CMD_TESTPOINT	0x07
#define CMD_TESTVALUE	0x08
#define	DT_VARIABLE_BIT	0x02
#define	DT_EVTTRG_BIT		0x08

//------------------------------------------------------------------------------
//	Func: _TP_BusOut
//	Desc: Called by Test Point
//------------------------------------------------------------------------------
void _TP_BusOut( DT_UINT addr, DT_UINT dat )
{
	int i;
	unsigned char buff[9];

	buff[0] = STX;					// STX
	buff[1] = CMD_TESTPOINT;		// RS-232C Test Point COMMAND
	buff[2] = (dat >> 8)  & 0x00FF;	// D1
	buff[3] = (dat)       & 0x00FF;	// D0
	buff[4] = (addr >>16) & 0x00FF;	// A2
	buff[5] = (addr >> 8) & 0x00FF;	// A1
	buff[6] = (addr     ) & 0x00FF;	// A0
	buff[7] = CMD_TESTPOINT;			// Check-SUM
	for(i=2;i<7;i++) {
		buff[7] ^= buff[i];
	}
	buff[8] = ETX;					// ETX
	_TP_BusOutDrv( buff, 9 );
}

//------------------------------------------------------------------------------
//	Func: _TP_MemoryOutput
//	Desc: Called by Variable Test Point
//------------------------------------------------------------------------------
void _TP_MemoryOutput( DT_UINT addr, DT_UINT dat, void *value, DT_UINT size )
{
	unsigned char buff[DT_MAX_VAL_SIZE+10], csum;
	DT_UINT i;
	unsigned char *p = value;

	if( size >= DT_MAX_VAL_SIZE ) size = DT_MAX_VAL_SIZE;
	buff[0] = STX;					// STX
	buff[1] = CMD_TESTVALUE;			// RS-232C Test Point COMMAND
	buff[2] = size;					// Number of Bytes
	buff[3] = (dat >> 8) & 0x00FF;	// D1
	buff[4] = (dat)      & 0x00FF;	// D0
	buff[5] = (addr >>16) & 0x00FF;	// A2
	buff[6] = (addr >> 8) & 0x00FF;	// A1
	buff[7] = (addr     ) & 0x00FF;	// A0
	for( i = 0; i < size; i++, p++) {				// Memory Value
		buff[8+i] = *p;
	}
	csum = buff[1];
	p = &buff[2];
	for( i = 2; i < (8+size); i++, p++ ){
		csum ^= *p;
	}
	buff[i] = csum;
	buff[i+1] = ETX;
	_TP_BusOutDrv( buff, size+10 );
}

//------------------------------------------------------------------------------
//	Func: _TP_EventTrigger
//	Desc: Called by Event Trigger Test Point
//------------------------------------------------------------------------------
void _TP_EventTrigger( DT_UINT addr, DT_UINT dat )
{
	_TP_BusOut( addr | DT_EVTTRG_BIT  , ((unsigned short)(dat&0x0FFF))<<4 );
}

// =============================================================================

