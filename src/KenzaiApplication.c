#include	"DT_KenzaiApplication.h"	/* For DTPlus-TestPoint */
/**
 * @file KenzaiApplication.c
 * @brief 機器を表す。機器の状態を管理する。機器への操作は、このファイルで定義する関数を介して行う。
 */
#include "KenzaiApplication.h"
#include "KenzaiActDefine.h"
#include "ActLogManager.h"
#include "dev_timer.h"
#include "DebugMonitor.h"
//#include "MotorControl.h"
#include "r_cg_serial.h"

#define TIMEOUT_OPENCLOSE   (60 * (1000 / 2)) // 開閉動作タイムアウト
#define TIME_COOLDOWN1      (1 * (1000 / 2))
#define TIME_COOLDOWN3      (3 * (1000 / 2))

static intptr_t          tid_move = TIMER_ID_ERROR, tid_cool1 = TIMER_ID_ERROR, tid_cool3 = TIMER_ID_ERROR;
static EEQUIP_STATE      last_state = EEQUIP_STATE_NUM;
static SKenzaiState      kenzai_state;
static SKenzaiOutputInfo out_info = {EKENZAI_EQUIP_UNDEFINED};
static ERESPONSE_MODE    responce_mode;
//static EKENZAI_EQUIP     type_equip = EKENZAI_EQUIP_UNDEFINED;

static void sendAck(EKENZAI_ACT_OPERATION act_op);
static void setResponseMode(EKENZAI_EQUIP type);

// アプリケーションの操作。必要な操作のみ実装する。
static void actStop(SKenzaiState * const pSt);          // 停止
static void actOpen(SKenzaiState * const pSt);          // 開
static void actClose(SKenzaiState * const pSt);         // 閉
#if 0
static void actFavorite(SKenzaiState * const pSt);      // ユーザ定義
static void actVentilation(SKenzaiState * const pSt);   // 採風（シャッター）
static void actLock(SKenzaiState * const pSt);          // 施錠
static void actUnlock(SKenzaiState * const pSt);        // 解錠
static void actLightNormal(SKenzaiState * const pSt);   // 採光正転（ブラインド）
static void actLightReverse(SKenzaiState * const pSt);  // 採光逆転（ブラインド）
#endif

static void callbackTimeoutMove(const intptr_t id, const uintptr_t count);
#if 0
static void callbackTimeoutCool1(const intptr_t id, const uintptr_t count);
static void callbackTimeoutCool3(const intptr_t id, const uintptr_t count);
static void setCooldown(const uint32_t cnt);
#endif

#define CMDQUEUE_DEPTH 1
EKENZAI_ACT_OPERATION cmdq[CMDQUEUE_DEPTH];
static uint8_t cmdq_last = 0;
static uint8_t cmdq_next = 0;
static uint8_t cmdq_num  = 0;
static EKENZAI_ACT_OPERATION overheat_recover = EKENZAI_ACT_OPERATION_NONE;

static void clearQueue(void)
{
    __DtTestPointFast( 0, 1 );
    cmdq_last = cmdq_next = cmdq_num = 0;
	__DtTestPointFast( 1, 1 );
}

static EKENZAI_ACT_OPERATION readQueue(void)
{
    EKENZAI_ACT_OPERATION op = EKENZAI_ACT_OPERATION_NONE;
    __DtTestPointFast( 2, 2 );
    if (0 < cmdq_num)
    {
        __DtTestPointFast( 3, 2 );
        __DtTestPointValueFast( 4, 3, ( void * )&op, sizeof( op ) );
        op = cmdq[cmdq_last];
        cmdq_last = (cmdq_last + 1) % CMDQUEUE_DEPTH;
        cmdq_num -= 1;
    }
    __DtTestPointFast(5, 3 );
    return op;
}

static uint8_t writeQueue(EKENZAI_ACT_OPERATION op)
{
    __DtTestPointFast(6, 3 );
    if (CMDQUEUE_DEPTH <= cmdq_num) // キューがフル
    {
        EKENZAI_ACT_OPERATION op = readQueue(); // 古いものから捨てる
        __DtTestPointFast(7, 3 );
        ActLog_append(op | KENZAI_ACT_DISCARDED);
    }
    // 以下else
    cmdq[cmdq_next] = op;
    cmdq_num += 1;
    cmdq_next = (cmdq_next + 1) % CMDQUEUE_DEPTH;
    __DtTestPointFast(8, 4);
    return cmdq_num;
}

/**
 * @brief アプリケーションタイプ（操作対象建材）を返す。
 * @param[in] -
 * @param[out] -
 * @pre -
 * @post -
 * @retval 操作対象建材
*/
EKENZAI_EQUIP KenzaiApplication_getEquipType(void)
{
    //return type_equip;
	__DtTestPointFast(9, 4 );
	return out_info.equip;
}

/**
 * @brief 初期化する。
 * @param[in] type 制御する建材
 * @param[out] -
 * @pre -
 * @post -
 * @retval bool 成功/失敗
 * @remarks 建材制御に対する制御の前に、呼び出す必要がある。
*/
bool KenzaiApplication_init(EKENZAI_EQUIP type)
{
    __DtTestPointFast(10, 4 );
    if ((EKENZAI_EQUIP_UNDEFINED > type) || (EKENZAI_EQUIP_NUM <= type)) return false;
    //type_equip = type;
	out_info.equip = type;
    setResponseMode(type);

    MotorControl_init();
    //R_UART1_Start();

    kenzai_state.accident    = EKENZAI_ACCIDENT_NONE;
    kenzai_state.equipState  = EEQUIP_STATE_UNKNOWN;
    kenzai_state.failureInfo = EKENZAI_FAILURE_NOTHING;
    kenzai_state.Initialized = EKENZAI_INIT_STATE_NOT;
    kenzai_state.Initialized = EKENZAI_INIT_STATE_ALREADY; //! 実機では初期化確認要
    kenzai_state.Pairing     = 0;   //未使用.
    kenzai_state.posConfirm  = EKENZAI_POSCONFIRM_DISABLE;

    KenzaiApplication_updateState(EEQUIP_STATE_UNKNOWN);
    if (ERESPONSE_MODE_UEI == responce_mode)
        return ue878_SendData(ESEND_KIND_NOTIFY, &kenzai_state, &out_info);
    else
        return true;
}


/**
 * @brief 建材に対して指定されたコマンド操作をする。
 * @param[in] act_op 操作コマンド
 * @param[out] -
 * @pre 初期化済みであること
 * @post -
 * @retval -
*/
void KenzaiApplication_actOperation(EKENZAI_ACT_OPERATION act_op)
{
    EEQUIP_STATE old_state = kenzai_state.equipState;
//    dbprintf(DEBUGLEVEL_DEBUG, "Operation:%02X"STR_CRLF, act_op);
    __DtTestPointFast(11, 4 );
    switch (act_op & ~KENZAI_NOOP_FLAG_MASK)
    {
    case EKENZAI_ACT_OPERATION_NOACT:
        // 動作はなし。ACKは返す。
        __DtTestPointFast(12, 4 );
        break;
    case EKENZAI_ACT_OPERATION_STOP:
        __DtTestPointFast(13, 4 );
        actStop(&kenzai_state);
        break;
    case EKENZAI_ACT_OPERATION_OPEN:
        __DtTestPointFast(14, 4 );
        actOpen(&kenzai_state);
        break;
    case EKENZAI_ACT_OPERATION_CLOSE:
        __DtTestPointFast(15, 4 );
        actClose(&kenzai_state);
        break;
    case EKENZAI_ACT_OVERCURRENT:
        __DtTestPointFast(16, 5);
        actStop(&kenzai_state);
        if (old_state == EEQUIP_STATE_OPENING) kenzai_state.equipState = EEQUIP_STATE_OPENED;
        if (old_state == EEQUIP_STATE_CLOSING) kenzai_state.equipState = EEQUIP_STATE_CLOSED;
        //setCooldown(TIME_COOLDOWN3);
        break;
    //case EKENZAI_ACT_OVERCURRENT_LITTLE:
        //actStop(&kenzai_state);
        //setCooldown(TIME_COOLDOWN1);
        //break;
    case EKENZAI_ACT_REACH_END:
        __DtTestPointFast(17, 5 );
        actStop(&kenzai_state);
        if (old_state == EEQUIP_STATE_OPENING) kenzai_state.equipState = EEQUIP_STATE_OPENED;
        if (old_state == EEQUIP_STATE_CLOSING) kenzai_state.equipState = EEQUIP_STATE_CLOSED;
        break;
    default:
        // nothing to do.
        __DtTestPointFast(18, 5 );
        return;
    }

    // Stateに変化があったら通知を行う
    KenzaiApplication_checkState(act_op);
	__DtTestPointFast(19, 5 );
}

/**
 * @brief コマンドのACK応答、キュー処理、実行を行う。
 * @param[in] act_op 操作コマンド
 * @param[out] -
 * @pre 初期化済みであること
 * @post -
 * @retval -
*/
void KenzaiApplication_actQueue(EKENZAI_ACT_OPERATION const act_op)
{
    __DtTestPointFast(20, 5 );
    sendAck(act_op);
    ActLog_append(act_op);  // ログ記録.

    if ((TIMER_ID_ERROR != tid_cool1) || (TIMER_ID_ERROR != tid_cool3))
    {
        __DtTestPointFast(21, 5 );
        writeQueue(act_op);
        __DtTestPointFast(22, 5 );
        return;
    }
    EKENZAI_ACT_OPERATION inq = readQueue();
    if (EKENZAI_ACT_OPERATION_NONE == inq)
        KenzaiApplication_actOperation(act_op);
    else
        KenzaiApplication_actOperation(inq);
	__DtTestPointFast(23, 5 );
}

/**
 * @brief 建材の状態をデバッグ出力する。
 * @param[in] -
 * @param[out] -
 * @pre -
 * @post -
 * @retval -
*/
void KenzaiApplication_OutputState(void)
{
    __DtTestPointFast(24, 5 );
    DEBUG_OUTPUT_WITHUINT("State:", (uint32_t)kenzai_state.equipState);
	__DtTestPointFast(25, 5 );
}

/**
 * @brief 建材の状態を取得する。
 * @param[in] -
 * @param[out] -
 * @pre -
 * @post -
 * @retval EEQUIP_STATE 建材の状態
*/
EEQUIP_STATE KenzaiApplication_getState(void)
{
    __DtTestPointFast(26, 5 );
    return kenzai_state.equipState;
}

/**
 * @brief 建材が動作中であるかどうか確認する
 * @param[in] -
 * @param[out] -
 * @pre 初期化済みであること
 * @post -
 * @retval 1 動作中.
 * @retval 1以外 非動作中.
*/
uint8_t KenzaiApplication_isMoving(void)
{
    __DtTestPointFast(27, 5 );
    return KenzaiState_IsMoving(&kenzai_state);
}

/**
 * @brief 初期設定が必要かどうか確認する
 * @param[in] -
 * @param[out] -
 * @post -
 * @retval true 未初期設定
 * @retval false 初期設定済
*/
bool KenzaiApplication_isInitalSet(void)
{
    __DtTestPointFast(28, 5 );
    return KenzaiState_IsMoving(&kenzai_state);
}

void KenzaiApplication_restartFromOverheat(void)
{
    __DtTestPointFast(29, 5 );
    if (EKENZAI_ACT_OPERATION_NONE != overheat_recover)
    {
        __DtTestPointFast(30, 5 );
        KenzaiApplication_actOperation(overheat_recover);
    }
	__DtTestPointFast(31, 5 );
}

/**
 * @brief 過電流時の処理をする。
 * @param[in] -
 * @param[out] -
 * @pre 初期化済みであること
 * @post -
 * @retval -
 * @remarks 動作中か否かで内部で処理を変える。
*/
void KenzaiApplication_handlerDetectOverCurrent(void)
{
    __DtTestPointFast(32, 6);
    overheat_recover = EKENZAI_ACT_OPERATION_NONE;
    if (KenzaiApplication_isMoving())
    {
        // 動作中の過電流検知は上下限到達
        __DtTestPointFast(33, 6 );
        kenzai_state.accident = EKENZAI_ACCIDENT_OVERCURRENT;
        KenzaiApplication_actOperation(EKENZAI_ACT_OVERCURRENT | KENZAI_ACT_EVENT_FLAG);
    }
	// 動作中でない過電流は考慮しない
#if 0 // 過電流未満の電流大→一時停止
	if (KenzaiApplication_isMoving())
    {
        // 動作中の過電流警告はモーターを一時停止する
        __DtTestPointFast(34, 6 );
        kenzai_state.accident = EKENZAI_WARNING_OVERCURRENT;
        KenzaiApplication_actOperation(EKENZAI_ACT_OVERCURRENT_LITTLE | KENZAI_ACT_EVENT_FLAG);
        if (EEQUIP_STATE_OPENING == kenzai_state.equipState)
        {
            __DtTestPointFast(35, 6 );
            overheat_recover = EKENZAI_ACT_OPERATION_OPEN | KENZAI_ACT_EVENT_FLAG;
        }
        else
        {
            __DtTestPointFast(36, 6 );
            overheat_recover = EKENZAI_ACT_OPERATION_CLOSE | KENZAI_ACT_EVENT_FLAG;
        }
    }
#endif
	__DtTestPointFast(37, 6 );
}


/**
 * @brief 現在（最新）の状態を確認する。
 * @param[in] act_op 操作コマンド
 * @param[out] -
 * @pre 初期化済みであること
 * @post 状態確認の結果、状態が変わっていた場合には、状態変化を通知していること
 * @retval -
*/
void KenzaiApplication_checkState(EKENZAI_ACT_OPERATION act_op)
{
    __DtTestPointFast(38, 6 );
    if (last_state != kenzai_state.equipState)
    {
        __DtTestPointFast(39, 6 );
        KenzaiActDefine_RegistChanged(&kenzai_state, act_op); // サブクラスで実装.
//        dbprintf(DEBUGLEVEL_DEBUG, "State Change(%02d->%02d)"STR_CRLF, last_state, kenzai_state.equipState);

        //状態変化を通知.
        if (responce_mode == ERESPONSE_MODE_UEI)
        {
            __DtTestPointFast(40, 6 );
            ue878_SendData(ESEND_KIND_NOTIFY, &kenzai_state, &out_info);
        }
    }
	__DtTestPointFast(41, 6 );
}


/**
 * @brief 指定された状態に変化させる。
 * @param[in] state 変化させる状態
 * @param[out] -
 * @pre 初期化済みであること
 * @post 状態確認の結果、状態が変わった場合には、状態変化を通知していること
 * @retval -
*/
void KenzaiApplication_updateState(EEQUIP_STATE state)
{
    __DtTestPointFast(42, 6 );
    kenzai_state.equipState = state;
    KenzaiApplication_checkState(EKENZAI_ACT_OPERATION_NONE);
	__DtTestPointFast(43, 6 );
}

/**
 * @brief 通信モジュールにACKを返す。
 * @param[in] act_op 要求されたオペレーション
 * @param[out] -
 * @pre 初期化済みであること
 * @retval -
*/
static void sendAck(EKENZAI_ACT_OPERATION act_op)
{
    __DtTestPointFast(44, 6 );
    if (responce_mode == ERESPONSE_MODE_UEI)
    {
        // UEI受信機では必要。
        __DtTestPointFast(45, 6 );
        if (!(act_op & KENZAI_NOOP_FLAG_MASK) && (act_op != EKENZAI_ACT_OPERATION_NONE))
        {
            __DtTestPointFast(46, 6 );
            ue878_SendData(ESEND_KIND_ACK, &kenzai_state, &out_info);
        }
    }

	__DtTestPointFast(47, 6 );
}

/**
 * @brief 建材制御の結果の通知のモードを設定する（見直し予定）
 * @param[in] type 建材
 * @param[out] -
 * @pre -
 * @post モードが設定されていること
 * @retval -
*/
static void setResponseMode(EKENZAI_EQUIP type)
{
    __DtTestPointFast(48, 6 );
    switch (type)
    {
    default:
        __DtTestPointFast(49, 6 );
        responce_mode = ERESPONSE_MODE_UEI;
    }
	__DtTestPointFast(50, 6 );
}

#if 0
static void setCooldown(const uint32_t cnt)
{
    __DtTestPointFast(51, 6 );
    if (cnt == TIME_COOLDOWN1)
    {
        __DtTestPointFast(52, 6 );
        if (TIMER_ID_ERROR == tid_cool3)
        {
            __DtTestPointFast(53, 6 );
            tid_cool3 = dev_timer_setCallback(TIMERIDX_1MS, cnt, TIMERCB_ONETIME, callbackTimeoutCool1);
        }
    }
    else if (cnt == TIME_COOLDOWN3)
    {
        __DtTestPointFast(54, 6 );
        if (TIMER_ID_ERROR != tid_cool1)
        {
            __DtTestPointFast(55, 6 );
            dev_timer_cancelCallback(TIMERIDX_1MS, tid_cool1);
            tid_cool1 = TIMER_ID_ERROR;
        }
        tid_cool3 = dev_timer_setCallback(TIMERIDX_1MS, cnt, TIMERCB_ONETIME, callbackTimeoutCool3);
    }
	__DtTestPointFast(56, 6 );
}
#endif

/**
 * @brief タイマコールバック（開閉60s）。
 * @param[in] id：タイマID
 * @param[in] count：タイマカウント値
 * @retval -
 */
static void callbackTimeoutMove(const intptr_t id, const uintptr_t count)
{
    __DtTestPointFast(57, 6 );
    (void)count;
    if (id != tid_move) return;
    tid_move = TIMER_ID_ERROR;
    if (KenzaiApplication_isMoving())
        actStop(&kenzai_state);
	__DtTestPointFast(58, 6 );
}

#if 0
/**
 * @brief タイマコールバック（クールダウン1s）。
 * @param[in] id：タイマID
 * @param[in] count：タイマカウント値
 * @retval -
 */
static void callbackTimeoutCool1(const intptr_t id, const uintptr_t count)
{
    __DtTestPointFast(59, 6 );
    (void)count;
    if (id != tid_cool1) return;
    tid_cool1 = TIMER_ID_ERROR;
    // to do :implementation
	__DtTestPointFast(60, 6 );
}

/**
 * @brief タイマコールバック（クールダウン3s）。
 * @param[in] id：タイマID
 * @param[in] count：タイマカウント値
 * @retval -
 */
static void callbackTimeoutCool3(const intptr_t id, const uintptr_t count)
{
    __DtTestPointFast(61, 6 );
    (void)count;
    if (id != tid_cool3) return;
    tid_cool3 = TIMER_ID_ERROR;
    // to do :implementation
	__DtTestPointFast(62, 6 );
}
#endif

static void callbackMotor(void)
{
     __DtTestPointFast(63, 6 );
     KenzaiApplication_actOperation(EKENZAI_ACT_REACH_END | KENZAI_ACT_EVENT_FLAG);
	__DtTestPointFast(64, 7);
}

static void actStop(SKenzaiState * const pSt)
{
    // 不要な場合も停止動作させる。動作異常発生時でもなるべく停止できるようにするため。
    // motor_stop();
    __DtTestPointFast(65, 7 );
    MotorControl_stop();
    switch (pSt->equipState)
    {
    case EEQUIP_STATE_OPENING:
        __DtTestPointFast(66, 7 );
        if (pSt->accident == EKENZAI_ACCIDENT_OVERCURRENT)
        { // 過電流=>上限検知
            __DtTestPointFast(67, 7 );
            pSt->accident = EKENZAI_ACCIDENT_NONE;
            pSt->equipState = EEQUIP_STATE_OPENED;
            //updatePosOpen(true);
        }
        else
        {
            __DtTestPointFast(68, 7 );
            pSt->equipState = EEQUIP_STATE_STOPPED;
            //updatePosOpen(false);
        }
        break;
    case EEQUIP_STATE_UNKNOWN:
        __DtTestPointFast(69, 7 );
        pSt->equipState = EEQUIP_STATE_STOPPED;
        break;
    case EEQUIP_STATE_CLOSING:
        __DtTestPointFast(70, 7 );
        if (pSt->accident == EKENZAI_ACCIDENT_OVERCURRENT)
        { // 過電流=>下限検知
            __DtTestPointFast(71, 7 );
            pSt->accident = EKENZAI_ACCIDENT_NONE;
            pSt->equipState = EEQUIP_STATE_CLOSED;
            //updatePosClose(true);
        }
        else
        {
            __DtTestPointFast(72, 7 );
            pSt->equipState = EEQUIP_STATE_STOPPED;
            //updatePosClose(false);
        }
        break;
    case EEQUIP_STATE_OPENED:
    case EEQUIP_STATE_CLOSED:
    case EEQUIP_STATE_STOPPED:
    default:
        __DtTestPointFast(73, 7 );
        ; // nothing to do
    }
    // if (TIMER_ID_ERROR != tid_move)
    // {
    //     dev_timer_cancelCallback(TIMERIDX_1MS, tid_move);
    //     tid_move = TIMER_ID_ERROR;
    // }
	__DtTestPointFast(74, 7 );
}

static void actOpen(SKenzaiState * const pSt)
{
    uint8_t act = 0;
    __DtTestPointFast(75, 7 );
    switch (pSt->equipState)
    {
    case EEQUIP_STATE_STOPPED:
    case EEQUIP_STATE_CLOSING:
    case EEQUIP_STATE_CLOSED:
    case EEQUIP_STATE_UNKNOWN:
		__DtTestPointFast(76, 7 );
		if ((MOTORST_UNKNOWN == MotorControl_getState()) || (MOTORST_READY == MotorControl_getState()))
		{
	        __DtTestPointFast(77, 7 );
	        pSt->equipState = EEQUIP_STATE_OPENING;
	        act = 1;
		}
        break;
    case EEQUIP_STATE_OPENING:
    case EEQUIP_STATE_OPENED:
    default:
        __DtTestPointFast(78, 7 );
        ; // nothing to do
    }
    if (act)
    {
        // tid_move = dev_timer_setCallback(TIMERIDX_1MS, TIMEOUT_OPENCLOSE, TIMERCB_ONETIME, callbackTimeoutMove);
        // motor_start(DIR_OPEN);
        __DtTestPointFast(79, 7 );
        MotorControl_start(MOTOR_DIR_OPEN, callbackMotor);
        //R_UART1_Send("open", 5);
    }
	__DtTestPointFast(80, 7 );
}

static void actClose(SKenzaiState * const pSt)
{
    uint8_t act = 0;
    __DtTestPointFast(81, 7 );
    switch (pSt->equipState)
    {
    case EEQUIP_STATE_STOPPED:
    case EEQUIP_STATE_OPENING:
    case EEQUIP_STATE_OPENED:
    case EEQUIP_STATE_UNKNOWN:
		__DtTestPointFast(82, 7 );
		if ((MOTORST_UNKNOWN == MotorControl_getState()) || (MOTORST_READY == MotorControl_getState()))
		{
	        __DtTestPointFast(83, 7 );
	        pSt->equipState = EEQUIP_STATE_CLOSING;
	        act = 1;
		}
        break;
    case EEQUIP_STATE_CLOSING:
    case EEQUIP_STATE_CLOSED:
    default:
        __DtTestPointFast(84, 7 );
        ; // nothing to do
    }
    if (act)
    {
        // tid_move = dev_timer_setCallback(TIMERIDX_1MS, TIMEOUT_OPENCLOSE, TIMERCB_ONETIME, callbackTimeoutMove);
        // motor_start(DIR_CLOSE);
        __DtTestPointFast(85, 7 );
        MotorControl_start(MOTOR_DIR_CLOSE, callbackMotor);
        //R_UART1_Send("close", 6);
    }
	__DtTestPointFast(86, 7 );
}
/* [] END OF FILE */
