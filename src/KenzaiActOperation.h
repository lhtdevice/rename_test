/**
 * @file KenzaiActOperation.h
 * @brief 建材機器の操作種類を定義する。
 */

#ifndef KENZAI_ACT_OPERATION
#define KENZAI_ACT_OPERATION


/**
* @enum EKENZAI_ACT_OPERATION
* @brief 建材に対する操作を列挙
* @details 0はまだログ記載されてないことを示す。
*/
typedef enum {
    EKENZAI_ACT_OPERATION_NONE = 0,
    EKENZAI_ACT_OPERATION_NOACT = 1,    ///< 操作なし.
    EKENZAI_ACT_OPERATION_STOP,         ///< 停止.
    EKENZAI_ACT_OPERATION_OPEN,         ///< 開操作.
    EKENZAI_ACT_OPERATION_CLOSE,        ///< 閉操作.
    EKENZAI_ACT_OVERCURRENT,            ///< 過電流
//    EKENZAI_ACT_OVERCURRENT_LITTLE,     ///< 過電流
    EKENZAI_ACT_REACH_END,
    EKENZAI_ACT_MAX // must be under 64
} EKENZAI_ACT_OPERATION;

#define KENZAI_ACT_EVENT_FLAG 0x40  // 通信モジュールからのコマンド処理でない場合はこのフラグを立てる
#define KENZAI_ACT_DISCARDED  0x80  // オペレーションが破棄された場合はこのフラグを立てる
#define KENZAI_NOOP_FLAG_MASK 0xC0

void KenzaiActOperation_actOperation(EKENZAI_ACT_OPERATION act_op);
// 実装ファイルで定義する。


#endif
/* [] END OF FILE */
