/**
 * @file KenzaiApplication.h
 * @brief 制御する建材機器を表す。機器への操作は、このファイルで定義する関数を介して行う。
 */

#if !defined(KENZAIAPPLICATION_H)
#define KENZAIAPPLICATION_H

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "KenzaiTypes.h"
#include "KenzaiActOperation.h"
#include "KenzaiActDefine.h"
#include "KenzaiState.h"
#include "KenzaiAppTemplate.h"
#include "KenzaiOutputInfo.h"
#include "project.h"
#include "UE878.h"
#include "Motor.h"
#include "motor_control.h"
#include "MotorControl.h"

/**
* @typedef ERESPONSE_MODE
* @brief 建材への制御要求に対する応答の返し方のバリエーション
*/
typedef enum responce_mode_tag {
    ERESPONSE_MODE_UEI, ///<  UEI受信機使用(Default)
    ERESPONSE_MODE_MAX
} ERESPONSE_MODE;

//typedef void (*KENZAI_COMMAND_RCV_FUNC)(ESEND_KIND, SKenzaiState*);    // 状態変化通知.

bool KenzaiApplication_init(EKENZAI_EQUIP type);
void KenzaiApplication_actOperation(EKENZAI_ACT_OPERATION const act_op);
void KenzaiApplication_actQueue(EKENZAI_ACT_OPERATION const act_op);
void KenzaiApplication_OutputState(void);
EEQUIP_STATE KenzaiApplication_getState(void); //!U
uint8_t KenzaiApplication_isMoving(void); //!U
void KenzaiApplication_handlerDetectOverCurrent(void); //!U
void KenzaiApplication_checkState(EKENZAI_ACT_OPERATION act_op);//zantei
void KenzaiApplication_updateState(EEQUIP_STATE state);//zantei
EKENZAI_EQUIP KenzaiApplication_getEquipType(void);
void KenzaiApplication_setManualMode(const bool bManual);
bool KenzaiApplication_isManualMode(void);
void KenzaiApplication_restartFromOverheat(void);
bool KenzaiApplication_requireInitialSetting(void);
bool KenzaiApplication_doingInitialSetting(void);

#endif

/* [] END OF FILE */
