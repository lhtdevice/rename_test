#include	"DT_MotorControl.h"	/* For DTPlus-TestPoint */
/**
 * @file  MotorControl.c
 * @brief モータ制御
 */

/*******************************************************************************
 * Include Files
 *******************************************************************************/

#include "project.h"
#include "main.h"
#include "SpeedControl.h"
#include "DebugMonitor.h"
#include "GPIO.h"
#include "dev_adc.h"
#include "Motor.h"
#include "MotorControl.h"

/*******************************************************************************
 * Private Macros
 *******************************************************************************/
#define REDUCTION_RATIO             8.2f
#define PULLEY_PERIMETER            125.0f  // [mm]

#define NUM_OF_MAGNETIC_POLE        10
#define PULSES_PER_ELECTRICAL_ANGLE 3
#define PULSES_PER_ROTATION         (PULSES_PER_ELECTRICAL_ANGLE * NUM_OF_MAGNETIC_POLE / 2)
#define MOVING_DISTANCE_PER_PULSE   (PULLEY_PERIMETER / PULSES_PER_ROTATION / REDUCTION_RATIO)

#define FG_OUTPUT_1PPR              GPIO_DATA_HIGH
#define FG_OUTPUT_3PPR              GPIO_DATA_LOW

#define CW_ROTATION                 GPIO_DATA_HIGH
#define CCW_ROTATION                GPIO_DATA_LOW

#define SMOOTHING_FACTOR            0.1f

#define MEASURING_TIMER_FREQUENCY   62500   // [Hz]

/*******************************************************************************
 * Private Type Definitions
 *******************************************************************************/

/*******************************************************************************
 * Private Function Prototypes
 *******************************************************************************/
static void setSpeed(float speed);

/*******************************************************************************
 * Private Variables
 *******************************************************************************/
static MOTOR_DIR _direction;
//static float _position;
//static float _speed;
//static MOTOR_CALLBACK _callback;
static bool _is_working;

//static bool _measuring;

/*******************************************************************************
 * Public Functions
 *******************************************************************************/
MOTOR_STATE MotorControl_getState(void)
{
	__DtTestPointFast( 87, 7 );
	if (g_ac_or_dc)
	{
		__DtTestPointFast( 88, 7 );
		if (ACMOTOR_SENSE_MIN > im_value)
		{
			__DtTestPointFast( 89, 7 );
			return MOTORST_OPENING;
		}
		else if (ACMOTOR_SENSE_MAX < im_value)
		{
			__DtTestPointFast( 90, 7 );
			return MOTORST_CLOSING;
		}
		__DtTestPointFast( 91, 7 );
		return MOTORST_STOP;
	}
	__DtTestPointFast( 92, 7 );
	return MOTORST_UNKNOWN;
}

void MotorControl_init(void)
{
	__DtTestPointFast( 93, 7 );
	motor_Init();
	__DtTestPointFast( 94, 7 );
}

void MotorControl_start(MOTOR_DIR dir, MOTOR_CALLBACK cb)
{
    // 止めずに反転動作して大丈夫か
    // → 確実に止めてから反転しないとパルスではどちらに回転しているかわからないので位置がおかしくなる
    __DtTestPointFast( 95, 7 );
    if (_is_working) {
        __DtTestPointFast( 96, 7 );
        if (_direction != dir) {
            __DtTestPointFast( 97, 7 );
            motor_Off();
			pauseByNop(100);
        }
        else {
            __DtTestPointFast( 98, 7 );
            return;
        }
    }

    //_speed = 0;
    _direction = dir;
    //_callback = cb;
    _is_working = true;

    //float speed = SpeedControl_init(dir == MOTOR_DIR_CLOSE);

    //GPIO_CW_CCW = (dir == MOTOR_DIR_OPEN) ? CW_ROTATION : CCW_ROTATION;
    //setSpeed(speed);

    //R_TAU0_Channel0_Start();    // PWM
    //R_TAU0_Channel3_Start();    // Cyclic Timer
	ignore_im_data = 0;
	motor_On(dir == MOTOR_DIR_CLOSE);
	g_motor_start = getTime();
	__DtTestPointFast( 99, 7 );
}

void MotorControl_stop(void)
{
	__DtTestPointFast( 100, 7 );
	motor_Off();
    _is_working = false;
	if (!g_ac_or_dc && g_dc_short_brake) g_wait_brake = MOTOR_WAIT_BRAKE;
	__DtTestPointFast( 101, 7 );
}

/*******************************************************************************
 * Private Functions
 *******************************************************************************/
static void setSpeed(float speed)
{
    float ratio = (speed - 30.0f) / (300 - 30);
    float duty = (65 + 30 * ratio) / 100;
    __DtTestPointValueFast( 102, 7, ( void * )&duty, sizeof( duty ) );

    __DtTestPointFast( 103, 7 );
    TDR01 = (uint16_t)((TDR00 + 1) * duty);

    dbprintf(DEBUGLEVEL_DEBUG, " %.1f\n", duty * 100);
	__DtTestPointFast( 104, 7 );
}
