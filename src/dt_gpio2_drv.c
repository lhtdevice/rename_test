/*==============================================================================*/
/*  Copyright (C) 2009-2016, Heartland Data inc. All Rights Reserved.           */
/*                                                                              */
/*  Title  :   GPIO 2bit Driver                                                 */
/*  EventID:   Insert EventID Test Point/Insert Extended EventID Test Point     */
/*  AppVer :   DT10 Ver10.XX�`�@                                                */
/*  FileID :   13a                                                              */
/*  Version:   1.1                                                              */
/*  Author :   HLDC                                                             */
/*==============================================================================*/

/*==============================================================================*/
/*  Please customize the code for your environment.                             */
/*==============================================================================*/

/*==============================================================================*/
/*  Desc:   Header for Port Control                                             */
/*==============================================================================*/
//#include "Common.h"
#include "iodefine.h"

/*==============================================================================*/
/*  Macro:  DT_UINT                                                             */
/*  Desc:   Please change Test Point argument type for DT10 Project setting.    */
/*==============================================================================*/
#define	DT_UINT unsigned int

/*==============================================================================*/
/*  Macro:  DT_INLINE                                                           */
/*  Desc:   Please use "static" instead of "inline" if "inline" cannot be used. */
/*==============================================================================*/
#define	DT_INLINE	static
/* #define	DT_INLINE	static */

/*==============================================================================*/
/*  Macro:  DT_WRITE_VARIABLE                                                   */
/*  Desc:   Please set 1 if use Write Test Point                                */
/*==============================================================================*/
#define DT_WRITE_VARIABLE 0

/*==============================================================================*/
/*  Func:   _TP_BusPortInit                                                     */
/*  Desc:   Please describe the code to initializes ports.                      */
/*==============================================================================*/
static void _TP_BusPortInit(void)
{
	/* ex)	
			PA.DIR = 0x3f;
			PD.DIR = 0x3f;
			PA.word = 0x3f;
			PD.word = 0x3f;	
	*/

#if 0
	SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOA );
	SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOD );
	GPIOPinTypeGPIOOutput( GPIO_PORTD_BASE, GPIO_PIN_2 );		/*	CLK	*/
	GPIOPinTypeGPIOOutput( GPIO_PORTD_BASE, GPIO_PIN_3 );		/*	CS	*/
	GPIOPinTypeGPIOInput( GPIO_PORTD_BASE, GPIO_PIN_6 );		/*	SEL	*/
	GPIOPinTypeGPIOOutput( GPIO_PORTA_BASE, GPIO_PIN_2 );		/*	DAT0	*/
	GPIOPinTypeGPIOOutput( GPIO_PORTA_BASE, GPIO_PIN_3 );		/*	DAT1	*/
	GPIOPinWrite( GPIO_PORTD_BASE, GPIO_PIN_2, GPIO_PIN_2);
	GPIOPinWrite( GPIO_PORTD_BASE, GPIO_PIN_3, GPIO_PIN_3);
	GPIOPinWrite( GPIO_PORTA_BASE, GPIO_PIN_2, GPIO_PIN_2 );
	GPIOPinWrite( GPIO_PORTA_BASE, GPIO_PIN_3, GPIO_PIN_3 );
#endif
	//P2_bit.no2 = 1;
	//P0_bit.no1 = 1; 

}

static unsigned int dtclk = 0; 
/*==============================================================================*/
/*  Func:   portReverseCLK                                                      */
/*  Desc:   Please describe the code to reverse CLK Port.                       */
/*==============================================================================*/
DT_INLINE void portReverseCLK(void)
{
	/* ex)
		PA.bit.b0 ^= 1;
	*/
	P2_bit.no2 = dtclk?1:0;
	dtclk ^= 1;

#if 0
	if( GPIOPinRead( GPIO_PORTD_BASE, GPIO_PIN_2 ) ){
		GPIOPinWrite( GPIO_PORTD_BASE, GPIO_PIN_2, 0 );
	}
	else{
		GPIOPinWrite( GPIO_PORTD_BASE, GPIO_PIN_2, GPIO_PIN_2 );
	}
#endif
}

/*==============================================================================*/
/*  Func:   portSetCLK                                                          */
/*  Desc:   Please describe the code to set the value to CLK Port.              */
/*==============================================================================*/
DT_INLINE void portSetCLK( DT_UINT dat)
{
	/* ex)
		PA.bit.b0 = dat?1:0;
	*/
	//GPIOPinWrite( GPIO_PORTD_BASE, GPIO_PIN_2, dat?0xff:0x00 );
}

/*==============================================================================*/
/*  Func:   portSetCS                                                           */
/*  Desc:   Please describe the code to set the value to CS Port.               */
/*==============================================================================*/
DT_INLINE void portSetCS(DT_UINT dat)
{
	/* ex)
		PA.bit.b1 = dat?1:0;
	*/
	//P0_bit.no1 = dat?1:0;
	//GPIOPinWrite( GPIO_PORTD_BASE, GPIO_PIN_3, dat?0xff:0x00 );
}

/*==============================================================================*/
/*  Func:   portSetDATA                                                         */
/*  Desc:   Please describe the code to set the value to DATA Port              */
/*==============================================================================*/
DT_INLINE void portSetDATA(DT_UINT dat)
{
	/* ex)
	 PA.word = (PA.word & 0xffcf) | (dat << 4);
	*/  
	 P0 =  (P0 & 0xF3) | (dat << 2);
	//GPIOPinWrite( GPIO_PORTA_BASE, (GPIO_PIN_2|GPIO_PIN_3), dat << 2 );
}

#if DT_WRITE_VARIABLE
/*==============================================================================*/
/*  Func:   portGetSEL                                                          */
/*  Desc:   Please describe the code to get the value from SEL Port             */
/*==============================================================================*/
DT_INLINE DT_UINT portGetSEL(void)
{
	/* ex)
		return PA.bit.b6;
	*/ 

	return GPIOPinRead( GPIO_PORTD_BASE, GPIO_PIN_6 );
}
#endif

/*==============================================================================*/
/*  Func:   enterCritical                                                       */
/*  Desc:   Please describe the code to enter Critical Section.                 */
/*==============================================================================*/
DT_INLINE void enterCritical(void)
{
	/* ex)
		DI();
	*/ 
	//DI();
	//taskENTER_CRITICAL();
}

/*==============================================================================*/
/*  Func:   exitCritical                                                        */
/*  Desc:   Please describe the code to exit Critical Section.                  */
/*==============================================================================*/
DT_INLINE void exitCritical(void)
{
	/* ex)
		EI();
	*/
	//EI();
	//taskEXIT_CRITICAL();
}

/*==============================================================================*/
/*  Don't change the code from here as possible.                                */
/*==============================================================================*/

/*==============================================================================*/
/*  Desc:   Test Point Parametaer                                               */
/*==============================================================================*/
#define		DT_VARIABLE_BIT			0x02
#define		DT_TARGET_TIME_BIT		0x04
#define		DT_EVTTRG_BIT			0x08
#define		DT_VARIABLE_FAST_BIT	0x01
#define		DT_EVTTRG_FAST_BIT		0x02
#define		DT_VARIABLE_WRITE_BIT	0x01

/*==============================================================================*/
/*  Func:   portInit                                                            */
/*  Desc:   Initialize Port Function                                            */
/*==============================================================================*/
static int init = 0;
DT_INLINE void portInit(void)
{
	if( init == 0 ){
		_TP_BusPortInit();
		init = 1;
	}
}

/*==============================================================================*/
/*  Func:   _TP_Bus2BitOutDrv                                                   */
/*  Desc:   2bit Data Output Function                                           */
/*==============================================================================*/
DT_INLINE void _TP_Bus2BitOutDrv( DT_UINT dat )
{
	portSetDATA( dat );
	portReverseCLK();
}

/*==============================================================================*/
/*  Func: _TP_BusOutDrv                                                         */
/*  Desc: Test Point Output Function                                            */
/*==============================================================================*/
DT_INLINE void _TP_BusOutDrv( DT_UINT addr, DT_UINT dat )
{
	portSetCS(0);
	_TP_Bus2BitOutDrv( dat >> 14 );
	_TP_Bus2BitOutDrv( dat >> 12 );
	_TP_Bus2BitOutDrv( dat >> 10 );
	_TP_Bus2BitOutDrv( dat >> 8 );
	_TP_Bus2BitOutDrv( dat >> 6 );
	_TP_Bus2BitOutDrv( dat >> 4 );
	_TP_Bus2BitOutDrv( dat >> 2 );
	_TP_Bus2BitOutDrv( dat );
	addr &= 0xfffff;
	if( addr >= 0x40000 ) _TP_Bus2BitOutDrv( addr >> 18 );
	if( addr >= 0x10000 ) _TP_Bus2BitOutDrv( addr >> 16 );
	if( addr >= 0x4000 ) _TP_Bus2BitOutDrv( addr >> 14 );
	if( addr >= 0x1000 ) _TP_Bus2BitOutDrv( addr >> 12 );
	if( addr >= 0x0400 ) _TP_Bus2BitOutDrv( addr >> 10 );
	if( addr >= 0x0100 ) _TP_Bus2BitOutDrv( addr >> 8 );
	if( addr >= 0x0040 ) _TP_Bus2BitOutDrv( addr >> 6 );
	if( addr >= 0x0010 ) _TP_Bus2BitOutDrv( addr >> 4 );
	if( addr >= 0x0004 ) _TP_Bus2BitOutDrv( addr >> 2 );
	_TP_Bus2BitOutDrv( addr );
	portSetCS(1);
}

/*==============================================================================*/
/*  Func:   _TP_BusOutByteDrv                                                   */
/*  Desc:   Byte Data Output Function                                           */
/*==============================================================================*/
DT_INLINE void _TP_BusOutByteDrv( DT_UINT dat )
{
	_TP_Bus2BitOutDrv( dat >> 6 );
	_TP_Bus2BitOutDrv( dat >> 4 );
	_TP_Bus2BitOutDrv( dat >> 2 );
	_TP_Bus2BitOutDrv( dat );
}

/*==============================================================================*/
/*  Func:   _TP_BusOutFastDrv                                                   */
/*  Desc:   Test Point Output(Fast) Function                                    */
/*==============================================================================*/
DT_INLINE void _TP_BusOutFastDrv( DT_UINT step, DT_UINT bit_num )
{
	portSetCS(0);
	if( bit_num >= 11 ) _TP_Bus2BitOutDrv( step >> 10 );
	if( bit_num >= 9 ) _TP_Bus2BitOutDrv( step >> 8 );
	if( bit_num >= 7 ) _TP_Bus2BitOutDrv( step >> 6 );
	if( bit_num >= 5 ) _TP_Bus2BitOutDrv( step >> 4 );
	if( bit_num >= 3 ) _TP_Bus2BitOutDrv( step >> 2 );
	_TP_Bus2BitOutDrv( step );
	portSetCS(1);
}

/*==============================================================================*/
/*  Func:   _TP_MemoryOutDrv                                                    */
/*  Desc:   Value Output Function                                               */
/*==============================================================================*/
DT_INLINE void _TP_MemoryOutDrv( unsigned char *p, DT_UINT size )
{
	if( size >= 256 ) size = 256;
	_TP_BusOutByteDrv( size );
	for( ; size != 0; --size, ++p ){
		_TP_BusOutByteDrv( *p );
	}
}

#if DT_WRITE_VARIABLE
/*==============================================================================*/
/*  Func:   _TP_InputByteDrv                                                    */
/*  Desc:   Byte Data Input Function                                            */
/*==============================================================================*/
DT_INLINE DT_UINT _TP_InputByteDrv( void )
{
	DT_UINT	i, c;
	c = 0;
	for( i = 0; i < 8; i++ ){
		portSetCLK(0);
		c <<= 1;
		if( portGetSEL() ) c |= 0x01;
		portSetCLK(1);
	}
	return c;
}

/*==============================================================================*/
/*  Func:   _TP_WritePointDrv                                                   */
/*  Desc:   Value Write Function                                                */
/*==============================================================================*/
DT_INLINE void _TP_WritePointDrv( unsigned char *p, DT_UINT size )
{
	int	i, s;
	for( i = 0; i < 50; i++ ){
		portSetCLK(0);
		s = portGetSEL();
		portSetCLK(1);
		if( s ) break;
	}
	if( s == 0 ) return ;
	s = _TP_InputByteDrv();
	if( s > 16 || s == 0 ) return;
	for( i = 0; i < s; i++, p++ ){
		*p = _TP_InputByteDrv();
	}
}
#endif

/*==============================================================================*/
/*  Func: _TP_BusOut                                                            */
/*  Desc: Called by Test Point                                                  */
/*==============================================================================*/
void _TP_BusOut( DT_UINT addr, DT_UINT dat )
{

	portInit();
	enterCritical();

	_TP_BusOutDrv( addr, dat );

	exitCritical();
}

/*==============================================================================*/
/*  Func:   _TP_MemoryOutput                                                    */
/*  Desc:   Called by Variable Test Point                                       */
/*==============================================================================*/
void _TP_MemoryOutput( DT_UINT addr, DT_UINT dat, void *value, DT_UINT size )
{

	portInit();
	enterCritical();

	_TP_BusOutDrv( addr | DT_VARIABLE_BIT, dat );

	/* 2nd data output */
	 portSetCS(0);

	/* output value */
	_TP_MemoryOutDrv( (unsigned char *)value, size );

	portSetCS(1);
	exitCritical();
}

/*==============================================================================*/
/*  Func:   _TP_EventTrigger                                                    */
/*  Desc:   Called by Event Trigger 12bit Test Point                            */
/*==============================================================================*/
void _TP_EventTrigger( DT_UINT addr, DT_UINT dat, DT_UINT event_id )
{

	portInit();
	enterCritical();

	_TP_BusOutDrv( addr | DT_EVTTRG_BIT, dat );

	 portSetCS(0);

	_TP_BusOutByteDrv(event_id);
	_TP_BusOutByteDrv((event_id >> 8) & 0x0f);

	portSetCS(1);

	exitCritical();
}

/*==============================================================================*/
/*  Func:   _TP_EventTrigger32                                                  */
/*  Desc:   Called by Event Trigger 32bit Test Point                            */
/*==============================================================================*/
void _TP_EventTrigger32( DT_UINT addr, DT_UINT dat, DT_UINT event_id )
{

	portInit();
	enterCritical();

	_TP_BusOutDrv( addr | DT_EVTTRG_BIT, dat );

	 portSetCS(0);

	_TP_BusOutByteDrv(event_id);
	_TP_BusOutByteDrv(event_id >> 8);
	_TP_BusOutByteDrv(event_id >> 16);
	_TP_BusOutByteDrv(event_id >> 24);

	portSetCS(1);
	exitCritical();
}

#if DT_WRITE_VARIABLE
/*==============================================================================*/
/*  Func:   _TP_WritePoint                                                      */
/*  Desc:   Called by Variable Write Test Point                                 */
/*==============================================================================*/
void _TP_WritePoint( DT_UINT addr, DT_UINT dat, void *value, DT_UINT size )
{

	addr |= DT_VARIABLE_WRITE_BIT;

	portInit();
	enterCritical();
	_TP_BusOutDrv( addr, dat );

	_TP_WritePointDrv( value, size );
	exitCritical();
}
#endif

/*==============================================================================*/
/*  Func:   _TP_BusOutFast                                                      */
/*  Desc:   Called by Fast Test Point                                           */
/*==============================================================================*/
void _TP_BusOutFast( DT_UINT step, DT_UINT bit_num )
{
	portInit();
	step = step << 2;
	bit_num = bit_num + 2;
	enterCritical();
	_TP_BusOutFastDrv( step, bit_num );
	exitCritical();
}

/*==============================================================================*/
/*  Func:   _TP_MemoryOutputFast                                                */
/*  Desc:   Called by Variable Fast Test Point                                  */
/*==============================================================================*/
void _TP_MemoryOutputFast( DT_UINT step, DT_UINT bit_num, void *value, DT_UINT size )
{
	portInit();
	step = (step << 2) + DT_VARIABLE_FAST_BIT;
	bit_num = bit_num + 2;
	enterCritical();
	_TP_BusOutFastDrv( step, bit_num );
	portSetCS(0);
	_TP_MemoryOutDrv( (unsigned char *)value, size );
	portSetCS(1);
	exitCritical();
}

/*==============================================================================*/
/*  Func: _TP_EventTriggerFast                                                  */
/*  Desc: Called by Event Trigger Fast Test Point                               */
/*==============================================================================*/
void _TP_EventTriggerFast( DT_UINT step, DT_UINT bit_num, DT_UINT event_id )
{
	portInit();
	enterCritical();
	step = (step << 2) + DT_EVTTRG_FAST_BIT;
	bit_num = bit_num + 2;
	_TP_BusOutFastDrv( step, bit_num );
	 portSetCS(0);
	_TP_BusOutByteDrv(event_id);
	_TP_BusOutByteDrv((event_id >> 8) & 0x0f);
	 portSetCS(1);
	exitCritical();
}

/*==============================================================================*/
/*  Func:   _TP_EventTrigger32Fast                                              */
/*  Desc:   Called by Event Trigger 32bit Fast Test Point                       */
/*==============================================================================*/
void _TP_EventTrigger32Fast( DT_UINT step, DT_UINT bit_num, DT_UINT event_id )
{
	portInit();
	enterCritical();
	step = (step << 2) + DT_EVTTRG_FAST_BIT;
	bit_num = bit_num + 2;
	_TP_BusOutFastDrv( step, bit_num );
	 portSetCS(0);
	_TP_BusOutByteDrv(event_id);
	_TP_BusOutByteDrv(event_id >> 8);
	_TP_BusOutByteDrv(event_id >> 16);
	_TP_BusOutByteDrv(event_id >> 24);
	 portSetCS(1);
	exitCritical();
}

/*==============================================================================*/


