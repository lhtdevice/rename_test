/**
 * @file MotorControl.h
 * @brief モータ制御
 */

#ifndef _MOTOR_CONTROL_H_
#define _MOTOR_CONTROL_H_
/*******************************************************************************
 * Include Files
 *******************************************************************************/

/*******************************************************************************
 * Macros
 *******************************************************************************/
#define MOTOR_CONTROL_PERIOD    0.01 // [s]
#define ACMOTOR_VREF			1.50
#define ACMOTOR_SENSE_MARGIN	0.10
#define ACMOTOR_SENSE_MIN		(ACMOTOR_VREF - ACMOTOR_SENSE_MARGIN) // 1.40[V]
#define ACMOTOR_SENSE_MAX		(ACMOTOR_VREF + ACMOTOR_SENSE_MARGIN) // 1.60[V]
#define MOTOR_WAIT_BRAKE		10 // [X10 ms] モータ停止後ブレーキをONにするまでの待ち時間
#define IM_WAIT_COUNT			20 // [X10 ms] モータ起動時のモータ電流無視時間

/*******************************************************************************
 * Type Definitions
 *******************************************************************************/
typedef enum {
    MOTOR_DIR_OPEN,
    MOTOR_DIR_CLOSE
} MOTOR_DIR;

typedef enum {
	MOTORST_STOP = 0,
	MOTORST_OPENING,
	MOTORST_CLOSING,
	MOTORST_ERROR
} MOTOR_STATE;

#define MOTORST_READY   MOTORST_STOP
#define MOTORST_UNKNOWN MOTORST_ERROR

typedef void (*MOTOR_CALLBACK)(void);

/*******************************************************************************
 * Function Prototypes
 *******************************************************************************/

/**
 * @brief モータの状態を取得する。
 */
MOTOR_STATE MotorControl_getState(void);

/**
 * @brief モータ制御初期化
 */
void MotorControl_init(void);

/**
 * @brief モータ動作開始
 * @param[in] dir 動作方向
 * @param[in] cb コールバック関数へのポインタ
 */
void MotorControl_start(MOTOR_DIR dir, MOTOR_CALLBACK cb);

/**
 * @brief モータ動作停止
 */
void MotorControl_stop(void);

/**
 * @brief パルス計測処理
 * @remark イベントカウンタ割り込みからコール
 */
void MotorControl_measurePulse(void);

/**
 * @brief パルス無観測時処理
 * @remark パルス計測用タイマ割り込みからコール
 */
void MotorControl_noPulseObserved(void);

/**
 * @brief モータ制御周期処理
 * @remark 周期制御用タイマ割り込みからコール
 */
void MotorControl_update(void);

#endif // _MOTOR_CONTROL_H_