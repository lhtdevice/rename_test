/**
 * @file  main.c
 * @brief プロジェクト実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#include "project.h"
#include "main.h"
#include "UE878.h"
#include "Motor.h"
#include "dev_digital.h"
#include "dev_timer.h"
#include "dev_pwm.h"
#include "dev_adc.h"
#include "dev_encoder.h"
#include "DebugMonitor.h"
#include "KenzaiApplication.h"

uint32_t g_dcm_rate = 50;
float    g_current_level = 0.0;
bool     g_ac_or_dc = false;
bool     g_dc_short_brake = false;
int16_t  g_wait_brake = -1;
uint32_t g_motor_start = 0;
uint32_t ignore_im_data = 0;

/*******************************************************************************
 * Macros
 *******************************************************************************/
#define SW_ON_THRESHOLD     4   // 40ms
#define SW_LONGON_THRESHOLD 200 // 2s

//! 実機での調整が必要 ->
#define IM_SMA_NUM          8   // IMのSMAデータ数
//#define IM_SMA_IGNORE_NUM   (IM_WAIT_COUNT * (10000 / 38))  // 動作開始時に除外するIMのデータ数
#define IM_SMA_IGNORE_NUM   IM_WAIT_COUNT  // 動作開始時に除外するIMのデータ数
//! <-

/*******************************************************************************
 * Private Function Prototypes
 *******************************************************************************/
static void loopMain(void);
static bool initDevices(void);

/*******************************************************************************
 * Private Variables
 *******************************************************************************/
//static THE_APP App;
static int32_t  id_timer_1ms   = TIMER_ID_ERROR;
static int32_t  id_timer_1s    = TIMER_ID_ERROR;
static uint32_t cnt_timer      = 0;
static volatile uint32_t sec_count = 0;
static float    im_data[IM_SMA_NUM];
static float    im_peak = 0.0;
static uint8_t  idx_imsma_next = 0;
static uint8_t  num_imsma_data = 0;

static uint32_t tm_over = 0;
static bool     occur_over = false;
static uint32_t cooldowning_count = 0;
static bool     detect_stop = false;

/**
 * @brief タイマ割り込みハンドラ。。
 * @param[in] sParam 未使用
 * @param[in] uParam 未使用
 * @param[out] -
 * @retval -
 */
static void handlerTimer1ms(const intptr_t sParam, const uintptr_t uParam)
{
    (void)sParam; (void)uParam;
    cnt_timer += 1;
	static uint8_t lsw_on = 0;
/***** ↓ADCを連続モードから手動モードに変更↓ *****/
	if (g_ac_or_dc)
		dev_adc_Start(ADCIDX_AC, DEVIO_ASYNC);
	else
		dev_adc_Start(ADCIDX_DC, DEVIO_ASYNC);
/***** ↑ADCを連続モードから手動モードに変更↑ *****/
	if (!g_ac_or_dc && !g_dc_short_brake) // DCで非ショートブレーキの場合のみリミットスイッチ有効
	{
		uint8_t lsw = GPIOIDX_LSWOPEN;
		if (EEQUIP_STATE_CLOSING == KenzaiApplication_getState()) lsw = GPIOIDX_LSWCLOSE;
		if (dev_digital_Read(lsw))
		{
			lsw_on += 1;
		}
		else
		{
			lsw_on = 0;
			detect_stop = false;
		}
	}
	if (g_dc_short_brake && (g_wait_brake > 0))
	{
		g_wait_brake -= 1;
	}
#ifndef EVAL_BOARD_ONLY
	if (lsw_on == 4)
	{
		detect_stop = true;
	}
#endif
#if 0
    if (cooldowning_count) cooldowning_count -= 1;
    if (LW_STOP_DETECT_TIME < dev_enc_getStopTime(ENCIDX_1))
    {
        if (!detect_stop)
        {
            detect_stop = true;
            KenzaiApplication_actQueue(EKENZAI_ACT_DETECT_STOP | KENZAI_ACT_EVENT_FLAG);
        }
    }
    else
    {
        detect_stop = false;
    }
#endif
}

/**
 * @brief リアルタイムクロック割り込みハンドラ。1秒カウンタをインクリメントする。
 * @param[in] sParam 未使用
 * @param[in] uParam 未使用
 * @param[out] -
 * @retval -
 */
static void handlerTimer1s(const intptr_t sParam, const uintptr_t uParam)
{
    (void)sParam; (void)uParam;
	sec_count += 1;
}

static void toggle_alert_led(void)
{
    // static bool led = 1;
    // led = led ? 0 : 1;
    // dev_digital_Write(GPIOIDX_ALERTLED, led);
}

float getAverage(const float v)
{
	static float sum = 0.0;
	sum -= im_data[idx_imsma_next];
	im_data[idx_imsma_next] = v;
	sum += v;
	idx_imsma_next = (idx_imsma_next + 1) % IM_SMA_NUM;
	num_imsma_data += 1;
	if (num_imsma_data >= IM_SMA_NUM)
	{
		num_imsma_data = IM_SMA_NUM;
	}
	return (sum / num_imsma_data);
}

float im_value = 0.0;
/**
 * @brief DCモータ電流ADC割り込みハンドラ。
 * @param[in] sParam：ADCチャネル（ADCIDX_DC）
 * @param[in] fParam：電流値[V]
 * @param[out] -
 * @retval -
*/
static void handlerAdcDc(const intptr_t sParam, const float fParam)
{
    (void)sParam; (void)fParam;

#if STUB_IMOTOR
    float im_v = fParam;
    sim_handlerAdcIm((intptr_t *)&sParam, &im_v);
#endif

	if (!occur_over)
	{
		//im_value = getAverage(fParam);
		im_value = fParam;
	}

	if (ignore_im_data < IM_SMA_IGNORE_NUM)
	{
		ignore_im_data += 1;
		im_peak = 0.0;
		return;
	}
	if (im_peak < fParam) im_peak = fParam;
	if (g_current_level <= im_value)
	{
		occur_over = true;
	}
	//else
	//{
	//	occur_over = false;
	//}
}

/**
 * @brief ACモータ電流ADC割り込みハンドラ。
 * @param[in] sParam：ADCチャネル（ADCIDX_AC）
 * @param[in] fParam：ACモータ電圧[V]
 * @param[out] -
 * @retval -
*/
static void handlerAdcAc(const intptr_t sParam, const float fParam)
{
	static float peak = ACMOTOR_VREF;
	static bool peak_hold = false;
	if (peak < fParam) peak = fParam;
	if ((cnt_timer % 8) == 0)
	{
		if (peak_hold)
		{
			im_value = peak;
			peak = ACMOTOR_VREF;
			peak_hold = false;
		}
	}
	else
	{
		peak_hold = true;
	}
}

/**
 * @brief モード設定
 * @remark オンボードモードSWを起動時のみ参照してモード設定を行う。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void setMode(void)
{
	uint8_t mode = (uint8_t)dev_digital_Read(GPIOIDX_MODE1);
	mode = ~mode & MODESW_MASK;
	// モードSWはGPIOIDX_MODE?で全ビットを参照する。
#if 0	// 製品
#else	// 試作
#ifdef EVAL_BOARD_ONLY
	g_ac_or_dc = false;
	g_dc_short_brake = true;
	g_dcm_rate = 10;
	g_current_level = 2.90;
#else
	g_ac_or_dc = ((mode & MODESW_MOTOR_MASK) == MODESW_AC_MOTOR);
	g_dc_short_brake = ((mode & MODESW_BRAKE_MASK) == MODESW_SHORT_BRAKE);
	switch (mode & MODESW_DUTY_MASK)
	{
	case MODESW_DUTY25:
		g_dcm_rate = 25;
		break;
	case MODESW_DUTY50:
		g_dcm_rate = 50;
		break;
	case MODESW_DUTY75:
		g_dcm_rate = 75;
		break;
	case MODESW_DUTY100:
	default:
		g_dcm_rate = 100;
		break;
	}
	switch (mode & MODESW_OVC_MASK)
	{
	case MODESW_OVC_0250:
		g_current_level = 0.25;
		break;
	case MODESW_OVC_0500:
		g_current_level = 0.50;
		break;
	case MODESW_OVC_0750:
		g_current_level = 0.75;
		break;
	case MODESW_OVC_1000:
	default:
		g_current_level = 1.00;
		break;
	}
#endif
#endif
}

/**
 * @brief アプリケーションメインルーチン。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void doApplication(void)
{
    bool bSuccess = initDevices();
    bSuccess &= KenzaiApplication_init(EKENZAI_EQUIP_DEMO); // ITALIA

    id_timer_1ms = dev_timer_setCallback(TIMERIDX_1MS, 1, TIMERCB_CONTINUOUS, handlerTimer1ms);
    id_timer_1s  = dev_timer_setCallback(TIMERIDX_1S,  1, TIMERCB_CONTINUOUS, handlerTimer1s);
    bSuccess &= (id_timer_1ms != TIMER_ID_ERROR);
    bSuccess &= (id_timer_1s  != TIMER_ID_ERROR);
    if (!bSuccess) haltByError();

    // オンボードスイッチイベント
    // dev_digital_Wait(GPIOIDX_FORCECLOSESW, 0, DEVIO_ASYNC_CONT, IOINT_FALLING);
    // dev_digital_Wait(GPIOIDX_MANUALSW, 0, DEVIO_ASYNC_CONT, IOINT_BOTH);

	if (g_ac_or_dc)
		dev_adc_Start(ADCIDX_AC, DEVIO_ASYNC);
	else
		dev_adc_Start(ADCIDX_DC, DEVIO_ASYNC);

    for (;;)
    {
        //! アプリケーションのメイン処理を記述する。
        loopMain();
    }
}

bool requireCooldown(void)
{
    return (0 < cooldowning_count);
}

#if 1
/**
 * @brief 起動からの時間（ms）を取得する。
 * @param[in] -
 * @param[out] -
 * @retval 起動からの時間[ms]
 */
uint32_t getTime(void)
{
    return dev_timer_getTicks(TIMERIDX_1MS);
}

/**
 * @brief 指定時間からの経過時間（ms）を取得する。
 * @param[in] start 開始時間
 * @param[out] -
 * @retval 経過時間[ms]
 */
uint32_t getElapsedTime(uint32_t start)
{
    uint32_t now = dev_timer_getTicks(TIMERIDX_1MS);
    if (now >= start) return now - start;
    else              return now + (ULONG_MAX - start);
}

/**
 * @brief 指定時間からの経過時間（s）を取得する。
 * @param[in] start 開始時間
 * @param[out] -
 * @retval 経過時間[s]
 */
uint32_t getElapsedSeconds(uint32_t start)
{
    uint32_t now = sec_count;
    if (now >= start) return now - start;
    else              return now + (ULONG_MAX - start);
}
#endif

/**
 * @brief NOP単位で処理を停止する。
 * @param[in] uint32_t NOP回数
 * @param[out] -
 * @retval -
 */
void pauseByNop(uint32_t num)
{
    for (; num>0; num--)
    {
        NOP();
    }
}

/**
 * @brief エラー停止ルーチン。ALERT LEDを高速点滅（5Hz）する。
 * @remarks タイマーが動作していない場合は点灯。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
void haltByError(void)
{
    // dev_digital_Write(GPIOIDX_ALERTLED, 1);
    for (;;)
    {
        if (0 == (cnt_timer % 10))
        {
            // toggle_alert_led();
        }
    }
}

/*******************************************************************************
 * Private Functions
 *******************************************************************************/
static bool initDevices(void)
{
    bool result = true;
    result &= initDeviceLibrary();
    DebugMonitor_init();
    result &= ue878_init(UARTIDX_COMMMODULE);
    result &= dev_digital_Initialize(GPIOIDX_MODE1, NULL);
	setMode();
    result &= dev_digital_Initialize(GPIOIDX_RAIN, NULL);//handlerRain);
    //result &= dev_digital_Initialize(GPIOIDX_THERMO, NULL);//handlerThermo);
    result &= dev_digital_Initialize(GPIOIDX_LSWOPEN, NULL);
    result &= dev_digital_Initialize(GPIOIDX_LSWCLOSE, NULL);
    result &= dev_digital_Initialize(GPIOIDX_OPEN, NULL);
    result &= dev_digital_Initialize(GPIOIDX_CLOSE, NULL);
    result &= dev_digital_Initialize(GPIOIDX_RS485IRQ, NULL);
    result &= dev_digital_Initialize(GPIOIDX_RS485CS, NULL);
    //result &= dev_digital_Initialize(GPIOIDX_RELAY1, NULL);
    //result &= dev_digital_Initialize(GPIOIDX_RELAY2, NULL);
    result &= dev_digital_Initialize(GPIOIDX_ACL1, NULL);
    result &= dev_digital_Initialize(GPIOIDX_ACL2, NULL);
    result &= dev_pwm_Initialize(PWMIDX_DCMOTOR, NULL);
    result &= dev_timer_Initialize(TIMERIDX_1MS, handlerTimer1ms);
    result &= dev_timer_Initialize(TIMERIDX_1S, handlerTimer1s);
	for (uint8_t ii=0; ii<IM_SMA_NUM; ii++)
	{
		im_data[ii] = 0.0;
	}
	if (g_ac_or_dc)
    	result &= dev_adc_Initialize(ADCIDX_AC, handlerAdcAc);
	else
    	result &= dev_adc_Initialize(ADCIDX_DC, handlerAdcDc);
    	//result &= dev_adc_Initialize(ADCIDX_DC, NULL);
    // result &= dev_enc_Initialize(ENCIDX_1, handlerEncoder);

    dbprintf(DEBUGLEVEL_DEBUG, "initDevices:%d "LIB_DEVICE_VERSION_STR STR_CRLF, result);
	// 以下を1行のprintf文にすると%fが正しく表示されない（0.00になる）。なぜだろう？
    //dbprintf(DEBUGLEVEL_DEBUG, "configure:%cC, PWM rate=%d%%, current limit:%.2fV"STR_CRLF,
	//	g_ac_or_dc ? 'A' : 'D', g_dcm_rate, g_current_level);
    dbprintf(DEBUGLEVEL_DEBUG, "configure:%cC %cB, PWM rate=%d%%, ",
		g_ac_or_dc ? 'A' : 'D', g_dc_short_brake ? 'S' : 'N', g_dcm_rate);
    dbprintf(DEBUGLEVEL_DEBUG, "current limit:%.2fV"STR_CRLF, g_current_level);

	static struct tm t;
	t.tm_year = 120;
	t.tm_mon  = 9;
	t.tm_mday = 30;
	t.tm_wday = 5;
	t.tm_hour = 16;
	t.tm_min  = 2;
	t.tm_sec  = 12;
	dev_timer_setRTC(TIMERIDX_1S, &t);
    return result;
}

static void loopMain(void)
{
#ifdef DEBUG
	static uint32_t tm_last = 0;
	static bool bPrint = true;
	if (KenzaiApplication_isMoving())
		bPrint = (100 <= getElapsedTime(tm_last));
	else
		bPrint = (3000 <= getElapsedTime(tm_last));
	if (bPrint)
	{
		tm_last = getTime();
		bPrint = false;
		if (KenzaiApplication_isMoving())
	    	dbprintf(DEBUGLEVEL_DEBUG, "motor current:%.2fV,%.2fV"STR_CRLF, im_value, im_peak);
		else
		{
			static struct tm t;
			if (dev_timer_getRTC(TIMERIDX_1S, &t))
			{
	    		dbprintf(DEBUGLEVEL_DEBUG, "%04d-%02d-%02d(%d) %02d:%02d:%02d"STR_CRLF,
					t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_wday, t.tm_hour, t.tm_min, t.tm_sec);
			}
	    	dbprintf(DEBUGLEVEL_DEBUG, "motor current:%.2fV,%.2fV"STR_CRLF, im_value, im_peak);
		}
	}
#endif
	if (occur_over)
	{
		// 過電流検出中は操作を受け付けない。
    	dbprintf(DEBUGLEVEL_DEBUG, "current over:%.2fV,%.2fV"STR_CRLF, im_value, im_peak);
		KenzaiApplication_actOperation(EKENZAI_ACT_OVERCURRENT);
		occur_over = false;
		im_value = 0.0;
		ignore_im_data = 0;
		return;
	}
	if (KenzaiApplication_isMoving())
	{
		if (IM_WAIT_COUNT < getElapsedTime(g_motor_start))
		{
#ifdef DUMMY_ACM
			if (detect_stop /*|| (MOTORST_STOP == MotorControl_getState())*/) // DC stop || AC stop
#else
			if (detect_stop || (MOTORST_STOP == MotorControl_getState())) // DC stop || AC stop
#endif
			{
				uint32_t tm = getElapsedTime(g_motor_start);
				KenzaiApplication_actOperation(EKENZAI_ACT_OPERATION_STOP);
				g_motor_start = tm;
			}
		}
	}
	if (g_dc_short_brake && (g_wait_brake == 0))
	{
		motor_Brake();
		g_wait_brake = -1;
	}
    EKENZAI_ACT_OPERATION cmd = ue878_getCommand();
	if ((EKENZAI_ACT_OPERATION_NONE <= cmd) && (cmd < EKENZAI_ACT_MAX))
	{
		// 受信がなくてもコールする（キュー処理のため）
		KenzaiApplication_actQueue(cmd);
	}
}

/* [] END OF FILE */
