/**
 * @file  main.h
 * @brief プロジェクト定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#ifndef PRJ_TEMPLATE_H
#define PRJ_TEMPLATE_H

#include "KenzaiAppTemplate.h"

#if 0	// 製品
	#define MODESW_ROOF_WINDOW	0x20
	#define MODESW_ROLL_SCREEN	0x10
	#define MODESW_OUTER_BLIND	0x08
	#define MODESW_SHADE		0x04
	#define MODESW_SLIDE_DOOR	0x02
	#define MODESW_SEGEL_SWING	0x01
#else	// 試作
	#define MODESW_AC_MOTOR		0x20
	#define MODESW_DC_MOTOR		0x00
	#define MODESW_MOTOR_MASK	0x20
	#define MODESW_SHORT_BRAKE	0x00
	#define MODESW_NO_BRAKE		0x10
	#define MODESW_BRAKE_MASK	0x10
	#define MODESW_DUTY25		0x00 // DCモータPWMデューティー25%
	#define MODESW_DUTY50		0x08
	#define MODESW_DUTY75		0x04
	#define MODESW_DUTY100		0x0C
	#define MODESW_DUTY_MASK	0x0C
//	#define MODESW_OVC_NONE		0x00 // 過電流検出なし
	#define MODESW_OVC_0250		0x00 // 過電流検出250mV
	#define MODESW_OVC_0500		0x02
	#define MODESW_OVC_0750		0x01
	#define MODESW_OVC_1000		0x03
//	#define MODESW_OVC_1500		0x05
//	#define MODESW_OVC_2000		0x03
//	#define MODESW_OVC_2500		0x07
	#define MODESW_OVC_MASK		0x03
	#define MODESW_MASK			0x3F
#endif

extern uint32_t g_dcm_rate;
extern float    g_current_level;
extern bool     g_ac_or_dc;
extern bool     g_dc_short_brake;
extern int16_t	g_wait_brake;
extern float    im_value;
extern uint32_t g_motor_start;
extern uint32_t ignore_im_data;

#define LW_WINDOW_WIDTH         1730    // [mm]
#define LW_FULL_WIDTH           3390    // [mm]

//! 実機での調整が必要 ->
#define LW_FULL_LENGTH          (1800.0) // 全閉⇔全開移動量[mm]
#define LW_ACCELERATION_LENGTH  (300.0)  // 始動時最大速加速長[mm]
#define LW_HEAD_GUARD_LENGTH    (200.0)  // 頭サイズガード位置[mm]
#define LW_MARGIN_M2H           (150.0)  // 最高速→頭ガード移行開始距離
#define LW_FINGER_GUARD_LENGTH  (30.0)   // 指サイズガード位置[mm]
#define LW_MARGIN_H2F           (20.0)   // 頭ガード→指ガード移行開始距離
#define LW_SPEED_MAX            (0.250)  // 最高速度[m/s]
#define LW_SPEED_NEAR_HEAD      (0.100)  // 端近くの速度[m/s]
#define LW_SPEED_NEAR_FINGER    (0.030)  // 最低速度[m/s]
#define LW_SPEED_MARGIN         (0.005)  // 速度比較マージン
#define LW_ACCELERATION_MAX     (10)     // モータ最大加速値（PWM rate）[‰]
#define LW_DECELERATION_MAX     (20)     // モータ最大減速値（PWM rate）[‰]
#define LW_ACCELERATION_MIN     (2)      // モータ加減速最小値（PWM rate）[‰]
#define LW_START_POWER          (50)     // 起動時の初期パワー（PWM rate）[‰]
#define LW_MIN_POWER            (30)     // 最小モータパワー（PWM rate）[‰]
#define LW_STOP_DETECT_TIME     (200)    // 動作中の停止検出時間[ms]
#define LW_ADC_REFERENCE        (3.3)    // ADC基準電圧
//! <-

typedef struct tag_TemplateApp {
    SKenzaiAppTemplate appCommon;
    //必要に応じて追加.
} THE_APP;

/* Read and write length of RGB LED data */
#define RGB_CHAR_DATA_LEN 4

typedef enum {
    ESLEEP_MODE_0,
    ESLEEP_MODE_1,
    ESLEEP_MODE_2,
    ESLEEP_MODE_NUM
} ESLEEP_MODE;

#define IMOTOR_OVER1TIME 1000 // [ms] モータ過電流1警告時間
#define IMOTOR_OVER2TIME 500  // [ms] モータ過電流2警告時間

#define COOLDOWN_TIME1   1000 // [ms] モータ過電流1冷却時間
#define COOLDOWN_TIME2   3000 // [ms] モータ過電流2冷却時間

SKenzaiAppTemplate* App_init(EKENZAI_EQUIP type);
void     doApplication(void);
void     haltByError(void);
void     pauseByNop(uint32_t num);
uint32_t getTime(void);
uint32_t getElapsedTime(const uint32_t start);
bool     requireCooldown(void);
bool     isInitialSetting(void);

#endif//PRJ_TEMPLATE_H
/* [] END OF FILE */
