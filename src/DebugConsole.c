/**
 * @file  DebugConsole.c
 * @brief CLI for debug
 */

#ifdef false
/*******************************************************************************
 * Include Files
 *******************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "DebugConsole.h"
#include "DebugMonitor.h"
//#include "dev_uart.h"
#include "r_cg_macrodriver.h"
#include "r_cg_serial.h"

#include "SpeedControl.h"

/*******************************************************************************
 * Private Macros
 *******************************************************************************/
#define MAX_INPUT_LENGTH    64
#define MAX_NUM_OF_ARGS     4

/*******************************************************************************
 * Private Type Definitions
 *******************************************************************************/
typedef int32_t (*CMD_HANDLER)(char *argv[]);

typedef struct {
    const char* const cmd;
    const char* const help;
    const CMD_HANDLER handler;
    int8_t num_args;    // <= MAX_NUM_OF_ARGS
} COMMAND_DEFINITION;

/*******************************************************************************
 * Private Function Prototypes
 *******************************************************************************/
static void parseInput(char *input);
static void showAllCommands(void);

/*** Command Handlers ***/
static int32_t setgain_handler(char *argv[]);
static int32_t getgain_handler(char *argv[]);
static int32_t setlpf_handler(char *argv[]);
static int32_t setspd_handler(char *argv[]);

/*******************************************************************************
 * Private Variables
 *******************************************************************************/
static uint8_t _rcv_buf;
static char _input_string[MAX_INPUT_LENGTH];

static const COMMAND_DEFINITION _commands[] = {
    {"setgain",
     "setgain Kp Ki\n"
     "  Set the PI gains to control motor speed.\n"
     "  Kp: Propotional Gain\n"
     "  Ki: Integral Gain\n",
     setgain_handler, 2},

    {"getgain",
     "getgain\n"
     "  Show the current PI gains to control motor speed.\n",
     getgain_handler, 0},

    {"setlpf",
     "setlpf ALPHA\n"
     "  Set the smoothing factor of the measured motor speed.\n"
     "  ALPHA: Smoothing factor (0.0-1.0)\n",
     setlpf_handler, 1},

    {"setspd",
     "setspd SECTION MAX_SPEED ACCEL DECEL\n"
     "  Set the speed profile of the specified section.\n"
     "  SECTION:   Section number (1-3)\n"
     "  MAX_SPEED: Max speed [mm/s]\n"
     "  ACCEL:     Acceleration (> 0) [mm/s^2]\n"
     "  DECEL:     Deceleration (< 0) [mm/s^2]\n",
     setspd_handler, 4}
};
#define NUM_OF_COMMANDS     (sizeof(_commands)/sizeof(_commands[0]))


/*
 * TODO
 * debugimpl.c: DebugOutput_init() から DebugConsole_init() 呼ぶ
 * dev_uart.c にDebugチャネルの受信割り込みハンドラ実装
 */
/*******************************************************************************
 * Public Functions
 *******************************************************************************/
// 
void DebugConsole_init(void)
{
    // dev_uart_Read(UARTIDX_DEBUG, _rcv_buf, 1, DEVIO_ASYNC_CONT);
    R_UART1_Receive(&_rcv_buf, 1);
}

// void DebugConsole_receive(const intptr_t channel, const uintptr_t length)
void DebugConsole_receive(void)
{
    static int8_t wp = 0;

    if (_rcv_buf == '\n') {
        if (wp == 0) {
            showAllCommands();
        }
        else {
            _input_string[wp] = '\0';
            wp = 0;
            parseInput(_input_string);
        }
    }
    else {
        _input_string[wp++] = _rcv_buf;

        if (wp >= MAX_INPUT_LENGTH) {
            wp = 0;
        }
    }

    R_UART1_Receive(&_rcv_buf, 1);
}

/*******************************************************************************
 * Private Functions
 *******************************************************************************/
static void parseInput(char *input)
{
    char *token;
    char *args[MAX_NUM_OF_ARGS] = {0};
    bool cmd_found = false;
    
    token = (char*)strtok(input, " ");
    if (token != NULL) {
        for (int i = 0; i < NUM_OF_COMMANDS; i++) {
            if (strncmp(token, _commands[i].cmd, strlen(_commands[i].cmd)) == 0) {
                bool execute = true;
                cmd_found = true;

                if (_commands[i].num_args > 0) {
                    int argc = 0;
                    while (token != NULL) {
                        token = (char*)strtok(NULL, " ");
                        if (token != NULL) {
                            if (argc >= MAX_NUM_OF_ARGS) {
                                execute = false;
                                dbprintf(DEBUGLEVEL_DEBUG,
                                         "The number of arguments should be less than %d.\n",
                                         MAX_NUM_OF_ARGS + 1);
                                break;
                            }
                            args[argc++] = token;
                        }
                    }

                    if (argc < _commands[i].num_args) {
                        execute = false;
                        dbprintf(DEBUGLEVEL_DEBUG, "Too few arguments.\n");
                        dbprintf(DEBUGLEVEL_DEBUG, "\n%s\n", _commands[i].help);
                    }
                }

                if (execute) {
                    if (!(*_commands[i].handler)(args)) {
                        dbprintf(DEBUGLEVEL_DEBUG, "\n%s\n", _commands[i].help);
                    }
                }
                break;
            }
        }
    }

    if (!cmd_found) {
        dbprintf(DEBUGLEVEL_DEBUG, "Command not found.\n");
    }
}

static void showAllCommands(void)
{
    dbprintf(DEBUGLEVEL_DEBUG, "[Command List]\n");
    for (int i = 0; i < NUM_OF_COMMANDS; i++) {
        dbprintf(DEBUGLEVEL_DEBUG, "%s\n", _commands[i].cmd);
    }
}

/*******************************************
 * Command Handlers
 *******************************************/
static int32_t setgain_handler(char *argv[])
{
    //float kp = (float)atof(argv[0]);
    //float ki = (float)atof(argv[1]);

    //SpeedControl_setGains(kp, ki);
    //dbprintf(DEBUGLEVEL_DEBUG, "Kp=%.1f, Ki=%.1f\n", kp, ki);
    return 0;
}

static int32_t getgain_handler(char *argv[])
{
    (void)argv;
    //float kp, ki;

    //SpeedControl_getGains(&kp, &ki);
    //dbprintf(DEBUGLEVEL_DEBUG, "Kp=%.1f, Ki=%.1f\n", kp, ki);
    return 0;
}

static int32_t setlpf_handler(char *argv[])
{
    float alpha = (float)atof(argv[0]);

    if ((alpha < 0.0) || (1.0 < alpha)) {
        return -1;
    }

    dbprintf(DEBUGLEVEL_DEBUG, "alpha=%f\n", alpha);
    return 0;
}

static int32_t setspd_handler(char *argv[])
{
    int32_t sec = atoi(argv[0]);
    if ((sec < 1) || (3 < sec)) {
        dbprintf(DEBUGLEVEL_DEBUG, "Invalid section number.\n");
        return -1;
    }

    float max = (float)atof(argv[1]);
    float acc = (float)atof(argv[2]);
    float dec = (float)atof(argv[3]);
    
//    SpeedControl_setSpeedProfile(sec - 1, max, acc, dec);
    return 0;
}

#endif // DEBUG