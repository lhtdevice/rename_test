/**
 * @file ActLogManager.h
 * @brief ログを管理する。ログとして、コマンド履歴と、送信データ履歴がある。そのための構造体を定義している
*/
#ifndef ACT_LOG_MANAGER_H
#define ACT_LOG_MANAGER_H

//#include "cytypes.h"
#include <stdint.h>
#include "KenzaiActDefine.h"
#include "KenzaiState.h"

#define MAX_HISTORY (20)

typedef struct KenzaiActHistory_tag {
    EKENZAI_ACT_OPERATION history[MAX_HISTORY];
    uint16_t index;
} SKenzaiActHistory;

typedef struct KenzaiSendHistory_tag {
    uint32_t history[MAX_HISTORY];
    uint16_t index;
} SKenzaiSendHistory;

typedef struct KenzaiActLog_tag {
    SKenzaiActHistory act_history;
    SKenzaiSendHistory send_history;
} SKenzaiActLog;

void ActLog_init(void);
void ActLog_append(EKENZAI_ACT_OPERATION);
void ActLog_Output(void);
void SendHistory_Output(void);

void SendHistory_append(const void * const data);

#endif
/* [] END OF FILE */
