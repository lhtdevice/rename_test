﻿/**
 * @file  motor_control.h
 * @brief モータ制御定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#ifndef MOTOR_CONTROL_H
#define MOTOR_CONTROL_H

/**
* @enum POSINDEX
* @brief LWの動作位置を示す
*/
typedef enum {
    POSIDX_START = 0,
    POSIDX_MAX_BEGIN,
    POSIDX_MAX_END,
    POSIDX_HEAD_BEGIN,
    POSIDX_HEAD_END,
    POSIDX_FINGER,
    POSIDX_FULL,
    POSIDX_MAX
} POSINDEX;

#define POSIDX_ERROR POSIDX_MAX
#define POSIDX_NUM   POSIDX_MAX

int32_t  updatePosition(int32_t cnt, uint8_t dir);
float    getSpeedTarget(int32_t pos, uint8_t dir);
float    getSpeedTargetAtCurrentPosition(void);
float    getSpeedMax(void);
POSINDEX getPositionId(int32_t pos, uint8_t dir);
void     setPositionId(int32_t enc_fullopen);
void     updatePositionIdByOpen(int32_t enc_fullopen);
void     updatePositionIdByClose(int32_t enc_fullclose);
void     eventFullOpen(bool initalize);
void     eventFullClose(bool initalize);
bool     motor_start(uint8_t dir);
bool     motor_stop(void);

#endif//MOTOR_CONTROL_H