/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
/**
 * @file KenzaiActDefine.c
 * @brief 機器に対する操作のインターフェイスを定義する。
 */
#include "KenzaiActDefine.h"
#include "KenzaiActRegist.h"

//!U
static uint8_t KenzaiActDefine_RegistStop(SKenzaiState* state)
{
    switch (state->equipState)
    {
    case EEQUIP_STATE_OPENING:
    case EEQUIP_STATE_CLOSING:
        KenzaiActRegist_Regist(EACT_CHANGE_TO_OTHER);
        return 1;
    case EEQUIP_STATE_OPENED:
    case EEQUIP_STATE_CLOSED:
    case EEQUIP_STATE_STOPPED:
    case EEQUIP_STATE_VENTILATION_OPENING:
    case EEQUIP_STATE_VENTILATION_CLOSING:
    case EEQUIP_STATE_VENTILATION_STOPPED:
    case EEQUIP_STATE_VENTILATION_OPENED:
    case EEQUIP_STATE_BLIND_STOPPED:
    case EEQUIP_STATE_BLIND_OPENING:
    case EEQUIP_STATE_BLIND_OPENED:
    case EEQUIP_STATE_BLIND_CLOSING:
    case EEQUIP_STATE_BLIND_CLOSED:
    case EEQUIP_STATE_ALLLATCH_LOCKED:
    case EEQUIP_STATE_ALLLATCH_UNLOCKED:
    case EEQUIP_STATE_LATCH1_LOCKED:
    case EEQUIP_STATE_LATCH1_UNLOCKED:
    case EEQUIP_STATE_LATCH2_LOCKED:
    case EEQUIP_STATE_LATCH2_UNLOCKED:
    case EEQUIP_STATE_LATCH3_LOCKED:
    case EEQUIP_STATE_LATCH3_UNLOCKED:
    case EEQUIP_STATE_LATCH4_LOCKED:
    case EEQUIP_STATE_LATCH4_UNLOCKED:
    case EEQUIP_STATE_DOOR_OPENED:
    case EEQUIP_STATE_DOOR_FULLOPENED:
    case EEQUIP_STATE_DOOR_CLOSED:
    case EEQUIP_STATE_WINDOW_OPENED:
    case EEQUIP_STATE_WINDOW_FULLOPENED:
    case EEQUIP_STATE_WINDOW_CLOSED:
    case EEQUIP_STATE_UNKNOWN:
    case EEQUIP_STATE_NUM: // この状態はない（default）
    default:
        ; // nothing to do
    }
    return 0;
}

//!U
static uint8_t KenzaiActDefine_RegistOpen(SKenzaiState* state)
{
    switch (state->equipState)
    {
    case EEQUIP_STATE_STOPPED:
    case EEQUIP_STATE_CLOSING:
    case EEQUIP_STATE_CLOSED:
        KenzaiActRegist_Regist(EACT_CHANGE_TO_OTHER);
        return 1;
    case EEQUIP_STATE_OPENING:
    case EEQUIP_STATE_OPENED:
    case EEQUIP_STATE_VENTILATION_OPENING:
    case EEQUIP_STATE_VENTILATION_CLOSING:
    case EEQUIP_STATE_VENTILATION_STOPPED:
    case EEQUIP_STATE_VENTILATION_OPENED:
    case EEQUIP_STATE_BLIND_STOPPED:
    case EEQUIP_STATE_BLIND_OPENING:
    case EEQUIP_STATE_BLIND_OPENED:
    case EEQUIP_STATE_BLIND_CLOSING:
    case EEQUIP_STATE_BLIND_CLOSED:
    case EEQUIP_STATE_ALLLATCH_LOCKED:
    case EEQUIP_STATE_ALLLATCH_UNLOCKED:
    case EEQUIP_STATE_LATCH1_LOCKED:
    case EEQUIP_STATE_LATCH1_UNLOCKED:
    case EEQUIP_STATE_LATCH2_LOCKED:
    case EEQUIP_STATE_LATCH2_UNLOCKED:
    case EEQUIP_STATE_LATCH3_LOCKED:
    case EEQUIP_STATE_LATCH3_UNLOCKED:
    case EEQUIP_STATE_LATCH4_LOCKED:
    case EEQUIP_STATE_LATCH4_UNLOCKED:
    case EEQUIP_STATE_DOOR_OPENED:
    case EEQUIP_STATE_DOOR_FULLOPENED:
    case EEQUIP_STATE_DOOR_CLOSED:
    case EEQUIP_STATE_WINDOW_OPENED:
    case EEQUIP_STATE_WINDOW_FULLOPENED:
    case EEQUIP_STATE_WINDOW_CLOSED:
    case EEQUIP_STATE_UNKNOWN:
    case EEQUIP_STATE_NUM: // この状態はない（default）
    default:
        ; // nothing to do
    }
    return 0;
}

//!U
static uint8_t KenzaiActDefine_RegistClose(SKenzaiState* state)
{
    switch (state->equipState)
    {
    case EEQUIP_STATE_STOPPED:
    case EEQUIP_STATE_OPENING:
    case EEQUIP_STATE_OPENED:
        KenzaiActRegist_Regist(EACT_CHANGE_TO_CLOSE);
        return 1;
    case EEQUIP_STATE_CLOSING:
    case EEQUIP_STATE_CLOSED:
    case EEQUIP_STATE_VENTILATION_OPENING:
    case EEQUIP_STATE_VENTILATION_CLOSING:
    case EEQUIP_STATE_VENTILATION_STOPPED:
    case EEQUIP_STATE_VENTILATION_OPENED:
    case EEQUIP_STATE_BLIND_STOPPED:
    case EEQUIP_STATE_BLIND_OPENING:
    case EEQUIP_STATE_BLIND_OPENED:
    case EEQUIP_STATE_BLIND_CLOSING:
    case EEQUIP_STATE_BLIND_CLOSED:
    case EEQUIP_STATE_ALLLATCH_LOCKED:
    case EEQUIP_STATE_ALLLATCH_UNLOCKED:
    case EEQUIP_STATE_LATCH1_LOCKED:
    case EEQUIP_STATE_LATCH1_UNLOCKED:
    case EEQUIP_STATE_LATCH2_LOCKED:
    case EEQUIP_STATE_LATCH2_UNLOCKED:
    case EEQUIP_STATE_LATCH3_LOCKED:
    case EEQUIP_STATE_LATCH3_UNLOCKED:
    case EEQUIP_STATE_LATCH4_LOCKED:
    case EEQUIP_STATE_LATCH4_UNLOCKED:
    case EEQUIP_STATE_DOOR_OPENED:
    case EEQUIP_STATE_DOOR_FULLOPENED:
    case EEQUIP_STATE_DOOR_CLOSED:
    case EEQUIP_STATE_WINDOW_OPENED:
    case EEQUIP_STATE_WINDOW_FULLOPENED:
    case EEQUIP_STATE_WINDOW_CLOSED:
    case EEQUIP_STATE_UNKNOWN:
    case EEQUIP_STATE_NUM: // この状態はない（default）
    default:
        ; // nothing to do
    }
    return 0;
}

/**
 * @brief 操作の結果に応じた処理を実施する。
 * @param[in] state 現在の機器の状態
 * @param[in] act 操作
 * @param[out] -
 * @pre 操作に対するアクションが実施済み
 * @post -
 * @retval -
*/
void KenzaiActDefine_RegistChanged(SKenzaiState* state, EKENZAI_ACT_OPERATION act)
{
    switch (act)
    {
    case EKENZAI_ACT_OPERATION_STOP:
        KenzaiActDefine_RegistStop(state);
        break;
    case EKENZAI_ACT_OPERATION_OPEN:
        KenzaiActDefine_RegistOpen(state);
        break;
    case EKENZAI_ACT_OPERATION_CLOSE:
        KenzaiActDefine_RegistClose(state);
        break;
    case EKENZAI_ACT_OPERATION_NOACT:
    case EKENZAI_ACT_OVERCURRENT:
    default:
        ; // nothing to do
    }
}

/* [] END OF FILE */
