/**
 * @file DebugConsole.h
 * @brief CLI for debug
 */

#ifndef _DEBUG_CONSOLE_H_
#define _DEBUG_CONSOLE_H_
/*******************************************************************************
 * Include Files
 *******************************************************************************/
#include <stdbool.h>

/*******************************************************************************
 * Macros
 *******************************************************************************/

/*******************************************************************************
 * Type Definitions
 *******************************************************************************/

/*******************************************************************************
 * Function Prototypes
 *******************************************************************************/
extern void DebugConsole_init(void);
//extern void DebugConsole_receive(const intptr_t channel, const uintptr_t length);
extern void DebugConsole_receive(void);

#endif // _DEBUG_CONSOLE_H_