/*
 Copyright (C) 2009-2020, Heartland.Data Inc. All rights reserved.        
                                                                          
 This software is furnished under a license and may be used and copied    
 only in accordance with the terms of such license and with the inclusion 
 of the above copyright notice. No title to and ownership of the software 
 is transferred.                                                          
 Heartland.Data Inc. makes no representation or warranties with           
 respect to the performance of this computer program, and specifically    
 disclaims any responsibility for any damages, special or consequential,  
 connected with the use of this program.                                  
*/

#ifndef	__DT_KenzaiApplication_h__
#define	__DT_KenzaiApplication_h__

#ifdef	__cplusplus
	#define	__DtEXTERN	extern "C"
#else
	#define	__DtEXTERN	extern
#endif

/* TestPoint MacroCode -----------------------------------------------------*/
#ifdef		__DtBaseAddress
#undef		__DtBaseAddress
#endif
#define		__DtBaseAddress		0x10
#define		__DtAllEnable		1
#if ( __DtAllEnable == 1 )
#define		__DtTestPoint(func, step)		__Dt##func##step
#define		__DtTestPointValue(func, step, value, size)		__Dt##func##step(value,size)
#define		__DtTestPointWrite(func, step, value, size)		__Dt##func##step(value,size)
#define		__DtTestPointEventTrigger(func, step, data)		__Dt##func##step(data)
#define		__DtTestPointEventTrigger32(func, step, data)		__Dt##func##step(data)
#define		__DtTestPointFuncCall(func, step, call)		__Dt##func##step(call)
#define		__DtTestPointFast(step, bit_num)		__DtFast_##step(step, bit_num)
#define		__DtTestPointValueFast(step, bit_num, value, size)		__DtFast_##step(step, bit_num, value, size)
#define		__DtTestPointEventTriggerFast(step, bit_num, data)		__DtFast_##step(step, bit_num, data)
#define		__DtTestPointEventTrigger32Fast(step, bit_num, data)		__DtFast_##step(step, bit_num, data)
#else
#define		__DtTestPoint(func, step)		
#define		__DtTestPointValue(func, step, value, size)		
#define		__DtTestPointWrite(func, step, value, size)		
#define		__DtTestPointEventTrigger(func, step, data)		
#define		__DtTestPointEventTrigger32(func, step, data)		
#define		__DtTestPointFuncCall(func, step, call)		call
#define		__DtTestPointFast(step, bit_num)		
#define		__DtTestPointValueFast(step, bit_num, value, size)		
#define		__DtTestPointEventTriggerFast(step, bit_num, data)		
#define		__DtTestPointEventTrigger32Fast(step, bit_num, data)		
#endif
__DtEXTERN		void	_TP_BusOut( unsigned int addr, unsigned int dat );
__DtEXTERN		void	_TP_MemoryOutput( unsigned int addr, unsigned int dat, void *value, unsigned int size );
__DtEXTERN		void	_TP_WritePoint( unsigned int addr, unsigned int dat, void *value, unsigned int size );
__DtEXTERN		void	_TP_EventTrigger( unsigned int addr, unsigned int dat, unsigned int event_id );
__DtEXTERN		void	_TP_EventTrigger32( unsigned int addr, unsigned int dat, unsigned int event_id );
__DtEXTERN		void	_TP_BusOutFast( unsigned int step, unsigned int bit_num );
__DtEXTERN		void	_TP_MemoryOutputFast( unsigned int step, unsigned int bit_num, void *value, unsigned int size );
__DtEXTERN		void	_TP_EventTriggerFast( unsigned int step, unsigned int bit_num, unsigned int event_id );
__DtEXTERN		void	_TP_EventTrigger32Fast( unsigned int step, unsigned int bit_num, unsigned int event_id );

/* TestPoint FuncList ------------------------------------------------------*/
#define		__DtFunc_clearQueue		0
#define		__DtFunc_readQueue		1
#define		__DtFunc_writeQueue		2
#define		__DtFunc_KenzaiApplication_getEquipType		3
#define		__DtFunc_KenzaiApplication_init		4
#define		__DtFunc_KenzaiApplication_actOperation		5
#define		__DtFunc_KenzaiApplication_actQueue		6
#define		__DtFunc_KenzaiApplication_OutputState		7
#define		__DtFunc_KenzaiApplication_getState		8
#define		__DtFunc_KenzaiApplication_isMoving		9
#define		__DtFunc_KenzaiApplication_isInitalSet		10
#define		__DtFunc_KenzaiApplication_restartFromOverheat		11
#define		__DtFunc_KenzaiApplication_handlerDetectOverCurrent		12
#define		__DtFunc_KenzaiApplication_checkState		13
#define		__DtFunc_KenzaiApplication_updateState		14
#define		__DtFunc_sendAck		15
#define		__DtFunc_setResponseMode		16
#define		__DtFunc_setCooldown		17
#define		__DtFunc_callbackTimeoutMove		18
#define		__DtFunc_callbackTimeoutCool1		19
#define		__DtFunc_callbackTimeoutCool3		20
#define		__DtFunc_callbackMotor		21
#define		__DtFunc_actStop		22
#define		__DtFunc_actOpen		23
#define		__DtFunc_actClose		24

/* TestPoint StepList ------------------------------------------------------*/
#define		__DtStep_0		0
#define		__DtStep_1		1
#define		__DtStep_2		2
#define		__DtStep_3		3
#define		__DtStep_4		4
#define		__DtStep_5		5
#define		__DtStep_6		6
#define		__DtStep_7		7
#define		__DtStep_8		8
#define		__DtStep_9		9

/* TestPoint DisableList ---------------------------------------------------*/
#define	__DtFast_0(step,bit_num)	/*FuncIn*/	
#define	__DtFast_1(step,bit_num)	/*FuncOut*/	
#define	__DtFast_2(step,bit_num)	/*FuncIn*/	
#define	__DtFast_3(step,bit_num)	/*if*/	
#define	__DtFast_4(step,bit_num,value,size)	/*Value*/	
#define	__DtFast_5(step,bit_num)	/*FuncOut*/	
#define	__DtFast_6(step,bit_num)	/*FuncIn*/	
#define	__DtFast_7(step,bit_num)	/*if*/	
#define	__DtFast_8(step,bit_num)	/*FuncOut*/	
#define	__DtFast_9(step,bit_num)	/*FuncIn+FuncOut*/	
#define	__DtFast_10(step,bit_num)	/*FuncIn*/	
#define	__DtFast_11(step,bit_num)	/*FuncIn*/	
#define	__DtFast_12(step,bit_num)	/*switch*/	
#define	__DtFast_13(step,bit_num)	/*switch*/	
#define	__DtFast_14(step,bit_num)	/*switch*/	
#define	__DtFast_15(step,bit_num)	/*switch*/	
#define	__DtFast_16(step,bit_num)	/*switch*/	
#define	__DtFast_17(step,bit_num)	/*switch*/	
#define	__DtFast_18(step,bit_num)	/*switch+FuncOut*/	
#define	__DtFast_19(step,bit_num)	/*FuncOut*/	
#define	__DtFast_20(step,bit_num)	/*FuncIn*/	
#define	__DtFast_21(step,bit_num)	/*if*/	
#define	__DtFast_22(step,bit_num)	/*FuncOut*/	
#define	__DtFast_23(step,bit_num)	/*FuncOut*/	
#define	__DtFast_24(step,bit_num)	/*FuncIn*/	
#define	__DtFast_25(step,bit_num)	/*FuncOut*/	
#define	__DtFast_26(step,bit_num)	/*FuncIn+FuncOut*/	
#define	__DtFast_27(step,bit_num)	/*FuncIn+FuncOut*/	
#define	__DtFast_28(step,bit_num)	/*FuncIn+FuncOut*/	
#define	__DtFast_29(step,bit_num)	/*FuncIn*/	
#define	__DtFast_30(step,bit_num)	/*if*/	
#define	__DtFast_31(step,bit_num)	/*FuncOut*/	
#define	__DtFast_32(step,bit_num)	/*FuncIn*/	
#define	__DtFast_33(step,bit_num)	/*if*/	
#define	__DtFast_34(step,bit_num)	/*if*/	
#define	__DtFast_35(step,bit_num)	/*if*/	
#define	__DtFast_36(step,bit_num)	/*if*/	
#define	__DtFast_37(step,bit_num)	/*FuncOut*/	
#define	__DtFast_38(step,bit_num)	/*FuncIn*/	
#define	__DtFast_39(step,bit_num)	/*if*/	
#define	__DtFast_40(step,bit_num)	/*if*/	
#define	__DtFast_41(step,bit_num)	/*FuncOut*/	
#define	__DtFast_42(step,bit_num)	/*FuncIn*/	
#define	__DtFast_43(step,bit_num)	/*FuncOut*/	
#define	__DtFast_44(step,bit_num)	/*FuncIn*/	
#define	__DtFast_45(step,bit_num)	/*if*/	
#define	__DtFast_46(step,bit_num)	/*if*/	
#define	__DtFast_47(step,bit_num)	/*FuncOut*/	
#define	__DtFast_48(step,bit_num)	/*FuncIn*/	
#define	__DtFast_49(step,bit_num)	/*switch*/	
#define	__DtFast_50(step,bit_num)	/*FuncOut*/	
#define	__DtFast_51(step,bit_num)	/*FuncIn*/	
#define	__DtFast_52(step,bit_num)	/*if*/	
#define	__DtFast_53(step,bit_num)	/*if*/	
#define	__DtFast_54(step,bit_num)	/*if*/	
#define	__DtFast_55(step,bit_num)	/*if*/	
#define	__DtFast_56(step,bit_num)	/*FuncOut*/	
#define	__DtFast_57(step,bit_num)	/*FuncIn*/	
#define	__DtFast_58(step,bit_num)	/*FuncOut*/	
#define	__DtFast_59(step,bit_num)	/*FuncIn*/	
#define	__DtFast_60(step,bit_num)	/*FuncOut*/	
#define	__DtFast_61(step,bit_num)	/*FuncIn*/	
#define	__DtFast_62(step,bit_num)	/*FuncOut*/	
#define	__DtFast_63(step,bit_num)	/*FuncIn*/	
#define	__DtFast_64(step,bit_num)	/*FuncOut*/	
#define	__DtFast_65(step,bit_num)	/*FuncIn*/	_TP_BusOutFast( step, bit_num );
#define	__DtFast_66(step,bit_num)	/*switch*/	
#define	__DtFast_67(step,bit_num)	/*if*/	
#define	__DtFast_68(step,bit_num)	/*if*/	
#define	__DtFast_69(step,bit_num)	/*switch*/	
#define	__DtFast_70(step,bit_num)	/*switch*/	
#define	__DtFast_71(step,bit_num)	/*if*/	
#define	__DtFast_72(step,bit_num)	/*if*/	
#define	__DtFast_73(step,bit_num)	/*switch*/	
#define	__DtFast_74(step,bit_num)	/*FuncOut*/	
#define	__DtFast_75(step,bit_num)	/*FuncIn*/	_TP_BusOutFast( step, bit_num );
#define	__DtFast_76(step,bit_num)	/*switch*/	
#define	__DtFast_77(step,bit_num)	/*if*/	
#define	__DtFast_78(step,bit_num)	/*switch*/	
#define	__DtFast_79(step,bit_num)	/*if*/	
#define	__DtFast_80(step,bit_num)	/*FuncOut*/	
#define	__DtFast_81(step,bit_num)	/*FuncIn*/	_TP_BusOutFast( step, bit_num );
#define	__DtFast_82(step,bit_num)	/*switch*/	
#define	__DtFast_83(step,bit_num)	/*if*/	
#define	__DtFast_84(step,bit_num)	/*switch*/	
#define	__DtFast_85(step,bit_num)	/*if*/	
#define	__DtFast_86(step,bit_num)	/*FuncOut*/	

#endif	/* __DT_KenzaiApplication_h__ */

