/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
/**
 * @file ActLogManager.c
 * @brief ログを管理する。ログとして、操作履歴と、送信データ履歴がある。
*/
#include "ActLogManager.h"
#include <string.h>
#include "DebugMonitor.h"

static SKenzaiActLog act_log;

/**
 * @brief 初期化する。
 * @param[in] -
 * @param[out] -
 * @pre -
 * @post -
 * @retval -
 */
void ActLog_init(void)
{
    memset(&act_log, 0, sizeof(SKenzaiActLog));
}

/**
  * @brief コマンドに対する操作を記録する。
  * @param[in] act ログとして記録する操作
  * @param[out] -
  * @pre -
  * @post -
  * @retval -
 */
void ActLog_append(EKENZAI_ACT_OPERATION act)
{
    if( act_log.act_history.index >= MAX_HISTORY) { 
        act_log.act_history.index = 0;
    }
    act_log.act_history.history[act_log.act_history.index] = act;
    act_log.act_history.index++;

}

/**
 * @brief 操作ログを文字列として出力する。
 * @param[in] -
 * @param[out] -
 * @pre -
 * @post -
 * @retval -
*/
void ActLog_Output(void)
{
    //DebugOutputWithInt("Print Act Log Curent:", act_log.act_history.index);
	uint16_t k;
    for( k = 0; k < act_log.act_history.index; k++) {
        DEBUG_OUTPUT_WITHUINT("Log Index:", k);
        DEBUG_OUTPUT_WITHUINT("    ", act_log.act_history.history[k]);
    }
}

/**
 * @brief 送信データを履歴に保存する。
 * @param[in] data 記録する送信データ配列（4byte）の先頭アドレス
 * @param[out] -
 * @pre -
 * @post -
 * @retval -
 * @remarks 4byte固定にしているのがいまいち。変更したい。
*/
void SendHistory_append(const void * const data)
{
    uint8_t *ad = (uint8_t *)data;
    if( act_log.send_history.index >= MAX_HISTORY ) {
        act_log.send_history.index = 0;    
    }
    act_log.send_history.history[act_log.send_history.index]
        = ((uint32_t)ad[0] << 24)| ((uint32_t)ad[1] << 16) | ((uint32_t)ad[2] << 8) | ((uint32_t)ad[3]);
    act_log.send_history.index++;    
   
}

/**
 * @brief 送信データ履歴を文字列として出力する。
 * @param[in] -
 * @param[out] -
 * @pre -
 * @post -
 * @retval -
*/
void SendHistory_Output(void)
{
	uint16_t k;
    DEBUG_OUTPUT_WITHUINT("Print Send Log Current:", act_log.send_history.index);
    for( k = 0; k < act_log.send_history.index; k++) {
        DEBUG_OUTPUT_WITHUINT("Log Index:", k);
        DEBUG_OUTPUT_WITHUINT("    ", act_log.send_history.history[k]);
    }
}

/* [] END OF FILE */
