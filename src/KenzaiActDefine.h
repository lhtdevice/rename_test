/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
/**
 * @file KenzaiActDefine.h
 * @brief 機器に対する操作のインターフェイスを定義する。
 */

#ifndef KENZAIACTDEFINE_H
#define KENZAIACTDEFINE_H

#include "KenzaiState.h"
#include "KenzaiActOperation.h"


typedef void (*KENZAI_CONTROLL_FUNC)(SKenzaiState *);
    // 操作の共通関数.

/**
* @struct SKenzaiActDefine
* @brief 建材機器に対する操作のインターフェイスをまとめて格納する。
*/
typedef struct KenzaiActDefine_tag {
    KENZAI_CONTROLL_FUNC actOpenPtr;		///< 開操作.
    KENZAI_CONTROLL_FUNC actClosePtr;		///< 閉操作.
    KENZAI_CONTROLL_FUNC actStopPtr;		///< 停止操作.
    KENZAI_CONTROLL_FUNC actLockPtr;		///< 施錠操作.
    KENZAI_CONTROLL_FUNC actUnlockPtr;		///< 解錠操作.
    KENZAI_CONTROLL_FUNC actCheckStatePtr;  ///< 状態確認（暫定）.
} SKenzaiActDefine;

void KenzaiActDefine_RegistChanged(SKenzaiState* state, EKENZAI_ACT_OPERATION act);

#endif
/* [] END OF FILE */
