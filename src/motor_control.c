﻿/**
 * @file  motor_control.c
 * @brief モータ制御実装ファイル
 * @copyright (c) LIXIL Corporation, 2018-2019 All Rights Reserved.
 */

#include "project.h"
#include "LibDevice.h"
#include "dev_encoder.h"
#include "Motor.h"
#include "KenzaiState.h"
#include "motor_control.h"
#include "main.h"

/**
* @brief LWの動作位置を示す。エンコーダ1/4周期のカウント数
* @remark 初期設定終了後に設定される。
*/
static int32_t pos_open[POSIDX_NUM];
static int32_t pos_close[POSIDX_NUM];

#define LWPOS_FULLCLOSE 0
static uint8_t current_dir = DIR_STOP;
static int32_t start_position = 0;
static uint16_t pwm_rate = 0; // PWM rate [‰]

/**
* @brief エンコーダ1/4周期の移動距離［mm / (1/4 period)］ = LW_FULL_LENGTH / 全閉⇔全開カウント数
* @remark 初期設定終了後に設定される。
*/
static float mm_per_encoder = 1.0;

/**
 * @brief 位置情報を変換する（mm -> エンコーダ1/4カウント値）。
 * @param[in] pos 位置[mm]
 * @param[out] -
 * @retval int32_t 位置[エンコーダ1/4カウント値]
 */
static inline int32_t mm2enc(int32_t pos)
{
    return (int32_t)((float)pos / mm_per_encoder);
}

/**
 * @brief 現在の位置情報を取得する。
 * @param[in] pos 位置（エンコーダカウント値）
 * @param[in] dir 動作方向
 * @param[out] -
 * @retval -
 */
POSINDEX getPositionId(int32_t pos, uint8_t dir)
{
    if (DIR_CLOSE == dir)
    {
        for (POSINDEX ii=POSIDX_FULL; ii>POSIDX_START; ii-=1)
        {
            if (current_position <= pos_close[ii]) return ii;
        }
    }
    else
    {
        for (POSINDEX ii=POSIDX_FULL; ii>POSIDX_START; ii-=1)
        {
            if (current_position >= pos_open[ii]) return ii;
        }
    }
    return POSIDX_START;
}

/**
 * @brief 現在の位置情報を更新する。
 * @param[in] cnt エンコーダカウント数
 * @param[out] -
 * @retval -
 */
int32_t updatePosition(int32_t cnt, uint8_t dir)
{
    if (0 == cnt) return POSIDX_ERROR;
    current_position += cnt;
    //if (LONG_MIN == lwpos_fullopen) return POSIDX_ERROR;
    return current_position;
}

/**
 * @brief 位置情報を設定する（初期設定動作完了後にコールする）。
 * @param[in] enc_fullopen 全開エンコーダカウント数
 * @param[out] -
 * @retval -
 */
void setPositionId(int32_t enc_fullopen)
{
    mm_per_encoder = (float)LW_FULL_LENGTH / enc_fullopen;
    pos_close[POSIDX_FULL]       = 0;
    pos_close[POSIDX_FINGER]     = mm2enc(LW_FINGER_GUARD_LENGTH);
    pos_close[POSIDX_HEAD_END]   = pos_close[POSIDX_FINGER] + mm2enc(LW_MARGIN_H2F);
    pos_close[POSIDX_HEAD_BEGIN] = pos_close[POSIDX_HEAD_END] + mm2enc(LW_HEAD_GUARD_LENGTH);
    pos_close[POSIDX_MAX_END]    = pos_close[POSIDX_HEAD_BEGIN] + mm2enc(LW_MARGIN_M2H);
    pos_close[POSIDX_MAX_BEGIN]  = enc_fullopen - mm2enc(LW_ACCELERATION_LENGTH);
    pos_close[POSIDX_START]      = enc_fullopen;
    pos_open[POSIDX_FULL]        = enc_fullopen;
    pos_open[POSIDX_FINGER]      = enc_fullopen - pos_close[POSIDX_FINGER];
    pos_open[POSIDX_HEAD_END]    = enc_fullopen - pos_close[POSIDX_HEAD_END];
    pos_open[POSIDX_HEAD_BEGIN]  = enc_fullopen - pos_close[POSIDX_HEAD_BEGIN];
    pos_open[POSIDX_MAX_END]     = enc_fullopen - pos_close[POSIDX_MAX_END];
    pos_open[POSIDX_MAX_BEGIN]   = enc_fullopen - pos_close[POSIDX_MAX_BEGIN];
    pos_open[POSIDX_START]       = 0;
    dev_enc_setEncoderLength(ENCIDX_1, mm_per_encoder);
}

/**
 * @brief 加減速エリアでの速度期待値を計算する。
 * @param[in] start_pos  ：加減速開始位置[encoder count / 4]
 * @param[in] end_pos    ：加減速終了位置[encoder count / 4]
 * @param[in] start_speed：加減速開始速度[(encoder count / 4) ms]
 * @param[in] end_speed  ：加減速終了速度[(encoder count / 4) ms]
 * @param[in] current_pos：計算位置[encoder count / 4]
 * @param[out] -
 * @retval int32_t：速度[(encoder count / 4) / ms]
 */
static inline float getSpeedTargetOnLinear(
    int32_t start_pos,
    int32_t end_pos,
    int32_t start_speed,
    int32_t end_speed,
    int32_t current_pos
)
{
    return (float)(current_pos * (end_speed - start_speed)) / (end_pos - start_pos);
}

/**
 * @brief 開動作での現在位置の速度期待値を取得する。
 * @param[in] pos：現在位置[encoder count / 4]
 * @param[out] -
 * @retval int32_t：速度[(encoder count / 4) / ms]
 */
static float getSpeedTargetOpen(int32_t pos)
{
    if      (pos < pos_open[POSIDX_MAX_BEGIN])
        return getSpeedTargetOnLinear(pos_open[POSIDX_START], pos_open[POSIDX_MAX_BEGIN], 0, (LW_SPEED_MAX / mm_per_encoder), pos); 
    else if (pos < pos_open[POSIDX_MAX_END])
        return LW_SPEED_MAX / mm_per_encoder;
    else if (pos < pos_open[POSIDX_HEAD_BEGIN])
        return getSpeedTargetOnLinear(pos_open[POSIDX_MAX_END], pos_open[POSIDX_HEAD_BEGIN], LW_SPEED_MAX, (LW_SPEED_NEAR_HEAD / mm_per_encoder), pos); 
    else if (pos < pos_open[POSIDX_HEAD_END])
        return LW_SPEED_NEAR_HEAD / mm_per_encoder;
    else if (pos < pos_open[POSIDX_FINGER])
        return getSpeedTargetOnLinear(pos_open[POSIDX_HEAD_END], pos_open[POSIDX_FINGER], LW_SPEED_NEAR_HEAD, (LW_SPEED_NEAR_FINGER / mm_per_encoder), pos); 
    return LW_SPEED_NEAR_FINGER / mm_per_encoder; // 最低速度
}

/**
 * @brief 閉動作での現在位置の速度期待値を取得する。
 * @param[in] pos：現在位置[encoder count / 4]
 * @param[out] -
 * @retval int32_t：速度[(encoder count / 4) / ms]
 */
static float getSpeedTargetClose(int32_t pos)
{
    if      (pos > pos_close[POSIDX_MAX_BEGIN])
        return getSpeedTargetOnLinear(pos_close[POSIDX_START], pos_close[POSIDX_MAX_BEGIN], 0, (LW_SPEED_MAX / mm_per_encoder), pos); 
    else if (pos > pos_close[POSIDX_MAX_END])
        return LW_SPEED_MAX / mm_per_encoder;
    else if (pos > pos_close[POSIDX_HEAD_BEGIN])
        return getSpeedTargetOnLinear(pos_close[POSIDX_MAX_END], pos_close[POSIDX_HEAD_BEGIN], LW_SPEED_MAX, (LW_SPEED_NEAR_HEAD / mm_per_encoder), pos); 
    else if (pos > pos_close[POSIDX_HEAD_END])
        return LW_SPEED_NEAR_HEAD / mm_per_encoder;
    else if (pos > pos_close[POSIDX_FINGER])
        return getSpeedTargetOnLinear(pos_close[POSIDX_HEAD_END], pos_close[POSIDX_FINGER], LW_SPEED_NEAR_HEAD, (LW_SPEED_NEAR_FINGER / mm_per_encoder), pos); 
    return LW_SPEED_NEAR_FINGER / mm_per_encoder; // 最低速度
}

/**
 * @brief 指定位置での速度期待値を取得する。
 * @param[in] pos：現在位置[mm]
 * @param[in] dir：動作方向
 * @param[out] -
 * @retval float：速度[(encoder count / 4) / ms]
 */
float getSpeedTarget(int32_t pos, uint8_t dir)
{
    if (DIR_OPEN) return getSpeedTargetOpen(pos);
    else          return getSpeedTargetClose(pos);
}

/**
 * @brief 現在位置での速度期待値を取得する。
 * @param[in] -
 * @param[out] -
 * @retval float：速度[(encoder count / 4) / ms]
 */
float getSpeedTargetAtCurrentPosition(void)
{
    if      (DIR_OPEN  == current_dir) return getSpeedTargetOpen(current_position);
    else if (DIR_CLOSE == current_dir) return getSpeedTargetClose(current_position);
    else                               return 0.0; // 停止
}

/**
 * @brief 位置情報を補正する（全閉→全開実行時）。
 * @param[in] enc_fullopen 全閉全開エンコーダカウント数（初期設定による）
 * @param[out] -
 * @retval -
 */
void updatePositionIdByOpen(int32_t enc_fullopen)
{
    if (pos_open[POSIDX_FULL] == enc_fullopen) return;
    current_position = enc_fullopen;
    setPositionId(enc_fullopen);
}

/**
 * @brief 最高速度（(1/4 count) / ms）を取得する。
 * @param[in] -
 * @param[out] -
 * @retval -
 */
inline float getSpeedMax(void)
{
    return LW_SPEED_MAX / mm_per_encoder;
}

/**
 * @brief 位置情報を補正する（全開→全閉実行時）。
 * @param[in] enc_fullclose 全閉時エンコーダカウント値
 * @param[out] -
 * @retval -
 */
void updatePositionIdByClose(int32_t enc_fullclose)
{
    if (0 == enc_fullclose) return;
    lwpos_fullopen -= enc_fullclose;
    current_position = 0;
    setPositionId(lwpos_fullopen);
}

/**
 * @brief 上限到達通知。
 * @param[in] initialize 初期設定実行中
 * @param[out] -
 * @retval -
 */
void eventFullOpen(bool initialize)
{
    if (initialize)
    {
        lwpos_fullopen = current_position;
    }
    else
    {
        if (start_position == pos_open[POSIDX_START])
        {
            updatePositionIdByOpen(current_position);
        }
    }
}

/**
 * @brief 下限到達通知。
 * @param[in] initialize 初期設定実行中
 * @param[out] -
 * @retval -
 */
void eventFullClose(bool initialize)
{
    if (initialize)
    {
        lwpos_fullopen -= current_position;
        current_position = LWPOS_FULLCLOSE;
        setPositionId(lwpos_fullopen);
    }
    else
    {
        if (start_position = pos_close[POSIDX_START])
        {
            updatePositionIdByClose(current_position);
        }
    }
}

/**
 * @brief モータ起動。
 * @param[in] 方向
 * @param[out] -
 * @retval bool 起動結果
 */
bool motor_start(uint8_t dir)
{
    pwm_rate = LW_START_POWER;
    start_position = current_position;
    current_dir = dir;
    motor_Set(pwm_rate);
    motor_On(dir);
    return true;
}

/**
 * @brief モータ停止。
 * @param[in] -
 * @param[out] -
 * @retval bool 停止結果
 */
bool motor_stop(void)
{
    current_dir = DIR_STOP;
    motor_Off();
    return true;
}

