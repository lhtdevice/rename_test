/**
 * @file SpeedControl.c
 * @brief 開閉速度制御
 */

/*******************************************************************************
 * Include Files
 *******************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#include "SpeedControl.h"

#include "main.h"
#include "MotorControl.h"
#include "DebugMonitor.h"

/*******************************************************************************
 * Private Macros
 *******************************************************************************/
#define DELTA_TIME          MOTOR_CONTROL_PERIOD

#define NUMBER_OF_SECTIONS  3

#define INITIAL_SPEED       30
#define TERMINAL_SPEED      30      // [mm/s]

//#define ENABLE_PI_CONTROL
#define KP                  1.0f    // P制御ゲイン
#define KI                  4.0f    // I制御ゲイン

#define NOT_IN_ANY_SECTION  -1

#define MAX(a, b)           (((a) > (b)) ? (a) : (b))
#define MIN(a, b)           (((a) < (b)) ? (a) : (b))
#define CLAMP(v, lo, hi)    (MIN(MAX((lo), (v)), (hi)))

/*******************************************************************************
 * Private Type Definitions
 *******************************************************************************/
typedef struct {
    int16_t max_speed;      // [mm/s]
    int16_t acceleration;   // [mm/s^2]
    int16_t deceleration;   // [mm/s^2]
} SPEED_PROFILE;

/*******************************************************************************
 * Private Function Prototypes
 *******************************************************************************/
static bool updateSection(float position);
static int8_t getCurrentSection(float position);
static void makePlan(float speed, float position);
static float updateSpeed(float speed, float position);
static int16_t getFinalSpeed(int8_t section);
static int16_t getSectionGoal(int8_t section);
static bool limitSpeed(float *speed);

/*******************************************************************************
 * Private Variables
 *******************************************************************************/
static const SPEED_PROFILE _profiles[NUMBER_OF_SECTIONS] = {
    {250, 100, -100},
    {100, 100, -100},
    { 30, 100, -100}
};

// セクション区切り（進行方向端からの距離）[mm]
static const int16_t _borders[NUMBER_OF_SECTIONS - 1] = {
    200, 30
};

static bool _backward;
static int8_t _section = NOT_IN_ANY_SECTION;
static float _ref_speed;
static float _dec_point;
#ifdef ENABLE_PI_CONTROL
static float _accumulated_error;
static bool _clamped;
#endif

#ifdef DEBUG
static float _kp = 0.1;
static float _ki = 1;
#undef KP
#define KP _kp
#undef KI
#define KI _ki
#endif // DEBUG

/*******************************************************************************
 * Public Functions
 *******************************************************************************/
float SpeedControl_init(bool backward)
{
    _section = NOT_IN_ANY_SECTION;
    _ref_speed = INITIAL_SPEED;
    _dec_point = 0;
    _backward = backward;
#ifdef ENABLE_PI_CONTROL
    _accumulated_error = 0;
    _clamped = false;
#endif

    return INITIAL_SPEED;
}

float SpeedControl_doWork(float y, float position)
{
    if (updateSection(position)) {
        makePlan(_ref_speed, position);
    }

    float r = updateSpeed(_ref_speed, position);
    _ref_speed = r;

#ifdef ENABLE_PI_CONTROL
    float e = r - y;
    if (!_clamped) {
        _accumulated_error += e;
    }
   
    float u = KP * e + KI * _accumulated_error * DELTA_TIME;

    _clamped = limitSpeed(&u);

    dbprintf(DEBUGLEVEL_DEBUG, "%.2f %.2f %.2f %.2f", r, y, u, position);
#else
    float u = _ref_speed;

    dbprintf(DEBUGLEVEL_DEBUG, "%.2f %.2f %.2f", r, y, position);
#endif

    return u;
}

#ifdef DEBUG
void SpeedControl_setGains(float kp, float ki)
{
    _kp = kp;
    _ki = ki;
}

void SpeedControl_getGains(float *kp, float *ki)
{
    *kp = _kp;
    *ki = _ki;
}
#endif

/*******************************************************************************
 * Private Functions
 *******************************************************************************/
static bool updateSection(float position)
{
    bool ret = false;
    int8_t new_sec = getCurrentSection(position);

    if (_section != new_sec) {
        _section = new_sec;
        ret = true;
    }

    return ret;
}

static int8_t getCurrentSection(float position)
{
    float p;
    if (!_backward) {
        p = LW_FULL_WIDTH - (position + LW_WINDOW_WIDTH);
    }
    else {
        p = position;
    }

    int i;
    for (i = 0; i < NUMBER_OF_SECTIONS - 1; i++) {
        if (p > _borders[i]) {
            break;
        }
    }

    return i;
}

static void makePlan(float speed, float position)
{
    if (_section == NOT_IN_ANY_SECTION) {
        return;
    }

    const int32_t final_speed = getFinalSpeed(_section);
    const int32_t max_speed = _profiles[_section].max_speed;
    const int32_t acc = _profiles[_section].acceleration;
    const int32_t dec = _profiles[_section].deceleration;
    const int32_t goal = getSectionGoal(_section);

    float edge; // 進行方向側の端位置
    if (!_backward) {
        edge = position + LW_WINDOW_WIDTH;
    }
    else {
        edge = LW_FULL_WIDTH - position;    // 座標反転（進行方向を正(+)とする）
    }

    // 区間最高速度まで加速して区間最終速度に減速するのに必要な距離
    float dmin = (max_speed + speed) * (max_speed - speed) / (2 * acc) +
                    (float)(final_speed + max_speed) * (final_speed - max_speed) / (2 * dec);
    float bd; // 減速を開始する位置の区間終点からの距離

    // 区間最高速度に到達可能
    if (edge <= goal - dmin) {
        bd = (float)(final_speed * final_speed - max_speed * max_speed) / (2 * dec);
    }
    // 区間最高速に到達しない
    else {
        // 区間最終速度に到達するのに必要な距離
        dmin = (final_speed * final_speed - speed * speed) / (2 * acc);

        // 区間最終速度に到達可能
        if (edge < goal - dmin) {
            float dist = fabsf(goal - edge);
            float A = acc / 2.0 - (float)(acc * acc) / (2 * dec);
            float B = (1 - (float)acc / dec) * speed;
            float C = (final_speed * final_speed - speed * speed) / (2 * dec) - dist;

            float tx = (-B + sqrtf(B * B - 4 * A * C)) / (2 * A);
            float vx = acc * tx + speed;
            bd = (final_speed * final_speed - vx * vx) / (2 * dec);
        }
        // 区間最終速度に到達しない
        else {
            bd = 0;    // 減速しない（加速のみ）
        }
    }

    _dec_point = goal - bd - LW_WINDOW_WIDTH;
}

static float updateSpeed(float speed, float position)
{
    if (_section != NOT_IN_ANY_SECTION) {
        float p;

        if (!_backward) {
            p = position;
        }
        else {
            p = LW_FULL_WIDTH - (position + LW_WINDOW_WIDTH);    // 座標反転（進行方向を正(+)とする）
        }

        float a = p < _dec_point ? _profiles[_section].acceleration : _profiles[_section].deceleration;
        speed += a * DELTA_TIME;

        int16_t fin = getFinalSpeed(_section);
        if (a < 0) {
            speed = MAX(speed, fin);    // 区間最終速度が下限
        }
        speed = MIN(speed, _profiles[_section].max_speed); // 区間最高速度が上限
    }
    else {
        speed = 0;
    }

    return speed;
}

static int16_t getFinalSpeed(int8_t section)
{
    int16_t fs;

    if (section >= NUMBER_OF_SECTIONS - 1) {
        fs = TERMINAL_SPEED;
    }
    else {
        fs = _profiles[section + 1].max_speed;
    }

    return fs;
}

static int16_t getSectionGoal(int8_t section)
{
    int16_t goal;

    if (section >= NUMBER_OF_SECTIONS - 1) {
        goal = LW_FULL_WIDTH;
    }
    else {
        goal = LW_FULL_WIDTH - _borders[section];
    }

    return goal;
}

static bool limitSpeed(float *speed)
{
    bool limited = false;

    if (*speed < MIN_SPEED) {
        limited = true;
        *speed = MIN_SPEED;
    }
    else if (*speed > MAX_SPEED) {
        limited = true;
        *speed = MAX_SPEED;
    }

    return limited;
}