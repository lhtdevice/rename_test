﻿/**
 * @file KenzaiOutputInfo.h
 * @brief 建材機器制御結果の出力情報を定義
 */
#ifndef KENZAI_OUTPUT_INFO_H
#define KENZAI_OUTPUT_INFO_H    
    
#include "KenzaiTypes.h"    
    
/**
* @struct SKenzaiOutputInfo
* @brief  建材機器制御結果の出力情報をまとめた構造体
*/
typedef struct KenzaiOutputInfo_tag {
    EKENZAI_EQUIP equip;	///< 状態
} SKenzaiOutputInfo;
    
#endif    
/* [] END OF FILE */
