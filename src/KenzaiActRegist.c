/**
 * @file KenzaiActRegist.c
 * @brief 機器に対する操作の結果に応じた処理の実装。
 * @copyright (c) LIXIL Corporation, 2018-2019 All Rights Reserved.
 */
#include "KenzaiActRegist.h"

static SKenzaiActRegist actRegist = {0, 0, 0, 0, 0, 0};

/**
 * @brief 全閉への移動操作を行った回数を取得する。
 * @param[in] -
 * @param[out] -
 * @retval 全閉への移動操作を行った回数
*/
uint32_t KenzaiActRegist_GetAllCloseCount(void)
{
    return actRegist.allCloseCount;
}

/**
 * @brief 全開への移動操作を行った回数を取得する。
 * @param[in] -
 * @param[out] -
 * @retval 全開への移動操作を行った回数
*/
uint32_t KenzaiActRegist_GetAllOpenCount(void)
{
    return actRegist.allOpenCount;
}

/**
 * @brief 全閉への移動操作を行った回数を取得する。
 * @param[in] -
 * @param[out] -
 * @retval 全閉への移動操作を行った回数
*/
uint32_t KenzaiActRegist_GetAllVentilationCount(void)
{
    return actRegist.allVentilationCount;
}

/**
 * @brief 全閉への移動操作を行った回数を取得する。
 * @param[in] -
 * @param[out] -
 * @retval 全閉への移動操作を行った回数
*/
uint32_t KenzaiActRegist_GetAllLockCount(void)
{
    return actRegist.allLockCount;
}

/**
 * @brief 全閉への移動操作を行った回数を取得する。
 * @param[in] -
 * @param[out] -
 * @retval 全閉への移動操作を行った回数
*/
uint32_t KenzaiActRegist_GetAllUnlockCount(void)
{
    return actRegist.allUnlockCount;
}

/**
 * @brief 全閉への移動操作を行った回数を取得する。
 * @param[in] -
 * @param[out] -
 * @retval 全閉への移動操作を行った回数
*/
uint32_t KenzaiActRegist_GetAllFalseCount(void)
{
    return actRegist.allOtherCount;
}

/**
 * @brief 機器に対する操作の結果に応じた処理を実施する。
 * @param[in] act_change 操作
 * @param[out] -
 * @retval -
 * @remarks actRegistのメンバ変数はUINT32_MAXの次は0に戻る
*/
void KenzaiActRegist_Regist(EACT_CHANGE act_change)
{
    switch (act_change)
    {
    case EACT_CHANGE_TO_CLOSE:  ///< 全閉状態に変化.
        actRegist.allCloseCount += 1;
        break;
    case EACT_CHANGE_TO_OPEN:   ///< 全開状態に変化
        actRegist.allOpenCount += 1;
        break;
    case EACT_CHANGE_TO_VENTILATION:    ///< 採風状態に変化
        actRegist.allVentilationCount += 1;
        break;
    case EACT_CHANGE_TO_LOCK:   ///< 施錠状態に変化
        actRegist.allLockCount += 1;
        break;
    case EACT_CHANGE_TO_UNLOCK: ///< 解錠状態に変化
        actRegist.allUnlockCount += 1;
        break;
    case EACT_CHANGE_TO_OTHER:  ///< 定義以外の状態に変化.
        // fall through
    default:
        actRegist.allOtherCount += 1;
    }
}

/* [] END OF FILE */
