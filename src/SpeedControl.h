/**
 * @file SpeedControl.h
 * @brief 開閉速度制御
 */

#ifndef _SPEED_CONTROL_H_
#define _SPEED_CONTROL_H_
/*******************************************************************************
 * Include Files
 *******************************************************************************/
#include <stdbool.h>

/*******************************************************************************
 * Macros
 *******************************************************************************/
#define MIN_SPEED           30      // [mm/s]
#define MAX_SPEED           300     // [mm/s]

/*******************************************************************************
 * Type Definitions
 *******************************************************************************/

/*******************************************************************************
 * Function Prototypes
 *******************************************************************************/

/**
 * @brief       速度制御初期化処理
 * @param[in]   backward    負方向への進行時にtrue
 * @return      初期速度指令値
 */
float SpeedControl_init(bool backward);

/**
 * @brief       速度更新処理
 * @param[in]   y           速度測定値
 * @param[in]   position    位置
 * @return      速度指令値
 */
float SpeedControl_doWork(float y, float position);

#ifdef DEBUG
void SpeedControl_setGains(float kp, float ki);
void SpeedControl_getGains(float *kp, float *ki);
#endif // DEBUG

#endif // _SPEED_CONTROL_H_