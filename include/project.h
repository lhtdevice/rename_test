﻿/**
 * @file  project.h
 * @brief プロジェクト共通定義ファイル
 * @copyright (c) LIXIL Corporation, 2018-2020 All Rights Reserved.
 */

#ifndef USER_PROJECT_H
#define USER_PROJECT_H

#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#ifdef __RL78__
 #include "r_cg_macrodriver.h"
 typedef int32_t             intptr_t;
 typedef uint32_t            uintptr_t;
#else
 #include <stdint.h>
#endif

#if (DEF_CRLF == 1014)
 #define STR_CRLF "\xd\xa"
#elif (DEF_CRLF == 14)
 #define STR_CRLF "\xd"
#else
 #define STR_CRLF "\xa"
#endif

#include "iodefine.h"
#include "r_cg_macrodriver.h"
#include "r_cg_cgc.h"
#include "r_cg_port.h"
#include "r_cg_intc.h"
#include "r_cg_serial.h"
#include "r_cg_wdt.h"
#include "r_cg_adc.h"
#include "r_cg_timer.h"
#include "r_cg_rtc.h"
#include "r_cg_userdefine.h"


#endif//USER_PROJECT_H
